/* 
 Generator of Altera MIF file from SiLabs ClockBuild Pro tool.
 This tools is designed for use with corresponding HDL module and SI5341 device.
 This file is delivered "as is" without any warranty.
 
 Company  : ReFLEX CES 
 Creation : 2015/04/22
 Author   : flavenant (flavenant@reflexces.com)
 Version  : 0.1
*/

#include <stdio.h>

#define MAX_LINE_SIZE 256

struct S_I2C_cmd
{
    unsigned int stop    : 1;
    unsigned int addr    : 7;
    unsigned int sub     : 8;
    unsigned int usa     : 1;
    unsigned int delay   : 7;
    unsigned int data    : 8;
};

union S_I2C_cmd_u
{
    unsigned int v;
    struct S_I2C_cmd s;
};

int main(int argc,char **argv)
{
    FILE* fp_i;
    FILE* fp_o;
    char pLine[MAX_LINE_SIZE];
    unsigned int uiLine, uiMIFAddr;
    unsigned int uiPage, uiPage_prev, uiAddr, uiData;
    unsigned int uiRAMDepth, uiI2CAddr;
    char* cFilename_i;
    char* cFilename_o;
    union S_I2C_cmd_u i2c_cmd;
    
    // Check arguments
    if (argc!=5) {
        printf("ERROR: invalid parameter input.\n");
        printf("----------------------------------------------------------------\n");
        printf("Usage is:\n");
        printf(" GenMIF RAM_DEPTH I2C_ADDR in_file out_file\n");
        printf("\n");
        printf("Parameters:\n");
        printf(" RAM_DEPTH  : RAM depth of the corresponding HDL module.\n");
        printf(" I2C_ADDR   : device address on i2c bus.\n");
        printf(" in_file    : input .txt file exported from ClockBuilder Pro.\n");
        printf(" out_file   : name of output .mif file to be generated.\n");
        printf("----------------------------------------------------------------\n");
        return -1;
    }
    
    // Extract parameters
    uiRAMDepth=atoi(argv[1]);
    uiI2CAddr=atoi(argv[2]);
    cFilename_i=argv[3];
    cFilename_o=argv[4];
    
    // Open input file
    fp_i=fopen(cFilename_i, "r");
    if (fp_i==NULL) {
        printf("ERROR: unable to open input file %s.\n", cFilename_i);
        return -1;
    }
    
    // Output file exists?
    if (access(cFilename_o, 0)==0) { 
        printf("ERROR: output file %s already exists.\n", cFilename_o);
        return -1;
    }
    
    // Open output file
    fp_o=fopen(cFilename_o, "w");
    if (fp_o==NULL) {
        printf("ERROR: unable to create output file %s.\n", cFilename_o);
        return -1;
    }
    
    // Write the MIF header
    fprintf(fp_o, "DEPTH = %d;\n", uiRAMDepth);
    fprintf(fp_o, "WIDTH = 32;\n");
    fprintf(fp_o, "ADDRESS_RADIX = HEX;\n");
    fprintf(fp_o, "DATA_RADIX = HEX;\n");
    fprintf(fp_o, "CONTENT\n");
    fprintf(fp_o, "BEGIN\n");

    // Process the files
    uiLine=0;
    uiMIFAddr=0;
    uiPage_prev=-1;
    while (fgets(pLine, MAX_LINE_SIZE, fp_i)!=NULL) {
        // Increment line counter
        uiLine++;
        
        ///////////////////////////////////////////////////////////////////////////////////////
        // Skip comment lines
        ///////////////////////////////////////////////////////////////////////////////////////
        if (pLine[0]=='#' ) {
            printf("line %d skipped (comment detected).\n", uiLine);
            continue;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////
        // Parse lines
        ///////////////////////////////////////////////////////////////////////////////////////
        if (sscanf(pLine, "0x%02x%02x,0x%02x", &uiPage, &uiAddr, &uiData)!=3) {
            printf("line %d skipped (not a valid data input).\n", uiLine);
            continue;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////
        // Test page: change current page if different
        ///////////////////////////////////////////////////////////////////////////////////////
        if (uiPage!=uiPage_prev) {
            uiPage_prev=uiPage;
            
            // Build the i2c command
            i2c_cmd.v=0; // ensure there is no bit set.
            i2c_cmd.s.stop=0;
            i2c_cmd.s.addr=uiI2CAddr;
            i2c_cmd.s.sub=0x01;
            i2c_cmd.s.usa=1;
            i2c_cmd.s.delay=0;
            i2c_cmd.s.data=uiPage;
            
            // Write an entry in the output file
            fprintf(fp_o, "%04X : %08X; -- Select page 0x%02X.\n", uiMIFAddr, i2c_cmd.v, uiPage);
            uiMIFAddr++;
        }
        
        ///////////////////////////////////////////////////////////////////////////////////////
        // Write the data
        ///////////////////////////////////////////////////////////////////////////////////////
        // Prepare the i2c command
        i2c_cmd.v=0; // ensure there is no bit set.
        
        // Detect special addresses which require delay
        if (uiPage==0x0B && uiAddr==0x24) i2c_cmd.s.delay=5;
        if (uiPage==0x0B && uiAddr==0x25) i2c_cmd.s.delay=5;
        if (uiPage==0x00 && uiAddr==0x21) i2c_cmd.s.delay=5;
        if (uiPage==0x00 && uiAddr==0x1C) i2c_cmd.s.delay=5;
        
        // Build the i2c command
        i2c_cmd.s.stop=0;
        i2c_cmd.s.addr=uiI2CAddr;
        i2c_cmd.s.sub=uiAddr;
        i2c_cmd.s.usa=1;
        i2c_cmd.s.data=uiData;
        
        // Write an entry in the output file
        fprintf(fp_o, "%04X : %08X; -- Write data (stop=%d, addr=0x%02X, sub=0x%02X, usa=%d, delay=%d, data=0x%02X).\n", uiMIFAddr, i2c_cmd.v, i2c_cmd.s.stop, i2c_cmd.s.addr, i2c_cmd.s.sub, i2c_cmd.s.usa, i2c_cmd.s.delay, i2c_cmd.s.data);
        uiMIFAddr++;
    }
    
    // Generate the stop entry
    if (uiMIFAddr<uiRAMDepth-1) {
        // Build the i2c command
        i2c_cmd.v=0; // ensure there is no bit set.
        i2c_cmd.s.stop=1;
        fprintf(fp_o, "%04X : %08X; -- Stop sequence.\n", uiMIFAddr, i2c_cmd.v);
        uiMIFAddr++;
    }
    
    // Write the MIF trailer
    fprintf(fp_o, "END;\n");
    
    // Close files
    fclose(fp_i);
    fclose(fp_o);
}

