==================================================================
= Design overview.
==================================================================
This design demonstrates the configuration of the SI5341.
It uses a VHDL module (auto_i2c) which reads data from a RAM block and performs I2C accesses in order to configure the clock generator.
The RAM content is generated with a custom converter from a ClockBuilder Pro exported file.
This allows to perform any kind of configuration:
 - setting output clock frequencies.
 - switching from input clock source.
 - ...
 
==================================================================
= Design testing.
==================================================================
Open the Quartus project file in the /syn/ folder.
Start a full compilation.
Program the board with the output .sof file.

[optional: Use BP1 to reset the design.]
Use BP2 to configure the SI5341.

Check clocks frequencies with the provided SignalTap CheckClocks.stp.

==================================================================
= How to change the SI5341 configuration.
==================================================================
1. SI5341 settings
Run ClockBuilder Pro and build your configuration (./CBP/ folder contains an example project).
Export the settings and save it to config.txt file.
/!\ Verify that "Include summary header" is checked for traceability of the output .txt file.
/!\ Verify that "Include pre- and post-write control register writes" is enabled for correct configuration of SI5341.

2. Generate the .mif
Go to the tools folder.
Edit run_GenMIF.bat to point to the correct .txt file.
If needed, modify the output file name.
Run run_GenMIF.bat.

3. Update MIF
Override the .mif file in the Quartus project folder with the new created one.
The steps below generate an updated .sof without running the full flow.
This requires that the Quartus database is complete and up to date.
If it is not the case, just run a full compilation process.

With full compilation database:
In Quartus, run Processing->Update Memory Initialization File.
In Quartus, run Processing->Start->Start Assembler.
The generated .sof now contains the updated .mif file.

==================================================================
= Deal with several configurations.
==================================================================
The auto_i2c module can manage several configurations for designs requiring two or more clock settings.
Each configuration is stored in a RAM block initialized from a .mif file.
The user can then select which configuration to use and/or switch from one to the other using the cfg_sel input port of the module.

Edit the auto_i2c instantiation in the top level file.
Change NB_RAM generic to reflect the number of configurations needed.
By default, each configuration uses a .mif file named as follow:
 - configuration 0: config_ram0.mif
 - configuration 1: config_ram1.mif
 - ...
