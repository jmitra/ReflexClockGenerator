#Created By: Jozsef Imrek 
#Purpose : For ISSP programming automation

set device [lindex [get_service_paths device] 0]
puts "device = $device"
device_download_sof $device output_files/RefDesign_SI5341.sof



set issp [lindex [get_service_paths issp] 0]
puts "issp = $issp"
set claimed_issp [claim_service issp $issp mylib]

issp_write_source_data $claimed_issp 0x3
after 100
issp_write_source_data $claimed_issp 0x2
after 100
issp_write_source_data $claimed_issp 0x3
after 100
issp_write_source_data $claimed_issp 0x1
after 100
issp_write_source_data $claimed_issp 0x3



