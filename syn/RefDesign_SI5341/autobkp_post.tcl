# Version 14
post_message "############################################################################################"
post_message "## Post incremental backup starts..."
post_message "############################################################################################"

# #################################################################################################################################
# #################################################################################################################################
# Edit variables in this file to customize behavior of auto backup
# #################################################################################################################################
# #################################################################################################################################
source "autobkp_cfg.tcl"
# #################################################################################################################################
# #################################################################################################################################
# #################################################################################################################################

# ################################################################################################################################
# If file bk_options_file exist, source it to take into account special cases.
# File is sourced then deleted.
# ################################################################################################################################
set bk_options_file "autobkp_post_options.tcl"
if { [catch {
    if {[file exists $bk_options_file]} {
        post_message -type info "File $bk_options_file exists. Source it."
        source $bk_options_file
        file delete -force $bk_options_file
    }            
} res ] } {
    post_message -type warning "Problem while processing $bk_options_file: $res"
}
# ################################################################################################################################

# ################################################################################################################################
# Exit if path to 7zip is incorrect
# ################################################################################################################################
if {![file exists $zipexe]} {
    post_message -type warning "Unable to find 7zip at $zipexe."
    post_message -type warning "Edit the path in autobkp_cfg.tcl file. Exit autobkp."
    return -1
}   
# ################################################################################################################################
# ################################################################################################################################

if {$skip_autobkp_post} {
    post_message -type info "autobkp_post is skipped due to skip_autobkp_post option enabled."
} else {
    # Do work...
    global quartus

    set module_or_flow [lindex $quartus(args) 0]
    set project [lindex $quartus(args) 1]
    set revision [lindex $quartus(args) 2]

    # Generates a name for the qar based on the name of the revision
    # and the current time.
    proc generateTimedName { project revision } {

        # The name of the qar is based on the revision name and the time
        set time_stamp [clock format [clock seconds] -format {%Y%m%d-%H%M%S}]
        return $revision-$time_stamp
    }

    # Temporary names (from pre script)
    set base_pre_name           "bk_pre_$revision"
    set base_pre_name_comment   "${base_pre_name}_comment.txt"

    # Directory and file name
    set timed_name          [generateTimedName $project $revision]
    set bkp_directory       "autobkp"
    set full_directory      [file join $bkp_directory $revision]
    set file_common_name    $revision
    set full_path           [file join $full_directory $file_common_name]

    # Test flow and .qar
    switch -exact -- $module_or_flow {
        compile -
        compile_and_simulate -
        incremental_fitting {
            if { [catch {
                if {![file exists "$base_pre_name.qar"]} {
                   post_message -type warning "## $base_pre_name.qar does not exist, exiting incremental backup."
                   return -1
                }
            } res ] } {
                post_message -type warning $res
                post_message -type warning "## Problem with $base_pre_name.qar file, exiting incremental backup."
                return -1
            } else {
                post_message "## $base_pre_name.qar exist, continuing incremental backup..."
            }
        }
    }

    # Create a new .qar with report and output files only
    set qar_post_name        "bk_post_$revision"

    # Create QAR
    if { [catch {
        project_open -revision $revision $project
        #set options "-use_file_set custom -use_file_subset out -use_file_subset rpt"
        project_archive -use_file_set custom -use_file_subset {out rpt} $qar_post_name.qar -overwrite
        project_close
    } res ] } {
        post_message -type warning $res
    } else {
        post_message "## Successfully archived project $qar_post_name.qar"
    }

    # Create backup directory
    if { [catch {

        file mkdir $bkp_directory

    } res ] } {
        post_message -type warning $res
        post_message -type warning "## Cannot create backup directories $bkp_directory, exiting incremental backup."
        return -1
    } else {
        post_message "## Successfully created backup directories $bkp_directory, continuing incremental backup..."
    }

    # Restore both projects (pre and post)
    if { [catch {

        if { $enable_qsf_overwrite } {
            project_restore $base_pre_name.qar -destination $full_directory -overwrite -update_included_file_info
            project_restore $qar_post_name.qar -destination $full_directory -overwrite -update_included_file_info
        } else {
            project_restore $base_pre_name.qar -destination $full_directory -overwrite
            project_restore $qar_post_name.qar -destination $full_directory -overwrite
        }

    } res ] } {
        post_message -type warning $res
        post_message -type warning "## Problem while restoring archived project, exiting incremental backup."
        return -1
    } else {

    }

    # Delete temporary .qar files
    if { [catch {

        file delete $qar_post_name.qar
        file delete $qar_post_name.qarlog
        file delete $base_pre_name.qar
        file delete $base_pre_name.qarlog

    } res ] } {
        post_message -type warning $res
        post_message -type warning "## Problem while deleting temporary .qar ($base_pre_name.* or $qar_post_name.*), exiting incremental backup."
        return -1
    } else {
        
    }

    # Update .log file
    if { [catch {
        # Open the comment file
        if {[file exists ${base_pre_name_comment}]} {
            set fh [open ${base_pre_name_comment} r ]
            set comment [read $fh]
            close $fh
        } else {
            set comment "Comment: none (file not found)"
        }

        # Write the comment to .log file
        cd $bkp_directory
        if {[file exists "$file_common_name.log"]} {
            if {$overwrite_ro} {file attribute $file_common_name.log -readonly 0}
        }
        set fh [open $file_common_name.log a ]
        
        puts $fh ""
        puts $fh "############################################################################################"
        puts $fh "File name: $timed_name.zip"
        puts $fh "Quartus: ${::quartus(version)}"
        puts $fh "Overwrite QSF filepaths: ${enable_qsf_overwrite}"
        puts $fh "$comment"

        close $fh
        cd ".."
            
        # Open .log file for edition through Notepad
        cd $bkp_directory
        if { $enable_log_prompt_post } {
            exec notepad $file_common_name.log
        }
        cd ".."
        
        # Copy log into future zip file
        if { $enable_log_include } {
            cd $bkp_directory
            file copy -force $file_common_name.log  [file join $file_common_name "${file_common_name}_fullhistory.log"]
            cd ".."
            file copy -force $base_pre_name_comment [file join $bkp_directory $file_common_name "${file_common_name}_modif.log"]
        }
        
        # Delete the comment file
        if {[file exists ${base_pre_name_comment}]} {
            file delete -force ${base_pre_name_comment}
        }
    } res ] } {
        post_message -type warning $res
        post_message -type warning "## Problem while opening/creating/updating ${base_pre_name_comment} or $file_common_name.log file. Skipping..."
    }

    # Create .zip
    if { [catch {       
            cd $bkp_directory
            if {[file exists "$file_common_name.zip"]} {
                if {$overwrite_ro} {file attribute $file_common_name.zip -readonly 0}
                file delete -force $file_common_name.zip
            }
            exec $zipexe a $file_common_name.zip $file_common_name
            file copy -force $file_common_name.zip $timed_name.zip
            file delete -force $file_common_name
            cd ".."
    } res ] } {
        post_message -type warning "## Problem while creating .zip file."
        return -code error $res
    }

    post_message "## Successfully processing incremental backup."
}

post_message "############################################################################################"
post_message "## Post incremental backup ends."
post_message "############################################################################################"


