post_message "############################################################################################"
post_message "## Archive_project starts..."
post_message "############################################################################################"

# #################################################################################################################################
# #################################################################################################################################
# Edit variables below to customize behavior of auto backup
# #################################################################################################################################

# Enable (1) / Disable (0) overwrite of filepaths in QSF file.
set enable_qsf_overwrite 0

#Here set full path to zip executable
set 7zip_path "C:\\Program Files\\7-Zip"
set zipexe [file join $7zip_path "7z.exe"]

# #################################################################################################################################
# #################################################################################################################################
# #################################################################################################################################

# Directory used to store zipped projects
set bkp_directory      "archived_projects"

global quartus

if ![is_project_open] {
    post_message -type error "## No project openned!"
    return -1
}

set revision [get_current_revision]
cd [get_project_directory]

# Temporary .qar name
set qar_name        "archive_$revision"

# Create QAR
if { [catch {
    project_archive -use_file_set custom -use_file_subset {qsf auto out rpt} $qar_name.qar -overwrite
} res ] } {
    post_message -type warning $res
    post_message -type warning "## $qar_name.qar cannot be created."
} else {
    post_message "## Successfully archived project $qar_name.qar"
}

# Generates a name for the qar based on the name of the revision
# and the current time.
proc generateTimedName { revision } {

    # The name of the qar is based on the revision name and the time
    set time_stamp [clock format [clock seconds] -format {%Y%m%d-%H%M%S}]
    return $revision-$time_stamp
}

# Directory and file name
set timed_name          [generateTimedName $revision]
set full_directory      [file join $bkp_directory $revision]
set file_common_name    $revision
set full_path           [file join $full_directory $file_common_name]

# Create archive directory
if { [catch {

    file mkdir $bkp_directory

} res ] } {
    post_message -type warning $res
    post_message -type warning "## Cannot create archive directory $bkp_directory, exiting Archive_project."
    return -1
} else {
    post_message "## Successfully created archive directory $bkp_directory, continuing Archive_project..."
}

# Restore both projects (pre and post)
if { [catch {
    if { $enable_qsf_overwrite } {
        project_restore $qar_name.qar -destination $full_directory -overwrite -update_included_file_info
    } else {
        project_restore $qar_name.qar -destination $full_directory -overwrite
    }
} res ] } {
    post_message -type warning $res
    post_message -type warning "## Problem while restoring archived project, exiting Archive_project."
    return -1
} else {

}

# Delete temporary .qar files
if { [catch {
    file delete $qar_name.qar
    file delete $qar_name.qarlog
} res ] } {
    post_message -type warning $res
    post_message -type warning "## Problem while deleting temporary .qar ($qar_name.*), exiting Archive_project."
    return -1
} else {
    
}

# Create .zip
if { [catch {       
        cd $bkp_directory
        if {[file exists "$file_common_name.zip"]} {
            file delete -force $file_common_name.zip
        }
        exec $zipexe a $file_common_name.zip $file_common_name
        file copy -force $file_common_name.zip $timed_name.zip
        file delete -force $file_common_name
} res ] } {
    post_message -type warning "## Problem while creating .zip file."
    return -code error $res
} else {
    post_message "## Successfully processing Archive_project."
}

post_message "############################################################################################"
post_message "## Archive_project ends."
post_message "############################################################################################"


