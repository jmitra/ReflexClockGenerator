# Version 13
# #################################################################################################################################
# #################################################################################################################################
# Edit variables below to customize behavior of auto backup
# #################################################################################################################################
# #################################################################################################################################

# Enable (1) / Disable (0) log prompt (notepad) at start (pre) or end (post) of flow
set enable_log_prompt_pre  1
set enable_log_prompt_post 0

# Skip (1) / not Skip (0) post-autobkp
set skip_autobkp_post 0

# Enable (1) / Disable (0) overwrite of filepaths in QSF file.
set enable_qsf_overwrite 0

# Enable (1) / Disable (0) include of log files in the ZIP.
set enable_log_include 1

#Here set full path to 7zip executable (use 7zG.zip for GUI version)
set 7zip_path "C:/Program Files/7-Zip"
#set zipexe [file join $7zip_path "7zG.exe"]
set zipexe [file join $7zip_path "7z.exe"]

# Enable (1) / Disable (0) overwrite of read-only files
set overwrite_ro 1
