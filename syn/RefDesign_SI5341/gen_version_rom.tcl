# #############################################################################################################################
#Set here informations (add/delete rom generation for additional/unused fields)
# #############################################################################################################################
set version_majeur  1
set version_mineur  0
set version_patch   0
set version_timecode [clock format [clock seconds] -format {%y%m%d%H%M}]

# #############################################################################################################################
#Usefull procedures...
# #############################################################################################################################
proc generate_rom_mif {rom_entity_name data_width data_hex_value} {

    if { [catch {
        set fh [open "${rom_entity_name}.mif" w ]
        
        
        puts $fh "WIDTH=${data_width};"
        puts $fh "DEPTH=1;"
        puts $fh " "
        puts $fh "ADDRESS_RADIX=HEX;"
        puts $fh "DATA_RADIX=HEX;"
        puts $fh " "
        puts $fh "CONTENT BEGIN"
        puts $fh "    0  :   ${data_hex_value};"
        puts $fh "END;"

        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

# #############################################################################################################################
#ROM generation : version_version
# #############################################################################################################################
set rom_entity_name "rom_id_version"
set data_width "32"
set data_hex_value [format "%04x%02x%02x" $version_patch $version_mineur $version_majeur]

if { [catch { generate_rom_mif ${rom_entity_name} ${data_width} ${data_hex_value} } res] } {
    post_message -type error "Problem while generating ${rom_entity_name}.mif file\n$res"
}

# #############################################################################################################################
#ROM generation : version_timecode
# #############################################################################################################################
set rom_entity_name "rom_id_timecode"
set data_width "32"
set data_hex_value [format %x $version_timecode]

if { [catch { generate_rom_mif ${rom_entity_name} ${data_width} ${data_hex_value} } res] } {
    post_message -type error "Problem while generating ${rom_entity_name}.mif file\n$res"
}


