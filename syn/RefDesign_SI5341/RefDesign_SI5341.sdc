# ############################################################################################################################################
# ############################################################################################################################################
post_message -type info "############################################################################################################################################"
post_message -type info "# Setting SDC constraints of RxC modules..."
# ############################################################################################################################################
# ############################################################################################################################################

# ############################################################################################################################
# ############################################################################################################################
# Base clocks
# ############################################################################################################################
# ############################################################################################################################
set_time_format -unit ns -decimal_places 3

create_clock -name {clk_100mhz_1}       -period 100.000MHz    [get_ports {clk_100mhz_1}]
create_clock -name {clk_100mhz_2}       -period 100.000MHz    [get_ports {clk_100mhz_2}]
create_clock -name {clk_100mhz_3}       -period 100.000MHz    [get_ports {clk_100mhz_3}]
create_clock -name {clk_100mhz_4}       -period 100.000MHz    [get_ports {clk_100mhz_4}]
create_clock -name {clk_xcvr_qsfp_a}    -period 156.250MHz    [get_ports {clk_xcvr_qsfp_a}]
create_clock -name {clk_xcvr_dp_a}      -period 156.250MHz    [get_ports {clk_xcvr_dp_a}]
create_clock -name {clk_xcvr_fcm1_a}    -period 156.250MHz    [get_ports {clk_xcvr_fcm1_a}]
create_clock -name {clk_xcvr_fcm2_a}    -period 156.250MHz    [get_ports {clk_xcvr_fcm2_a}]
create_clock -name {gbtclk0}            -period 156.250MHz    [get_ports {gbtclk0}]
create_clock -name {gbtclk1}            -period 156.250MHz    [get_ports {gbtclk1}]
create_clock -name {fmc_clk0_m2c}       -period 156.250MHz    [get_ports {fmc_clk0_m2c}]
create_clock -name {fmc_clk1_m2c}       -period 156.250MHz    [get_ports {fmc_clk1_m2c}]
create_clock -name {clk_xcvr_ext_a}     -period 156.250MHz    [get_ports {clk_xcvr_ext_a}]
create_clock -name {clk_xcvr_ext1}      -period 156.250MHz    [get_ports {clk_xcvr_ext1}]
create_clock -name {clk_xcvr_ext3}      -period 156.250MHz    [get_ports {clk_xcvr_ext3}]
create_clock -name {ext_dphs_clk1_m2c}  -period 156.250MHz    [get_ports {ext_dphs_clk1_m2c}]
create_clock -name {ext_dphs_clk2_m2c}  -period 156.250MHz    [get_ports {ext_dphs_clk2_m2c}]
create_clock -name {ext_dphs_clk3_m2c}  -period 156.250MHz    [get_ports {ext_dphs_clk3_m2c}]
create_clock -name {ext_dp_clk_m2c}     -period 156.250MHz    [get_ports {ext_dp_clk_m2c}]
create_clock -name {dp_aux}             -period 500.000MHz    [get_ports {dp_aux}]
create_clock -name {ext_dp_m2c[9]}      -period 500.000MHz    [get_ports {ext_dp_m2c[9]}]
create_clock -name {gbe_rx_clk}         -period 125.000MHz    [get_ports {gbe_rx_clk}]
create_clock -name {pcie_edge_refclk}   -period 100.000MHz    [get_ports {pcie_edge_refclk}]
create_clock -name {mem_ref_clk}        -period 100.000MHz    [get_ports {mem_ref_clk}]

# ############################################################################################################################
# ############################################################################################################################
# Generated clocks
# ############################################################################################################################
# ############################################################################################################################
derive_pll_clocks

derive_clock_uncertainty

# ############################################################################################################################
# ############################################################################################################################
#  MAX10 <=> FPGA
# ############################################################################################################################
# ############################################################################################################################
#============================================================================================================================
# RX constraints
#============================================================================================================================
# Clocks
create_clock -name      lnk_m2f_clk -period 20.000 [get_ports {lnk_m2f_clk}]    -waveform {5.000 15.000}
create_clock -name virt_lnk_m2f_clk -period 20.000 

# IO constraints
set tSKEW    2.000

set max_delay [expr " $tSKEW"]
set min_delay [expr "-$tSKEW"]

set_input_delay -clock [get_clocks "virt_lnk_m2f_clk"]  -max $max_delay [get_ports "lnk_m2f_dat"]
set_input_delay -clock [get_clocks "virt_lnk_m2f_clk"]  -max $max_delay [get_ports "lnk_m2f_dat"] -add_delay -clock_fall
set_input_delay -clock [get_clocks "virt_lnk_m2f_clk"]  -min $min_delay [get_ports "lnk_m2f_dat"] -add_delay
set_input_delay -clock [get_clocks "virt_lnk_m2f_clk"]  -min $min_delay [get_ports "lnk_m2f_dat"] -add_delay -clock_fall

#============================================================================================================================
# TX constraints
#============================================================================================================================
# Clocks
create_generated_clock -name      lnk_f2m_clk   -source [get_pins -compatibility_mode "*gen_f2m:i_ddio_o_wrapper|*|out_path.out_path_fr.fr_out_data_ddio|dataout"] [get_ports {lnk_f2m_clk}]
create_generated_clock -name virt_lnk_f2m_clk   -source [get_pins -compatibility_mode "*gen_f2m:i_ddio_o_wrapper|*|out_path.out_path_fr.fr_out_data_ddio|dataout"] -phase 90

# IO constraints
set tSKEW     0.250

set max_delay [expr " $tSKEW"]
set min_delay [expr "-$tSKEW"]

set_output_delay -clock [get_clocks "virt_lnk_f2m_clk"] -max $max_delay [get_ports "lnk_f2m_dat"]
set_output_delay -clock [get_clocks "virt_lnk_f2m_clk"] -max $max_delay [get_ports "lnk_f2m_dat"] -add_delay -clock_fall
set_output_delay -clock [get_clocks "virt_lnk_f2m_clk"] -min $min_delay [get_ports "lnk_f2m_dat"] -add_delay
set_output_delay -clock [get_clocks "virt_lnk_f2m_clk"] -min $min_delay [get_ports "lnk_f2m_dat"] -add_delay -clock_fall

# ############################################################################################################################
# ############################################################################################################################
# Asynchronous groups & False paths
# ############################################################################################################################
# ############################################################################################################################
set_clock_groups \
                -group {altera_reserved_tck} \
                -group clk_100mhz_1 \
                -group clk_100mhz_2 \
                -group clk_100mhz_3 \
                -group clk_100mhz_4 \
                -group *i_pll_sys|iopll_0|outclk0 \
                -group *i_pll_sys|iopll_0|outclk1 \
                -group *i_pll_sys|iopll_0|outclk2 \
                -group *i_pll_sys|iopll_0|outclk3 \
                -group *i_pll_sys|iopll_0|outclk4 \
                -group *i_pll_sys|iopll_0|outclk5 \
                -group lnk_f2m_clk \
                -group lnk_m2f_clk \
                -group clk_xcvr_qsfp_a \
                -group clk_xcvr_dp_a \
                -group clk_xcvr_fcm1_a \
                -group clk_xcvr_fcm2_a \
                -group gbtclk0 \
                -group gbtclk1 \
                -group fmc_clk0_m2c \
                -group fmc_clk1_m2c \
                -group clk_xcvr_ext_a \
                -group clk_xcvr_ext1 \
                -group clk_xcvr_ext3 \
                -group ext_dphs_clk1_m2c \
                -group ext_dphs_clk2_m2c \
                -group ext_dphs_clk3_m2c \
                -group ext_dp_clk_m2c \
                -group dp_aux \
                -group ext_dp_m2c[9] \
                -group gbe_rx_clk \
                -group pcie_edge_refclk \
                -group {mem_ref_clk *i_sodimm_ddr4_ctrl|emif_0_core_usr_clk *|i_sodimm_ddr4_ctrl|emif_0_phy_clk_*}\
                -group {*i_fmc_lvds_top_wrapper|i_lvds_pll|*:i_lvds_pll_ip|iopll_0|clk_core *i_fmc_lvds_top_wrapper|i_lvds_pll|*:i_lvds_pll_ip|iopll_0|clk_fast}\
                -group {*i_ext_lvds_top_wrapper|i_lvds_pll|*:i_lvds_pll_ip|iopll_0|clk_core *i_ext_lvds_top_wrapper|i_lvds_pll|*:i_lvds_pll_ip|iopll_0|clk_fast}\
                -group *i_xcvr_test_wrapper|i_xcvr_phy_wrapper|*:i_xcvr_channel|xcvr_native_a10_0|tx_pma_clk \
                -group *i_xcvr_test_wrapper|i_xcvr_phy_wrapper|*:i_xcvr_channel|xcvr_native_a10_0|rx_pma_clk \
                -asynchronous
                
# ############################################################################################################################################
# ############################################################################################################################################
post_message -type info "# Finished SDC constraints of RxC modules."
post_message -type info "############################################################################################################################################"
# ############################################################################################################################################
# ############################################################################################################################################
