# Version 13
post_message "############################################################################################"
post_message "## Pre incremental backup starts..."
post_message "############################################################################################"

# #################################################################################################################################
# #################################################################################################################################
# Edit variables in this file to customize behavior of auto backup
# #################################################################################################################################
# #################################################################################################################################
source "autobkp_cfg.tcl"
# #################################################################################################################################
# #################################################################################################################################
# #################################################################################################################################
global quartus

set project [lindex $quartus(args) 1]
set revision [lindex $quartus(args) 2]

# Temporary names
set base_pre_name           "bk_pre_$revision"
set base_pre_name_comment   "${base_pre_name}_comment.txt"

# ################################################################################################################################
# If file bk_options_file exist, source it to take into account special cases.
# File is sourced then deleted.
# ################################################################################################################################
set bk_options_file "autobkp_pre_options.tcl"
if { [catch {
    if {[file exists $bk_options_file]} {
        post_message -type info "File $bk_options_file exists. Source it."
        source $bk_options_file
        file delete -force $bk_options_file
    }            
} res ] } {
    post_message -type warning "Problem while processing $bk_options_file: $res"
}
# ################################################################################################################################

# ################################################################################################################################
# Exit if path to 7zip is incorrect
# ################################################################################################################################
if {![file exists $zipexe]} {
    post_message -type warning "Unable to find 7zip at $zipexe. Exit autobkp."
    return -1
}   
# ################################################################################################################################
# ################################################################################################################################

proc prompt_for_notepad {filename} {
    # Initialize the Tk library
    init_tk
    
    # Init some variables
    global open_notepad
    global toplevel_closed
    
    set open_notepad 0
    set toplevel_closed 0
    set help_lbl1 "Click on the button below to open NOTEPAD and edit the comment for the current run.
    If not, the window will close automatically and the compilation start."
    set help_lbl2 "Close the notepad window to start the compilation."

    # Create a top level and modify title
    toplevel .top -width 512 -heigh 128
    wm title .top "Add autobackup comment to log for this run..."
    wm attributes .top -topmost true
    wm attributes .top -toolwindow true

    # Little proc to center the toplevel
    proc center_window {w {offx 0} {offy 0} {divx 2} {divy 2}} {
        set width  [winfo reqwidth $w ]
        set height [winfo reqheight $w]
        set x [expr { ( [winfo vrootwidth  $w] - $width  ) / $divx + $offx}]
        set y [expr { ( [winfo vrootheight $w] - $height ) / $divy + $offy}]
        wm geometry $w ${width}x${height}+${x}+${y}
    }

    # Add widgets
    label .top.lbl -text $help_lbl1  -padx 8 -pady 8 -anchor n
    button .top.btn -text "..." -padx 8 -pady 8 -command {set open_notepad 1} -anchor s
    pack .top.lbl .top.btn -padx 8 -pady 8

    # Center toplevel and detect destroy event
    center_window .top
    bind .top <Destroy> {set toplevel_closed 1}

    # Ring a bell
    bell -displayof .top.btn -nice

    # Wait for user action
    set timeout 15
    for {set i 0} {$i<[expr {$timeout*10}] && $open_notepad==0 && $toplevel_closed==0} {incr i} {
        # Wait for a while
        after 100
        
        # Change button text
        set time_left [expr {($timeout*10-$i)/10}]
        .top.btn configure -text "Open NOTEPAD now (${time_left}s left)"
        update
        
        # User want to open_notepad ?
        if {$open_notepad==1} {
            center_window .top 0 0 2 16
            .top.lbl configure -text $help_lbl2 -font {-size 12}
            destroy .top.btn
            update
            exec notepad ${filename}
        }
    }

    destroy .top
}

# Create / Open .txt file for comments
if { [catch {
    if {![file exists ${base_pre_name_comment}]} {
        set fh [open ${base_pre_name_comment} w ]
        
        puts $fh "Comment:"
        puts $fh " - add comment here and describe updates/changes on project -"

        close $fh
    }            

    if { $enable_log_prompt_pre } {
        prompt_for_notepad ${base_pre_name_comment}
    }
} res ] } {
    post_message -type warning $res
    post_message -type warning "## Problem while creating/opening ${base_pre_name_comment} file. Skipping..."
}

# Create QAR
if { [catch {
    project_open -revision $revision $project
    project_archive -use_file_set basic $base_pre_name.qar -overwrite
    project_close
} res ] } {
    post_message -type warning $res
    post_message -type warning "## $base_pre_name.qar cannot be created."
} else {
    post_message "## Successfully archived project $base_pre_name.qar"
}

post_message "############################################################################################"
post_message "## Pre incremental backup ends."
post_message "############################################################################################"


