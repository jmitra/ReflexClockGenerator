post_message "##################################################################################################"
post_message "# Processing assignments file starts."

#============================================================================================================================
# Clock
#============================================================================================================================
set_location_assignment PIN_AJ5 -to clk_100mhz_1    -tag rxc_pin_clk
set_location_assignment PIN_Y2 -to clk_100mhz_2     -tag rxc_pin_clk
set_location_assignment PIN_C24 -to clk_100mhz_3    -tag rxc_pin_clk
set_location_assignment PIN_AR11 -to clk_100mhz_4   -tag rxc_pin_clk

#============================================================================================================================
# DisplayPort connector
#============================================================================================================================
set_location_assignment PIN_AJ28 -to clk_xcvr_dp_a(n)                               -tag rxc_pin_dp
set_location_assignment PIN_AJ29 -to clk_xcvr_dp_a                                  -tag rxc_pin_dp
set_location_assignment PIN_F23 -to dp_aux(n)                                       -tag rxc_pin_dp
set_location_assignment PIN_F24 -to dp_aux                                          -tag rxc_pin_dp
set_location_assignment PIN_E25 -to dp_hpd                                          -tag rxc_pin_dp
set_location_assignment PIN_AW32 -to dp_tx[0](n)                                    -tag rxc_pin_dp
set_location_assignment PIN_AW33 -to dp_tx[0]                                       -tag rxc_pin_dp
set_location_assignment PIN_AW36 -to dp_tx[1](n)                                    -tag rxc_pin_dp
set_location_assignment PIN_AW37 -to dp_tx[1]                                       -tag rxc_pin_dp
set_location_assignment PIN_AV34 -to dp_tx[2](n)                                    -tag rxc_pin_dp
set_location_assignment PIN_AV35 -to dp_tx[2]                                       -tag rxc_pin_dp
set_location_assignment PIN_AV38 -to dp_tx[3](n)                                    -tag rxc_pin_dp
set_location_assignment PIN_AV39 -to dp_tx[3]                                       -tag rxc_pin_dp
set_instance_assignment -name IO_STANDARD LVDS -to dp_aux                           -tag rxc_pin_dp
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_dp_a                    -tag rxc_pin_dp
set_instance_assignment -name INPUT_TERMINATION "Differential" -to dp_aux           -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_dp_a    -tag rxc_pin_fmc

#============================================================================================================================
# PCIe connector
#============================================================================================================================
set_location_assignment PIN_AL28 -to pcie_edge_refclk(n)                                -tag rxc_pin_pcie
set_location_assignment PIN_AL29 -to pcie_edge_refclk                                   -tag rxc_pin_pcie
set_location_assignment PIN_AN32 -to pcie_edge_rx[0](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AN33 -to pcie_edge_rx[0]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AM30 -to pcie_edge_rx[1](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AM31 -to pcie_edge_rx[1]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AM34 -to pcie_edge_rx[2](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AM35 -to pcie_edge_rx[2]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AL32 -to pcie_edge_rx[3](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AL33 -to pcie_edge_rx[3]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AK30 -to pcie_edge_rx[4](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AK31 -to pcie_edge_rx[4]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AK34 -to pcie_edge_rx[5](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AK35 -to pcie_edge_rx[5]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AJ32 -to pcie_edge_rx[6](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AJ33 -to pcie_edge_rx[6]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AH30 -to pcie_edge_rx[7](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AH31 -to pcie_edge_rx[7]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AU36 -to pcie_edge_tx[0](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AU37 -to pcie_edge_tx[0]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AT34 -to pcie_edge_tx[1](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AT35 -to pcie_edge_tx[1]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AT38 -to pcie_edge_tx[2](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AT39 -to pcie_edge_tx[2]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AR36 -to pcie_edge_tx[3](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AR37 -to pcie_edge_tx[3]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AP34 -to pcie_edge_tx[4](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AP35 -to pcie_edge_tx[4]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AP38 -to pcie_edge_tx[5](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AP39 -to pcie_edge_tx[5]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AN36 -to pcie_edge_tx[6](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AN37 -to pcie_edge_tx[6]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AM38 -to pcie_edge_tx[7](n)                                 -tag rxc_pin_pcie
set_location_assignment PIN_AM39 -to pcie_edge_tx[7]                                    -tag rxc_pin_pcie
set_location_assignment PIN_AW16 -to pcie_perst_n                                       -tag rxc_pin_pcie
set_location_assignment PIN_M25 -to pcie_smbclk                                         -tag rxc_pin_pcie
set_location_assignment PIN_L25 -to pcie_smbdat                                         -tag rxc_pin_pcie
set_location_assignment PIN_K26 -to pcie_wake_n                                         -tag rxc_pin_pcie
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_pcie_a                      -tag rxc_pin_pcie
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_pcie_a      -tag rxc_pin_fmc

#============================================================================================================================
# QSFP connector
#============================================================================================================================
set_location_assignment PIN_G28 -to clk_xcvr_qsfp_a(n)                              -tag rxc_pin_qsfp
set_location_assignment PIN_G29 -to clk_xcvr_qsfp_a                                 -tag rxc_pin_qsfp
set_location_assignment PIN_F19 -to qsfp_int_n                                      -tag rxc_pin_qsfp
set_location_assignment PIN_C18 -to qsfp_lowp_n                                     -tag rxc_pin_qsfp
set_location_assignment PIN_F18 -to qsfp_prsnt_n                                    -tag rxc_pin_qsfp
set_location_assignment PIN_D20 -to qsfp_reset_n                                    -tag rxc_pin_qsfp
set_location_assignment PIN_F30 -to qsfp_rx[0](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_F31 -to qsfp_rx[0]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_E32 -to qsfp_rx[1](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_E33 -to qsfp_rx[1]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_D30 -to qsfp_rx[2](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_D31 -to qsfp_rx[2]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_B30 -to qsfp_rx[3](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_B31 -to qsfp_rx[3]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_E21 -to qsfp_scl                                        -tag rxc_pin_qsfp
set_location_assignment PIN_E20 -to qsfp_sda                                        -tag rxc_pin_qsfp
set_location_assignment PIN_D21 -to qsfp_sel_n                                      -tag rxc_pin_qsfp
set_location_assignment PIN_A36 -to qsfp_tx[0](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_A37 -to qsfp_tx[0]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_B34 -to qsfp_tx[1](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_B35 -to qsfp_tx[1]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_C32 -to qsfp_tx[2](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_C33 -to qsfp_tx[2]                                      -tag rxc_pin_qsfp
set_location_assignment PIN_A32 -to qsfp_tx[3](n)                                   -tag rxc_pin_qsfp
set_location_assignment PIN_A33 -to qsfp_tx[3]                                      -tag rxc_pin_qsfp
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_qsfp_a                  -tag rxc_pin_qsfp
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_qsfp_a  -tag rxc_pin_fmc

#============================================================================================================================
# Extension connector
#============================================================================================================================
set_location_assignment PIN_N28 -to clk_xcvr_ext_a(n)                                   -tag rxc_pin_ext
set_location_assignment PIN_N29 -to clk_xcvr_ext_a                                      -tag rxc_pin_ext
set_location_assignment PIN_U28 -to clk_xcvr_ext1(n)                                    -tag rxc_pin_ext
set_location_assignment PIN_U29 -to clk_xcvr_ext1                                       -tag rxc_pin_ext
set_location_assignment PIN_J28 -to clk_xcvr_ext3(n)                                    -tag rxc_pin_ext
set_location_assignment PIN_J29 -to clk_xcvr_ext3                                       -tag rxc_pin_ext
set_location_assignment PIN_AB6 -to ext_dp_clk_m2c(n)                                   -tag rxc_pin_ext
set_location_assignment PIN_AB5 -to ext_dp_clk_m2c                                      -tag rxc_pin_ext
set_location_assignment PIN_AA3 -to ext_dp_c2m[0](n)                                    -tag rxc_pin_ext                    
set_location_assignment PIN_AA2 -to ext_dp_c2m[0]                                       -tag rxc_pin_ext                    
set_location_assignment PIN_AC4 -to ext_dp_m2c[0](n)                                    -tag rxc_pin_ext                    
set_location_assignment PIN_AC3 -to ext_dp_m2c[0]                                       -tag rxc_pin_ext                    
set_location_assignment PIN_AB2 -to ext_dp_c2m[1](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AB1 -to ext_dp_c2m[1]                                       -tag rxc_pin_ext
set_location_assignment PIN_W1 -to ext_dp_m2c[1](n)                                     -tag rxc_pin_ext
set_location_assignment PIN_Y1 -to ext_dp_m2c[1]                                        -tag rxc_pin_ext
set_location_assignment PIN_AD3 -to ext_dp_c2m[2](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AC2 -to ext_dp_c2m[2]                                       -tag rxc_pin_ext
set_location_assignment PIN_Y7 -to ext_dp_m2c[2](n)                                     -tag rxc_pin_ext
set_location_assignment PIN_Y6 -to ext_dp_m2c[2]                                        -tag rxc_pin_ext
set_location_assignment PIN_AC1 -to ext_dp_c2m[3](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AD1 -to ext_dp_c2m[3]                                       -tag rxc_pin_ext
set_location_assignment PIN_AA8 -to ext_dp_m2c[3](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AA7 -to ext_dp_m2c[3]                                       -tag rxc_pin_ext
set_location_assignment PIN_AE2 -to ext_dp_c2m[4](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AE1 -to ext_dp_c2m[4]                                       -tag rxc_pin_ext
set_location_assignment PIN_Y5 -to ext_dp_m2c[4](n)                                     -tag rxc_pin_ext
set_location_assignment PIN_AA5 -to ext_dp_m2c[4]                                       -tag rxc_pin_ext
set_location_assignment PIN_AD5 -to ext_dp_c2m[5](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AD4 -to ext_dp_c2m[5]                                       -tag rxc_pin_ext
set_location_assignment PIN_AA4 -to ext_dp_m2c[5](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AB4 -to ext_dp_m2c[5]                                       -tag rxc_pin_ext
set_location_assignment PIN_AG1 -to ext_dp_c2m[6](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AH1 -to ext_dp_c2m[6]                                       -tag rxc_pin_ext
set_location_assignment PIN_AB10 -to ext_dp_m2c[6](n)                                   -tag rxc_pin_ext
set_location_assignment PIN_AB9 -to ext_dp_m2c[6]                                       -tag rxc_pin_ext
set_location_assignment PIN_AC6 -to ext_dp_c2m[7](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AD6 -to ext_dp_c2m[7]                                       -tag rxc_pin_ext
set_location_assignment PIN_AB11 -to ext_dp_m2c[7](n)                                   -tag rxc_pin_ext
set_location_assignment PIN_AA10 -to ext_dp_m2c[7]                                      -tag rxc_pin_ext
set_location_assignment PIN_AB7 -to ext_dp_c2m[8](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AC7 -to ext_dp_c2m[8]                                       -tag rxc_pin_ext
set_location_assignment PIN_Y10 -to ext_dp_m2c[8](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AA9 -to ext_dp_m2c[8]                                       -tag rxc_pin_ext
set_location_assignment PIN_AF2 -to ext_dp_c2m[9](n)                                    -tag rxc_pin_ext
set_location_assignment PIN_AG2 -to ext_dp_c2m[9]                                       -tag rxc_pin_ext
set_location_assignment PIN_W8 -to ext_dp_m2c[9](n)                                     -tag rxc_pin_ext
set_location_assignment PIN_Y8 -to ext_dp_m2c[9]                                        -tag rxc_pin_ext
set_location_assignment PIN_W28 -to ext_dphs_clk1_m2c(n)                                -tag rxc_pin_ext
set_location_assignment PIN_W29 -to ext_dphs_clk1_m2c                                   -tag rxc_pin_ext
set_location_assignment PIN_R28 -to ext_dphs_clk2_m2c(n)                                -tag rxc_pin_ext
set_location_assignment PIN_R29 -to ext_dphs_clk2_m2c                                   -tag rxc_pin_ext
set_location_assignment PIN_L28 -to ext_dphs_clk3_m2c(n)                                -tag rxc_pin_ext
set_location_assignment PIN_L29 -to ext_dphs_clk3_m2c                                   -tag rxc_pin_ext
set_location_assignment PIN_AA36 -to ext_dphs_c2m[0](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_AA37 -to ext_dphs_c2m[0]                                    -tag rxc_pin_ext
set_location_assignment PIN_AA32 -to ext_dphs_m2c[0](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_AA33 -to ext_dphs_m2c[0]                                    -tag rxc_pin_ext
set_location_assignment PIN_M38 -to ext_dphs_c2m[9](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_M39 -to ext_dphs_c2m[9]                                     -tag rxc_pin_ext
set_location_assignment PIN_R32 -to ext_dphs_m2c[9](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_R33 -to ext_dphs_m2c[9]                                     -tag rxc_pin_ext
set_location_assignment PIN_L36 -to ext_dphs_c2m[10](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_L37 -to ext_dphs_c2m[10]                                    -tag rxc_pin_ext
set_location_assignment PIN_P34 -to ext_dphs_m2c[10](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_P35 -to ext_dphs_m2c[10]                                    -tag rxc_pin_ext
set_location_assignment PIN_K38 -to ext_dphs_c2m[11](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_K39 -to ext_dphs_c2m[11]                                    -tag rxc_pin_ext
set_location_assignment PIN_P30 -to ext_dphs_m2c[11](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_P31 -to ext_dphs_m2c[11]                                    -tag rxc_pin_ext
set_location_assignment PIN_J36 -to ext_dphs_c2m[12](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_J37 -to ext_dphs_c2m[12]                                    -tag rxc_pin_ext
set_location_assignment PIN_N32 -to ext_dphs_m2c[12](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_N33 -to ext_dphs_m2c[12]                                    -tag rxc_pin_ext
set_location_assignment PIN_H38 -to ext_dphs_c2m[13](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_H39 -to ext_dphs_c2m[13]                                    -tag rxc_pin_ext
set_location_assignment PIN_M34 -to ext_dphs_m2c[13](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_M35 -to ext_dphs_m2c[13]                                    -tag rxc_pin_ext
set_location_assignment PIN_G36 -to ext_dphs_c2m[14](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_G37 -to ext_dphs_c2m[14]                                    -tag rxc_pin_ext
set_location_assignment PIN_M30 -to ext_dphs_m2c[14](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_M31 -to ext_dphs_m2c[14]                                    -tag rxc_pin_ext
set_location_assignment PIN_F38 -to ext_dphs_c2m[15](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_F39 -to ext_dphs_c2m[15]                                    -tag rxc_pin_ext
set_location_assignment PIN_L32 -to ext_dphs_m2c[15](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_L33 -to ext_dphs_m2c[15]                                    -tag rxc_pin_ext
set_location_assignment PIN_F34 -to ext_dphs_c2m[16](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_F35 -to ext_dphs_c2m[16]                                    -tag rxc_pin_ext
set_location_assignment PIN_K34 -to ext_dphs_m2c[16](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_K35 -to ext_dphs_m2c[16]                                    -tag rxc_pin_ext
set_location_assignment PIN_E36 -to ext_dphs_c2m[17](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_E37 -to ext_dphs_c2m[17]                                    -tag rxc_pin_ext
set_location_assignment PIN_K30 -to ext_dphs_m2c[17](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_K31 -to ext_dphs_m2c[17]                                    -tag rxc_pin_ext
set_location_assignment PIN_D38 -to ext_dphs_c2m[18](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_D39 -to ext_dphs_c2m[18]                                    -tag rxc_pin_ext
set_location_assignment PIN_J32 -to ext_dphs_m2c[18](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_J33 -to ext_dphs_m2c[18]                                    -tag rxc_pin_ext
set_location_assignment PIN_Y38 -to ext_dphs_c2m[1](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_Y39 -to ext_dphs_c2m[1]                                     -tag rxc_pin_ext
set_location_assignment PIN_Y34 -to ext_dphs_m2c[1](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_Y35 -to ext_dphs_m2c[1]                                     -tag rxc_pin_ext
set_location_assignment PIN_D34 -to ext_dphs_c2m[19](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_D35 -to ext_dphs_c2m[19]                                    -tag rxc_pin_ext
set_location_assignment PIN_H34 -to ext_dphs_m2c[19](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_H35 -to ext_dphs_m2c[19]                                    -tag rxc_pin_ext
set_location_assignment PIN_C36 -to ext_dphs_c2m[20](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_C37 -to ext_dphs_c2m[20]                                    -tag rxc_pin_ext
set_location_assignment PIN_H30 -to ext_dphs_m2c[20](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_H31 -to ext_dphs_m2c[20]                                    -tag rxc_pin_ext
set_location_assignment PIN_B38 -to ext_dphs_c2m[21](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_B39 -to ext_dphs_c2m[21]                                    -tag rxc_pin_ext
set_location_assignment PIN_G32 -to ext_dphs_m2c[21](n)                                 -tag rxc_pin_ext
set_location_assignment PIN_G33 -to ext_dphs_m2c[21]                                    -tag rxc_pin_ext
set_location_assignment PIN_W36 -to ext_dphs_c2m[2](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_W37 -to ext_dphs_c2m[2]                                     -tag rxc_pin_ext
set_location_assignment PIN_Y30 -to ext_dphs_m2c[2](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_Y31 -to ext_dphs_m2c[2]                                     -tag rxc_pin_ext
set_location_assignment PIN_V38 -to ext_dphs_c2m[3](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_V39 -to ext_dphs_c2m[3]                                     -tag rxc_pin_ext
set_location_assignment PIN_W32 -to ext_dphs_m2c[3](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_W33 -to ext_dphs_m2c[3]                                     -tag rxc_pin_ext
set_location_assignment PIN_U36 -to ext_dphs_c2m[4](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_U37 -to ext_dphs_c2m[4]                                     -tag rxc_pin_ext
set_location_assignment PIN_V34 -to ext_dphs_m2c[4](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_V35 -to ext_dphs_m2c[4]                                     -tag rxc_pin_ext
set_location_assignment PIN_T38 -to ext_dphs_c2m[5](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_T39 -to ext_dphs_c2m[5]                                     -tag rxc_pin_ext
set_location_assignment PIN_V30 -to ext_dphs_m2c[5](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_V31 -to ext_dphs_m2c[5]                                     -tag rxc_pin_ext
set_location_assignment PIN_R36 -to ext_dphs_c2m[6](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_R37 -to ext_dphs_c2m[6]                                     -tag rxc_pin_ext
set_location_assignment PIN_U32 -to ext_dphs_m2c[6](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_U33 -to ext_dphs_m2c[6]                                     -tag rxc_pin_ext
set_location_assignment PIN_P38 -to ext_dphs_c2m[7](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_P39 -to ext_dphs_c2m[7]                                     -tag rxc_pin_ext
set_location_assignment PIN_T34 -to ext_dphs_m2c[7](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_T35 -to ext_dphs_m2c[7]                                     -tag rxc_pin_ext
set_location_assignment PIN_N36 -to ext_dphs_c2m[8](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_N37 -to ext_dphs_c2m[8]                                     -tag rxc_pin_ext
set_location_assignment PIN_T30 -to ext_dphs_m2c[8](n)                                  -tag rxc_pin_ext
set_location_assignment PIN_T31 -to ext_dphs_m2c[8]                                     -tag rxc_pin_ext
set_location_assignment PIN_AE6 -to ext_i2c_scl                                         -tag rxc_pin_ext
set_location_assignment PIN_AE5 -to ext_i2c_sda                                         -tag rxc_pin_ext
set_location_assignment PIN_AJ3 -to ext_prsnt_n                                         -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dp_clk_m2c                       -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dp_c2m[*]                        -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dp_m2c[*]                        -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dphs_clk1_m2c                    -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dphs_clk2_m2c                    -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to ext_dphs_clk3_m2c                    -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_ext_a                       -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_ext1                        -tag rxc_pin_ext
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_ext3                        -tag rxc_pin_ext
set_instance_assignment -name INPUT_TERMINATION "Differential" -to ext_dp_clk_m2c       -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to ext_dp_m2c[*]        -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to ext_dphs_clk1_m2c    -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to ext_dphs_clk2_m2c    -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to ext_dphs_clk3_m2c    -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_ext_a       -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_ext1        -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_ext3        -tag rxc_pin_fmc

#============================================================================================================================
# FMC connector
#============================================================================================================================
set_location_assignment PIN_AG28 -to clk_xcvr_fcm1_a(n)                             -tag rxc_pin_fmc
set_location_assignment PIN_AG29 -to clk_xcvr_fcm1_a                                -tag rxc_pin_fmc
set_location_assignment PIN_AC28 -to clk_xcvr_fcm2_a(n)                             -tag rxc_pin_fmc
set_location_assignment PIN_AC29 -to clk_xcvr_fcm2_a                                -tag rxc_pin_fmc
set_location_assignment PIN_AL36 -to fmc_dp_c2m[0](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AL37 -to fmc_dp_c2m[0]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AH34 -to fmc_dp_m2c[0](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AH35 -to fmc_dp_m2c[0]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AK38 -to fmc_dp_c2m[1](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AK39 -to fmc_dp_c2m[1]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AG32 -to fmc_dp_m2c[1](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AG33 -to fmc_dp_m2c[1]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AJ36 -to fmc_dp_c2m[2](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AJ37 -to fmc_dp_c2m[2]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AF30 -to fmc_dp_m2c[2](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AF31 -to fmc_dp_m2c[2]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AH38 -to fmc_dp_c2m[3](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AH39 -to fmc_dp_c2m[3]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AF34 -to fmc_dp_m2c[3](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AF35 -to fmc_dp_m2c[3]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AG36 -to fmc_dp_c2m[4](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AG37 -to fmc_dp_c2m[4]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AE32 -to fmc_dp_m2c[4](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AE33 -to fmc_dp_m2c[4]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AF38 -to fmc_dp_c2m[5](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AF39 -to fmc_dp_c2m[5]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AD30 -to fmc_dp_m2c[5](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AD31 -to fmc_dp_m2c[5]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AE36 -to fmc_dp_c2m[6](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AE37 -to fmc_dp_c2m[6]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AD34 -to fmc_dp_m2c[6](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AD35 -to fmc_dp_m2c[6]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AD38 -to fmc_dp_c2m[7](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AD39 -to fmc_dp_c2m[7]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AC32 -to fmc_dp_m2c[7](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AC33 -to fmc_dp_m2c[7]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AC36 -to fmc_dp_c2m[8](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AC37 -to fmc_dp_c2m[8]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AB30 -to fmc_dp_m2c[8](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AB31 -to fmc_dp_m2c[8]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AB38 -to fmc_dp_c2m[9](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AB39 -to fmc_dp_c2m[9]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AB34 -to fmc_dp_m2c[9](n)                               -tag rxc_pin_fmc
set_location_assignment PIN_AB35 -to fmc_dp_m2c[9]                                  -tag rxc_pin_fmc
set_location_assignment PIN_AT9  -to fmc_clk_dir                                    -tag rxc_pin_fmc
set_location_assignment PIN_AH21 -to fmc_clk0_m2c(n)                                -tag rxc_pin_fmc
set_location_assignment PIN_AJ21 -to fmc_clk0_m2c                                   -tag rxc_pin_fmc
set_location_assignment PIN_AN26 -to fmc_clk1_m2c(n)                                -tag rxc_pin_fmc
set_location_assignment PIN_AP26 -to fmc_clk1_m2c                                   -tag rxc_pin_fmc
set_location_assignment PIN_AL10 -to fmc_clk2_bidir(n)                              -tag rxc_pin_fmc
set_location_assignment PIN_AM10 -to fmc_clk2_bidir                                 -tag rxc_pin_fmc
set_location_assignment PIN_AM5  -to fmc_clk3_bidir(n)                              -tag rxc_pin_fmc
set_location_assignment PIN_AN4  -to fmc_clk3_bidir                                 -tag rxc_pin_fmc
set_location_assignment PIN_AM1  -to fmc_ha_i[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK1  -to fmc_ha_o[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AH4  -to fmc_ha_i[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AL5  -to fmc_ha_o[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AR1  -to fmc_ha_i[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AR3  -to fmc_ha_o[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AF7  -to fmc_ha_i[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AM2  -to fmc_ha_o[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AV4  -to fmc_ha_i[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AT2  -to fmc_ha_o[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AD8  -to fmc_ha_i[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU1  -to fmc_ha_o[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW6  -to fmc_ha_i[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU5  -to fmc_ha_o[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW4  -to fmc_ha_i[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU10 -to fmc_ha_i[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW9  -to fmc_ha_o[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AP1  -to fmc_ha_o[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW8  -to fmc_ha_i[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AL13 -to fmc_ha_i[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AN12 -to fmc_ha_o[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK13 -to fmc_ha_o[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AK15 -to fmc_ha_i[11]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AG14 -to fmc_ha_o[11]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AR23 -to fmc_hb_i[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AF20 -to fmc_hb_o[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AH11 -to fmc_hb_i[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AJ10 -to fmc_hb_o[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK21 -to fmc_hb_i[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AG20 -to fmc_hb_o[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW26 -to fmc_hb_i[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU24 -to fmc_hb_o[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AM22 -to fmc_hb_i[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU22 -to fmc_hb_o[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AU28 -to fmc_hb_i[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AV27 -to fmc_hb_o[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AL23 -to fmc_hb_i[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AN24 -to fmc_hb_o[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AJ26 -to fmc_hb_i[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AT26 -to fmc_hb_o[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AJ24 -to fmc_hb_i[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AG25 -to fmc_hb_i[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AH24 -to fmc_hb_o[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW28 -to fmc_hb_o[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK25 -to fmc_hb_i[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AM25 -to fmc_hb_o[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AJ1  -to fmc_la_i[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AN3  -to fmc_la_i[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AF4  -to fmc_la_o[1]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK3  -to fmc_la_i[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AF8  -to fmc_la_o[2]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AR5  -to fmc_la_o[0]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AT4  -to fmc_la_i[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AJ8  -to fmc_la_i[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AL3  -to fmc_la_o[3]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AR6  -to fmc_la_i[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AR8  -to fmc_la_o[4]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AN6  -to fmc_la_i[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AL7  -to fmc_la_o[5]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AP9  -to fmc_la_o[6]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AJ9  -to fmc_la_i[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AV9  -to fmc_la_i[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AT12 -to fmc_la_o[7]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AK11 -to fmc_la_o[8]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AG9  -to fmc_la_i[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AL8  -to fmc_la_i[11]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AN11 -to fmc_la_o[11]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AN21 -to fmc_la_i[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AM21 -to fmc_la_o[10]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AH12 -to fmc_la_o[9]                                    -tag rxc_pin_fmc
set_location_assignment PIN_AW24 -to fmc_la_i[12]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AW23 -to fmc_la_o[12]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AG12 -to fmc_la_i[13]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AF9  -to fmc_la_o[13]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AL24 -to fmc_la_i[14]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AU26 -to fmc_la_o[14]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AD10 -to fmc_la_i[15]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AH22 -to fmc_la_o[15]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AG24 -to fmc_la_i[16]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AE10 -to fmc_la_o[16]                                   -tag rxc_pin_fmc
set_location_assignment PIN_AT7  -to fmc_pg_c2m                                     -tag rxc_pin_fmc
set_location_assignment PIN_AT10 -to fmc_pg_m2c                                     -tag rxc_pin_fmc
set_location_assignment PIN_AP25 -to fmc_prsnt1_n                                   -tag rxc_pin_fmc
set_location_assignment PIN_AP23 -to fmc_scl                                        -tag rxc_pin_fmc
set_location_assignment PIN_AP24 -to fmc_sda                                        -tag rxc_pin_fmc
set_location_assignment PIN_AE28 -to gbtclk0(n)                                     -tag rxc_pin_fmc
set_location_assignment PIN_AE29 -to gbtclk0                                        -tag rxc_pin_fmc
set_location_assignment PIN_AA28 -to gbtclk1(n)                                     -tag rxc_pin_fmc
set_location_assignment PIN_AA29 -to gbtclk1                                        -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_fcm1_a                  -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to clk_xcvr_fcm2_a                  -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_clk0_m2c                     -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_clk1_m2c                     -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_clk2_bidir                   -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_clk3_bidir                   -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_ha_i[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_hb_i[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_la_i[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_ha_o[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_hb_o[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to fmc_la_o[*]                      -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to gbtclk0                          -tag rxc_pin_fmc
set_instance_assignment -name IO_STANDARD LVDS -to gbtclk1                          -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_fcm1_a  -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to clk_xcvr_fcm2_a  -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to fmc_clk0_m2c     -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to fmc_clk1_m2c     -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to gbtclk0          -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to gbtclk1          -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to fmc_hb_i[*]      -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to fmc_la_i[*]      -tag rxc_pin_fmc
set_instance_assignment -name INPUT_TERMINATION "Differential" -to fmc_la_i[*]      -tag rxc_pin_fmc

#============================================================================================================================
# SODIMM DDR4
#============================================================================================================================
set_location_assignment PIN_F5 -to mem_ref_clk(n)                                   -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E5 -to mem_ref_clk                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C1 -to mem_dq[69]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E2 -to mem_dq[70]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G4 -to mem_dq[66]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E1 -to mem_dq[65]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G2 -to mem_dq[64]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F4 -to mem_dq[67]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D1 -to mem_dq[68]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F2 -to mem_dq[71]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F13 -to mem_dq[40]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D13 -to mem_dq[41]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H12 -to mem_dq[42]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C14 -to mem_dq[43]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G14 -to mem_dq[44]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C13 -to mem_dq[45]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H14 -to mem_dq[46]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D14 -to mem_dq[47]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M9 -to mem_dq[48]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N9 -to mem_dq[49]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R10 -to mem_dq[50]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R11 -to mem_dq[51]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L9 -to mem_dq[52]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L8 -to mem_dq[53]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P9 -to mem_dq[54]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P11 -to mem_dq[55]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J3 -to mem_dq[16]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H1 -to mem_dq[17]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J5 -to mem_dq[18]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J4 -to mem_dq[19]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K3 -to mem_dq[20]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J1 -to mem_dq[21]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H4 -to mem_dq[22]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H3 -to mem_dq[23]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M1 -to mem_dq[8]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R3 -to mem_dq[9]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N3 -to mem_dq[10]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N4 -to mem_dq[11]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P3 -to mem_dq[12]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R2 -to mem_dq[13]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M2 -to mem_dq[14]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P1 -to mem_dq[15]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_V7 -to mem_dq[0]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_W6 -to mem_dq[1]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R5 -to mem_dq[2]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P6 -to mem_dq[3]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_V6 -to mem_dq[4]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_W5 -to mem_dq[5]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U7 -to mem_dq[6]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_T5 -to mem_dq[7]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L5 -to mem_dq[24]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K5 -to mem_dq[25]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M5 -to mem_dq[26]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K7 -to mem_dq[27]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L4 -to mem_dq[28]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M4 -to mem_dq[29]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K6 -to mem_dq[30]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N7 -to mem_dq[31]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G12 -to mem_dq[32]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C12 -to mem_dq[33]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C11 -to mem_dq[34]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E10 -to mem_dq[35]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G11 -to mem_dq[36]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E11 -to mem_dq[37]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D11 -to mem_dq[38]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D10 -to mem_dq[39]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_T8 -to mem_dq[56]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_T9 -to mem_dq[57]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_V9 -to mem_dq[58]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_V8 -to mem_dq[59]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R7 -to mem_dq[60]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R6 -to mem_dq[61]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_V11 -to mem_dq[62]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U11 -to mem_dq[63]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H6 -to mem_a[0]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J6 -to mem_a[1]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G7 -to mem_a[2]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H7 -to mem_a[3]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G5 -to mem_a[4]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G6 -to mem_a[5]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J8 -to mem_a[6]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K8 -to mem_a[7]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F9 -to mem_a[8]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G9 -to mem_a[9]                                         -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H8 -to mem_a[10]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H9 -to mem_a[11]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E6 -to mem_a[12]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D4 -to mem_a[13]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D5 -to mem_a[14]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E7 -to mem_a[15]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F7 -to mem_a[16]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J10 -to mem_act_n[0]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P5 -to mem_alert_n[0]                                   -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D3 -to mem_ba[0]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C3 -to mem_ba[1]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C4 -to mem_bg[0]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M11 -to mem_bg[1]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K11 -to mem_ck_n[0]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J11 -to mem_ck[0]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F8 -to mem_ck_n[1]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E8 -to mem_ck[1]                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M12 -to mem_cke[0]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_C6 -to mem_cke[1]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_J9 -to mem_cs_n[0]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_B5 -to mem_cs_n[1]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D8 -to mem_cs_n[2]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_B7 -to mem_cs_n[3]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_T7 -to mem_dbi_n[0]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R1 -to mem_dbi_n[1]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L3 -to mem_dbi_n[2]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L7 -to mem_dbi_n[3]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H11 -to mem_dbi_n[4]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_H13 -to mem_dbi_n[5]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N8 -to mem_dbi_n[6]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U10 -to mem_dbi_n[7]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G1 -to mem_dbi_n[8]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U6 -to mem_dqs_n[0]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U5 -to mem_dqs[0]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N2 -to mem_dqs_n[1]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N1 -to mem_dqs[1]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K2 -to mem_dqs_n[2]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_K1 -to mem_dqs[2]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M7 -to mem_dqs_n[3]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M6 -to mem_dqs[3]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_G10 -to mem_dqs_n[4]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F10 -to mem_dqs[4]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E13 -to mem_dqs_n[5]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E12 -to mem_dqs[5]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_R8 -to mem_dqs_n[6]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P8 -to mem_dqs[6]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_W10 -to mem_dqs_n[7]                                    -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_W9 -to mem_dqs[7]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_F3 -to mem_dqs_n[8]                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_E3 -to mem_dqs[8]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P14 -to mem_event_n                                     -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_M10 -to mem_odt[0]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_B6 -to mem_odt[1]                                       -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L10 -to mem_par[0]                                      -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_N12 -to mem_reset_n[0]                                  -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_U9 -to mem_scl                                          -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_P4 -to mem_sda                                          -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_L2 -to oct_rzqin                                        -tag rxc_pin_sodimm_ddr4
set_location_assignment PIN_D6 -to oct_rzqin1                                       -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2V" -to mem_scl                        -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2V" -to mem_sda                        -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2V" -to mem_event_n                    -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_rzqin                 -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_rzqin1                -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_rzqin2                -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to oct_rzqin3                -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_cs_n[1]               -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_cs_n[2]               -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_cs_n[3]               -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_odt[1]                -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_ck[1]                 -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_ck_n[1]               -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "1.2-V POD" -to mem_cke[1]                -tag rxc_pin_sodimm_ddr4
set_instance_assignment -name IO_STANDARD "LVDS" -to mem_ref_clk                    -tag rxc_pin_sodimm_ddr4

#============================================================================================================================
# SI5341
#============================================================================================================================
set_location_assignment PIN_H23 -to si5341_fdec    -tag rxc_pin_si5341
set_location_assignment PIN_E22 -to si5341_finc    -tag rxc_pin_si5341
set_location_assignment PIN_F22 -to si5341_i2c_sel -tag rxc_pin_si5341
set_location_assignment PIN_J23 -to si5341_intr_n  -tag rxc_pin_si5341
set_location_assignment PIN_F20 -to si5341_lol_n   -tag rxc_pin_si5341
set_location_assignment PIN_J26 -to si5341_rst_n   -tag rxc_pin_si5341
set_location_assignment PIN_H24 -to si5341_scl     -tag rxc_pin_si5341
set_location_assignment PIN_J24 -to si5341_sda     -tag rxc_pin_si5341
set_location_assignment PIN_J25 -to si5341_sync_n  -tag rxc_pin_si5341

#============================================================================================================================
# PowerMonitor
#============================================================================================================================
set_location_assignment PIN_AG15 -to pwr_0v9_salrt -tag rxc_pin_power
set_location_assignment PIN_AJ14 -to pwr_0v9_scl   -tag rxc_pin_power
set_location_assignment PIN_AF15 -to pwr_0v9_sda   -tag rxc_pin_power
#set_location_assignment PIN_AR25 -to pwr_0v9_sysg  -tag rxc_pin_power

#============================================================================================================================
# User interface
#============================================================================================================================
set_location_assignment PIN_K23 -to bp_n[1]           -tag rxc_pin_user_if
set_location_assignment PIN_J19 -to bp_n[2]           -tag rxc_pin_user_if
set_location_assignment PIN_L20 -to bp_n[3]           -tag rxc_pin_user_if
set_location_assignment PIN_H21 -to bp_n[4]           -tag rxc_pin_user_if
set_location_assignment PIN_L23 -to dipswitch_a[1]    -tag rxc_pin_user_if
set_location_assignment PIN_M22 -to dipswitch_a[2]    -tag rxc_pin_user_if
set_location_assignment PIN_L22 -to dipswitch_a[3]    -tag rxc_pin_user_if
set_location_assignment PIN_M21 -to dipswitch_a[4]    -tag rxc_pin_user_if
set_location_assignment PIN_N20 -to dipswitch_b[1]    -tag rxc_pin_user_if
set_location_assignment PIN_K22 -to dipswitch_b[2]    -tag rxc_pin_user_if
set_location_assignment PIN_K21 -to dipswitch_b[3]    -tag rxc_pin_user_if
set_location_assignment PIN_K20 -to dipswitch_b[4]    -tag rxc_pin_user_if
set_location_assignment PIN_M24 -to lcd_i2c_scl       -tag rxc_pin_user_if
set_location_assignment PIN_M20 -to lcd_i2c_sda       -tag rxc_pin_user_if
set_location_assignment PIN_P20 -to led_fav_blue      -tag rxc_pin_user_if
set_location_assignment PIN_N22 -to led_fav_green     -tag rxc_pin_user_if
set_location_assignment PIN_N23 -to led_fav_red       -tag rxc_pin_user_if
set_location_assignment PIN_J18 -to led_usr_green1_n  -tag rxc_pin_user_if
set_location_assignment PIN_H18 -to led_usr_green2_n  -tag rxc_pin_user_if
set_location_assignment PIN_G17 -to led_usr_orange1_n -tag rxc_pin_user_if
set_location_assignment PIN_H19 -to led_usr_red1_n    -tag rxc_pin_user_if

#============================================================================================================================
# FPGA <=> MAX interconnect
#============================================================================================================================
set_location_assignment PIN_F17 -to lnk_f2m_clk                 -tag rxc_pin_f2m
set_location_assignment PIN_E17 -to lnk_m2f_clk                 -tag rxc_pin_f2m
set_location_assignment PIN_E18 -to lnk_f2m_rdy                 -tag rxc_pin_f2m
set_location_assignment PIN_D18 -to lnk_m2f_dat                 -tag rxc_pin_f2m
set_location_assignment PIN_D19 -to lnk_f2m_dat                 -tag rxc_pin_f2m
set_location_assignment PIN_C19 -to lnk_m2f_rdy                 -tag rxc_pin_f2m
set_location_assignment PIN_AH18 -to fpp_data[0]                -tag rxc_pin_f2m
set_location_assignment PIN_AJ18 -to fpp_data[1]                -tag rxc_pin_f2m
set_location_assignment PIN_AH17 -to fpp_data[2]                -tag rxc_pin_f2m
set_location_assignment PIN_AJ16 -to fpp_data[3]                -tag rxc_pin_f2m
set_location_assignment PIN_AK17 -to fpp_data[4]                -tag rxc_pin_f2m
set_location_assignment PIN_AK16 -to fpp_data[5]                -tag rxc_pin_f2m
set_location_assignment PIN_AK18 -to fpp_data[6]                -tag rxc_pin_f2m
set_location_assignment PIN_AL17 -to fpp_data[7]                -tag rxc_pin_f2m
set_location_assignment PIN_AH19 -to fpp_data[8]                -tag rxc_pin_f2m
set_location_assignment PIN_AJ19 -to fpp_data[9]                -tag rxc_pin_f2m
set_location_assignment PIN_AL19 -to fpp_data[10]               -tag rxc_pin_f2m
set_location_assignment PIN_AL18 -to fpp_data[11]               -tag rxc_pin_f2m
set_location_assignment PIN_AM17 -to fpp_data[12]               -tag rxc_pin_f2m
set_location_assignment PIN_AN17 -to fpp_data[13]               -tag rxc_pin_f2m
set_location_assignment PIN_AM20 -to fpp_data[14]               -tag rxc_pin_f2m
set_location_assignment PIN_AM19 -to fpp_data[15]               -tag rxc_pin_f2m
set_location_assignment PIN_AM16 -to fpp_data[16]               -tag rxc_pin_f2m
set_location_assignment PIN_AN16 -to fpp_data[17]               -tag rxc_pin_f2m
set_location_assignment PIN_AP16 -to fpp_data[18]               -tag rxc_pin_f2m
set_location_assignment PIN_AR16 -to fpp_data[19]               -tag rxc_pin_f2m
set_location_assignment PIN_AN18 -to fpp_data[20]               -tag rxc_pin_f2m
set_location_assignment PIN_AP18 -to fpp_data[21]               -tag rxc_pin_f2m
set_location_assignment PIN_AR18 -to fpp_data[22]               -tag rxc_pin_f2m
set_location_assignment PIN_AT18 -to fpp_data[23]               -tag rxc_pin_f2m
set_location_assignment PIN_AR17 -to fpp_data[24]               -tag rxc_pin_f2m
set_location_assignment PIN_AT17 -to fpp_data[25]               -tag rxc_pin_f2m
set_location_assignment PIN_AT19 -to fpp_data[26]               -tag rxc_pin_f2m
set_location_assignment PIN_AU19 -to fpp_data[27]               -tag rxc_pin_f2m
set_location_assignment PIN_AT20 -to fpp_data[28]               -tag rxc_pin_f2m
set_location_assignment PIN_AU20 -to fpp_data[29]               -tag rxc_pin_f2m
set_location_assignment PIN_AU17 -to fpp_data[30]               -tag rxc_pin_f2m
set_location_assignment PIN_AU16 -to fpp_data[31]               -tag rxc_pin_f2m

#============================================================================================================================
# Misc.
#============================================================================================================================
set_location_assignment PIN_L24 -to fan_cde_n                   -tag rxc_pin_misc
set_location_assignment PIN_AJ6 -to clk_fpga_out(n)             -tag rxc_pin_misc
set_location_assignment PIN_AK6 -to clk_fpga_out                -tag rxc_pin_misc
set_instance_assignment -name IO_STANDARD LVDS -to clk_fpga_out -tag rxc_pin_misc

#============================================================================================================================
# FPGA configuration
#============================================================================================================================
set_location_assignment PIN_AP19 -to fpga_avst_valid   -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AP20 -to fpga_clkusr       -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AU21 -to fpga_crc_error    -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AV22 -to fpga_cvp_confdone -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AW20 -to fpga_init_done    -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AV19 -to fpga_pr_done      -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AV17 -to fpga_pr_error     -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AV16 -to fpga_pr_ready     -tag rxc_pin_fpga_cfg
set_location_assignment PIN_AR20 -to fpga_pr_request   -tag rxc_pin_fpga_cfg

#============================================================================================================================
# Gigabit Ethernet
#============================================================================================================================
set_location_assignment PIN_B26 -to gbe_int_n   -tag rxc_pin_gbe
set_location_assignment PIN_A26 -to gbe_mdc     -tag rxc_pin_gbe
set_location_assignment PIN_A25 -to gbe_mdio    -tag rxc_pin_gbe
set_location_assignment PIN_C26 -to gbe_reset_n -tag rxc_pin_gbe
set_location_assignment PIN_A24 -to gbe_rx_clk  -tag rxc_pin_gbe
set_location_assignment PIN_B24 -to gbe_rx_ctrl -tag rxc_pin_gbe
set_location_assignment PIN_A23 -to gbe_rxd[0]  -tag rxc_pin_gbe
set_location_assignment PIN_D23 -to gbe_rxd[1]  -tag rxc_pin_gbe
set_location_assignment PIN_E23 -to gbe_rxd[2]  -tag rxc_pin_gbe
set_location_assignment PIN_A22 -to gbe_rxd[3]  -tag rxc_pin_gbe
set_location_assignment PIN_C22 -to gbe_tx_clk  -tag rxc_pin_gbe
set_location_assignment PIN_C25 -to gbe_tx_ctrl -tag rxc_pin_gbe
set_location_assignment PIN_B20 -to gbe_txd[0]  -tag rxc_pin_gbe
set_location_assignment PIN_A20 -to gbe_txd[1]  -tag rxc_pin_gbe
set_location_assignment PIN_C21 -to gbe_txd[2]  -tag rxc_pin_gbe
set_location_assignment PIN_B21 -to gbe_txd[3]  -tag rxc_pin_gbe

#============================================================================================================================
# XCVR settings
#============================================================================================================================
proc xcvr_set_rx {pin tag} {
    set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to $pin -tag $tag
    set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to $pin -tag $tag

    set_instance_assignment -name XCVR_A10_RX_TERM_SEL R_R1 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP5 RADP_DFE_FXTAP5_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP4 RADP_DFE_FXTAP4_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP1 RADP_DFE_FXTAP1_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP7 RADP_DFE_FXTAP7_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP6 RADP_DFE_FXTAP6_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP3 RADP_DFE_FXTAP3_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_DFE_FXTAP2 RADP_DFE_FXTAP2_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ONE_STAGE_ENABLE NON_S1_MODE -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_CTLE_ACGAIN_4S RADP_CTLE_ACGAIN_4S_0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_EQ_DC_GAIN_TRIM NO_DC_GAIN -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_RX_ADP_VGA_SEL RADP_VGA_SEL_4 -to $pin -tag $tag

    post_message -type info "# XCVR RX settings applied to pin=$pin (with tag=$tag)."
}

proc xcvr_set_tx {pin tag} {
    set_instance_assignment -name IO_STANDARD "HSSI DIFFERENTIAL I/O" -to $pin -tag $tag
    set_instance_assignment -name XCVR_VCCR_VCCT_VOLTAGE 1_0V -to $pin -tag $tag

    set_instance_assignment -name XCVR_A10_TX_VOD_OUTPUT_SWING_CTRL 29 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_TX_COMPENSATION_EN ENABLE -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SWITCHING_CTRL_1ST_POST_TAP 0 -to $pin -tag $tag
    set_instance_assignment -name XCVR_A10_TX_PRE_EMP_SIGN_1ST_POST_TAP FIR_POST_1T_POS -to $pin -tag $tag
    
    post_message -type info "# XCVR TX settings applied to pin=$pin (with tag=$tag)."
}

#============================================================================================================================
# Set FMC XCVR pins
#============================================================================================================================
xcvr_set_rx fmc_dp_m2c[0] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[1] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[2] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[3] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[4] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[5] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[6] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[7] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[8] rxc_pin_fmc
xcvr_set_rx fmc_dp_m2c[9] rxc_pin_fmc

xcvr_set_tx fmc_dp_c2m[0] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[1] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[2] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[3] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[4] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[5] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[6] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[7] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[8] rxc_pin_fmc
xcvr_set_tx fmc_dp_c2m[9] rxc_pin_fmc

#============================================================================================================================
# Set PCIE XCVR pins
#============================================================================================================================
xcvr_set_rx pcie_edge_rx[0] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[1] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[2] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[3] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[4] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[5] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[6] rxc_pin_pcie
xcvr_set_rx pcie_edge_rx[7] rxc_pin_pcie

xcvr_set_tx pcie_edge_tx[0] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[1] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[2] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[3] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[4] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[5] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[6] rxc_pin_pcie
xcvr_set_tx pcie_edge_tx[7] rxc_pin_pcie

#============================================================================================================================
# Set QSFP XCVR pins
#============================================================================================================================
xcvr_set_rx qsfp_rx[0] rxc_pin_qsfp
xcvr_set_rx qsfp_rx[1] rxc_pin_qsfp
xcvr_set_rx qsfp_rx[2] rxc_pin_qsfp
xcvr_set_rx qsfp_rx[3] rxc_pin_qsfp

xcvr_set_tx qsfp_tx[0] rxc_pin_qsfp
xcvr_set_tx qsfp_tx[1] rxc_pin_qsfp
xcvr_set_tx qsfp_tx[2] rxc_pin_qsfp
xcvr_set_tx qsfp_tx[3] rxc_pin_qsfp

#============================================================================================================================
# Set EXT XCVR pins
#============================================================================================================================
xcvr_set_rx ext_dphs_m2c[0] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[1] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[2] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[3] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[4] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[5] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[6] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[7] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[8] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[9] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[10] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[11] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[12] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[13] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[14] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[15] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[16] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[17] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[18] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[19] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[20] rxc_pin_ext
xcvr_set_rx ext_dphs_m2c[21] rxc_pin_ext

xcvr_set_tx ext_dphs_c2m[0] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[1] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[2] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[3] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[4] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[5] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[6] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[7] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[8] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[9] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[10] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[11] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[12] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[13] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[14] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[15] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[16] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[17] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[18] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[19] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[20] rxc_pin_ext
xcvr_set_tx ext_dphs_c2m[21] rxc_pin_ext

#============================================================================================================================
# Set DP XCVR pins
#============================================================================================================================
xcvr_set_tx dp_tx[0] rxc_pin_dp
xcvr_set_tx dp_tx[1] rxc_pin_dp
xcvr_set_tx dp_tx[2] rxc_pin_dp
xcvr_set_tx dp_tx[3] rxc_pin_dp

#============================================================================================================================
# Disable unused interface
#============================================================================================================================
proc remove_by_tag {param_name tag_name} {
    if {[get_parameter -name $param_name] == false} { 
        remove_all_instance_assignments -name * -tag $tag_name
        post_message -type warning "# $param_name==false => assignments with tag -$tag_name- were removed."
    } else {
        post_message -type info "# $param_name!=false => assignments with tag -$tag_name- were preserved."
    }
}

remove_by_tag CONNECT_MISC         rxc_pin_misc
remove_by_tag CONNECT_SODIMM_DDR4  rxc_pin_sodimm_ddr4
remove_by_tag CONNECT_DP_XCVR      rxc_pin_dp
remove_by_tag CONNECT_ETH          rxc_pin_gbe
remove_by_tag CONNECT_EXT          rxc_pin_ext
remove_by_tag CONNECT_FMC          rxc_pin_fmc
remove_by_tag CONNECT_PCIE         rxc_pin_pcie
remove_by_tag CONNECT_QSFP_XCVR    rxc_pin_qsfp
remove_by_tag CONNECT_POWER_MON    rxc_pin_power
remove_by_tag CONNECT_SI5341       rxc_pin_si5341

post_message "# Processing assignments file ends."
post_message "##################################################################################################"
