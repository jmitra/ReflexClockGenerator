----------------------------------------------------------------------------------------------------
-- Copyright (c) ReFLEX CES 1998-2015
--
-- Use of this source code through a simulator and/or a compiler tool
-- is illegal if not authorised through ReFLEX CES License agreement.
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- Author      : Fr�d�ric Lavenant       flavenant@reflexces.com
-- Company     : ReFLEX CES
--               2, rue du gevaudan
--               91047 LISSES
--               FRANCE
--               http://www.reflexces.com
----------------------------------------------------------------------------------------------------
-- Description :
-- This module does I2C write accesses according to RAM content.
-- Stop conditions:
--  - data.bit[0]='1' is found in the RAM.
--  - an error is detected (NACK detected on i2c bus).
--  - last RAM address is reached.
--
-- Data word in RAM is organized as follow:
--  [    0]: '0' = write to i2c device.
--           '1' = stop the sequence.
--  [ 7: 1]: Address of i2c device.
--  [15: 8]: Sub-Address.
--  [   16]: '0' = Sub-Address field not sent.
--           '1' = Sub-Address field sent to i2c device before the data byte.
--  [23:17]: delay after the i2c command. Number of delay_tick before performing next command.
--  [31:24]: data byte sent to i2c device.
--
----------------------------------------------------------------------------------------------------
-- Version      Date            Author               Description
-- 0.1          2015/04/22      FLA                  Creation
----------------------------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;

library altera_mf;
use     altera_mf.altera_mf_components.all;

library lib_jlf;
use     lib_jlf.pkg_std.all;
use     lib_jlf.pkg_std_unsigned.all;

entity auto_i2c is
    generic (
          CLOCK_PERIOD      : time                                  := 10 ns                        -- Clock period
        ; I2C_MODE          : string                                := "STANDARD"                   -- "STANDARD" (100kHz), "FAST" (400kHz), "USER" (see I2C_PERIOD)
        ; I2C_PERIOD        : time                                  := 10 us                        -- User specific SCL clock period
        ; DEVICE            : string                                := "Arria 10"                   -- Target device family for RAM implementation.
        ; NB_RAM            : integer                               := 1                            -- Define the number of configuration RAM.
        ; RAM_DEPTH         : integer                               := 512                          -- Depth of each configuration RAM (must be a power of 2).
        ; RAM_MIF_BASE      : string                                := "config_ram"                 -- First RAM will be init. with RAM_MIF_BASE0.mif, next one with RAM_MIF_BASE1.mif...
    );
    port (
        -- Clock and Reset                          
          rst               : in    sl                                                              -- Active high (A)synchronous reset.
        ; clk               : in    sl                                                              -- Clock.
        ; delay_tick        : in    sl                              := '1'                          -- Tick for delay counter after an I2C command.
                                                
        -- Control / Status                                     
        ; cfg_req           : in    sl                                                              -- Assert to start configuration until ACK is set to '1'.
        ; cfg_ack           : out   sl                                                              -- Asserted when configuration ended.
        ; cfg_sel           : in    slv8                            := (others=>'0')                -- Select configuration RAM to use.
        ; cfg_error         : out   sl                                                              -- Asserted with ACK if an error occurs during configuration.
        ; cfg_busy          : out   sl                                                              -- Asserted when module is doing i2c accesses.
        
        -- i2c ports
        ; scl               : inout sl                                                              -- SCL ('0' or 'Z', never drive to '1')
        ; sda               : inout sl                                                              -- SDA ('0' or 'Z', never drive to '1')
    );
end entity auto_i2c;

architecture rtl of auto_i2c is
    --============================================================================================================================
    -- Function and Procedure declarations
    --============================================================================================================================
    
    --============================================================================================================================
    -- Constant and Type declarations
    --============================================================================================================================
    constant C_RAM_DATA_WIDTH   : integer := 32;
    constant C_RAM_ADDR_WIDTH   : integer := Log2(RAM_DEPTH);

    --============================================================================================================================
    -- Component declarations
    --============================================================================================================================
    
    --============================================================================================================================
    -- Signal declarations
    --============================================================================================================================
    signal s_i2c_addr       : slv7                      ; -- I�C component address 7bits (!!!) (must be legal for I�C compliance)
    signal s_i2c_rwn        : sl                        ; -- Read/Write
    signal s_i2c_data_from  : slv8                      ; -- Data Received from I�C
    signal s_i2c_data_to    : slv8                      ; -- Data to Write to I�C
    signal s_i2c_data_ok    : sl                        ; -- Data transferred
    signal s_i2c_busy       : sl                        ; -- I�C Bus Busy
    signal s_i2c_finish     : sl                        ; -- Current bus cycle finished
    signal s_i2c_start      : sl        := '0'          ; -- Start Transaction
    signal s_i2c_sub_addr   : slv8      := (others=>'0'); -- Sub Address
    signal s_i2c_usa        : sl        := '0'          ; -- Use Sub-Address

    signal s_ram_data       : slv(C_RAM_DATA_WIDTH-1 downto 0);
    signal s_ram_addr       : slv(C_RAM_ADDR_WIDTH-1 downto 0);
    
begin
    assert RAM_DEPTH=2**Log2(RAM_DEPTH) report "[auto_i2c]: RAM_DEPTH must be a power of 2." severity failure;

    --############################################################################################################################
    --############################################################################################################################
    -- Configuration RAM
    --############################################################################################################################
    --############################################################################################################################
    blk_ram : block is
        type TRAMDataArray is array (natural range <>) of slv(s_ram_data'range);
        signal sb_ram_data  : TRAMDataArray(NB_RAM-1 downto 0);
    begin
        --============================================================================================================================
        -- Generate RAM
        --============================================================================================================================
        gen_ram : for i in 0 to NB_RAM-1 generate
            constant C_INIT_FILE    : string    := RAM_MIF_BASE & to_Str(i) & ".mif";
            constant C_LPM_HINT     : string    := "ENABLE_RUNTIME_MOD=YES,INSTANCE_NAME=RAM" & to_Str(i);
        begin
            i_altsyncram : altsyncram
            generic map (
                  CLOCK_ENABLE_INPUT_A          => "BYPASS"
                , CLOCK_ENABLE_OUTPUT_A         => "BYPASS"
                , INIT_FILE                     => C_INIT_FILE
                , INTENDED_DEVICE_FAMILY        => DEVICE
                , LPM_TYPE                      => "altsyncram"
                , LPM_HINT                      => C_LPM_HINT
                , NUMWORDS_A                    => RAM_DEPTH
                , NUMWORDS_B                    => RAM_DEPTH
                , OPERATION_MODE                => "SINGLE_PORT"
                , OUTDATA_ACLR_A                => "NONE"
                , OUTDATA_REG_A                 => "CLOCK0"
                , POWER_UP_UNINITIALIZED        => "FALSE"
                , RAM_BLOCK_TYPE                => "AUTO"
                , READ_DURING_WRITE_MODE_PORT_A => "DONT_CARE"
                , WIDTHAD_A                     => C_RAM_ADDR_WIDTH
                , WIDTH_A                       => C_RAM_DATA_WIDTH
                , WIDTH_BYTEENA_A               => C_RAM_DATA_WIDTH/8
            )
            port map (
                  clock0    => clk
                , wren_a    => '0'
                , byteena_a => (others => '0')
                , address_a => s_ram_addr
                , data_a    => (others => '0')
                , q_a       => sb_ram_data(i)
            );
        end generate gen_ram;
        
        --============================================================================================================================
        -- Get data from correct RAM
        --============================================================================================================================
        s_ram_data <= sb_ram_data(conv_int(cfg_sel));
        
    end block blk_ram;
    
    --############################################################################################################################
    --############################################################################################################################
    -- I2C controller
    --############################################################################################################################
    --############################################################################################################################
    i_i2c_core : entity lib_jlf.i2c_core
    generic map (
          AGENT         => "MASTER"         --       string    := "MASTER"      -- "MASTER", "SLAVE"
        , CLOCK_PERIOD  => CLOCK_PERIOD     --       time      := 10 ns         -- Clock period
        , I2C_MODE      => I2C_MODE         --       string    := "STANDARD"    -- "STANDARD" (100kHz), "FAST" (400kHz), "USER" (see I2C_PERIOD)
        , I2C_PERIOD    => I2C_PERIOD       --       time      := 10 us         -- User specific SCL clock period
        , SMBUS         => false            --       boolean   := false         -- Activate SMBus options
    ) port map (
          dmn.ena       => '1'              -- in    domain                     -- Reset/clock
        , dmn.rst       => rst              -- in    domain                     -- Reset/clock
        , dmn.clk       => clk              -- in    domain                     -- Reset/clock
        , smbus_rst     => '0'              -- in    sl        := '0'           -- SMBus / Reset
        , i2c_addr      => s_i2c_addr       -- in    slv7                       -- I�C component address 7bits (!!!) (must be legal for I�C compliance)
        , i2c_rwn       => s_i2c_rwn        -- in    sl                         -- Read/Write
        , i2c_data_from => s_i2c_data_from  -- out   slv8                       -- Data Received from I�C
        , i2c_data_to   => s_i2c_data_to    -- in    slv8                       -- Data to Write to I�C
        , i2c_data_ok   => s_i2c_data_ok    -- out   sl                         -- Data transferred
        , i2c_busy      => s_i2c_busy       -- out   sl                         -- I�C Bus Busy
        , i2c_select    => "00000001"       -- in    slv8      := "00000001"    -- Select I�C bus
        , i2c_finish    => s_i2c_finish     -- out   sl                         -- Current bus cycle finished
        , i2c_start     => s_i2c_start      -- in    sl        := '0'           -- Start Transaction
        , i2c_sub_addr  => s_i2c_sub_addr   -- in    slv8      := (others=>'0') -- Sub Address
        , i2c_sbl       => '0'              -- in    sl        := '0'           -- Slave Boundary Limit
        , i2c_ndbt      => conv_slv(1, 8)   -- in    slv8      := (others=>'0') -- Number of Data to Be Transferred
        , i2c_usa       => s_i2c_usa        -- in    sl        := '0'           -- Use Sub-Address
        , h_sm_core     => open             -- out   core_type                  -- Main core state machine
        , h_sm_core_r   => open             -- out   core_type                  -- Main core state machine (1 clock later)
        , h_sda_shift   => open             -- out   slv8                       -- SDA Shift register (data to send or received)
        , clk_dbg       => open             -- out   sl                         -- 
        , scl           => scl              -- inout sl                         -- SCL ('0' or 'Z', never drive to '1')
        , sda           => sda              -- inout sl                         -- SDA ('0' or 'Z', never drive to '1')
        , scl1          => open             -- inout sl                         -- SCL, bus #1   ('0' or 'Z', never drive to '1')
        , sda1          => open             -- inout sl                         -- SDA, bus #1   ('0' or 'Z', never drive to '1')
        , scl2          => open             -- inout sl                         -- SCL, bus #2   ('0' or 'Z', never drive to '1')
        , sda2          => open             -- inout sl                         -- SDA, bus #2   ('0' or 'Z', never drive to '1')
        , scl3          => open             -- inout sl                         -- SCL, bus #3   ('0' or 'Z', never drive to '1')
        , sda3          => open             -- inout sl                         -- SDA, bus #3   ('0' or 'Z', never drive to '1')
        , scl4          => open             -- inout sl                         -- SCL, bus #4   ('0' or 'Z', never drive to '1')
        , sda4          => open             -- inout sl                         -- SDA, bus #4   ('0' or 'Z', never drive to '1')
        , scl5          => open             -- inout sl                         -- SCL, bus #5   ('0' or 'Z', never drive to '1')
        , sda5          => open             -- inout sl                         -- SDA, bus #5   ('0' or 'Z', never drive to '1')
        , scl6          => open             -- inout sl                         -- SCL, bus #6   ('0' or 'Z', never drive to '1')
        , sda6          => open             -- inout sl                         -- SDA, bus #6   ('0' or 'Z', never drive to '1')
        , scl7          => open             -- inout sl                         -- SCL, bus #7   ('0' or 'Z', never drive to '1')
        , sda7          => open             -- inout sl                         -- SDA, bus #7   ('0' or 'Z', never drive to '1')
    );
    
    --############################################################################################################################
    --############################################################################################################################
    -- Main block
    --############################################################################################################################
    --############################################################################################################################
    blk_main: block is
        type TState is (FS_IDLE, FS_RAM_READ, FS_I2C_START, FS_I2C_WAIT, FS_DELAY, FS_NEXT, FS_FINISH);
        signal sb_state         : TState;                
        
        signal sb_cmd_ok        : sl;
        signal sb_cnt_delay     : slv8;
        signal sb_ram_addr      : slv(s_ram_addr'length downto 0); -- this is one bit more than s_ram_addr.
        signal sb_ram_rdvld     : slv2;
    begin
        --============================================================================================================================
        -- Comb. part
        --============================================================================================================================
        -- Status
        cfg_ack     <= '1'  when sb_state=FS_FINISH else '0';
        cfg_busy    <= '1'  when sb_state/=FS_IDLE  else '0';
        
        -- RAM address
        s_ram_addr  <= sb_ram_addr(s_ram_addr'range);
        
        --============================================================================================================================
        -- Sync. part
        --============================================================================================================================
        process (rst, clk)
        begin
        if rst='1' then
            sb_state        <= FS_IDLE;
            s_i2c_addr      <= (others=>'0')    ; -- I�C component address 7bits (!!!) (must be legal for I�C compliance)
            s_i2c_rwn       <= '0'              ; -- Read/Write
            s_i2c_data_to   <= (others=>'0')    ; -- Data to Write to I�C
            s_i2c_start     <= '0'              ; -- Start Transaction
            s_i2c_sub_addr  <= (others=>'0')    ; -- Sub Address
            s_i2c_usa       <= '0'              ; -- Use Sub-Address
            sb_cnt_delay    <= (others=>'0');
            cfg_error       <= '0';
            sb_ram_addr     <= (others=>'0');
            sb_ram_rdvld    <= (others=>'0');

        elsif rising_edge(clk) then
            
            ------------------------------------------------
            -- Main FSM
            ------------------------------------------------
            case sb_state is
                when FS_IDLE        =>     if cfg_req='1'                                   then sb_state <= FS_RAM_READ    ; end if;
                when FS_RAM_READ    =>     if msb(sb_ram_addr)='1'                          then sb_state <= FS_FINISH      ;
                                        elsif msb(sb_ram_rdvld)='1' and s_ram_data(0)='1'   then sb_state <= FS_FINISH      ;
                                        elsif msb(sb_ram_rdvld)='1'                         then sb_state <= FS_I2C_START   ; end if;
                when FS_I2C_START   =>                                                           sb_state <= FS_I2C_WAIT    ;
                when FS_I2C_WAIT    =>     if s_i2c_finish='1'                              then sb_state <= FS_DELAY       ; end if;
                when FS_DELAY       =>     if sb_cmd_ok='0'                                 then sb_state <= FS_FINISH      ;
                                        elsif msb(sb_cnt_delay)='1'                         then sb_state <= FS_NEXT        ; end if;
                when FS_NEXT        =>                                                           sb_state <= FS_RAM_READ    ;
                when FS_FINISH      =>                                                           sb_state <= FS_IDLE        ;
            end case;
            
            --============================================================================================================================
            -- Misc.
            --============================================================================================================================
            -- Delay after the command
               if sb_state=FS_I2C_WAIT                                              then sb_cnt_delay <= ('0' & s_ram_data(23 downto 17)) - 1;
            elsif sb_state=FS_DELAY   and delay_tick='1' and msb(sb_cnt_delay)='0'  then sb_cnt_delay <= sb_cnt_delay - 1;
            end if;
            
            -- Command OK
               if s_i2c_start='1'       then sb_cmd_ok <= '0';
            elsif s_i2c_data_ok='1'     then sb_cmd_ok <= '1';
            end if;
            
            -- Error during sequence.
               if sb_state=FS_IDLE                      then cfg_error <= '0';
            elsif sb_state=FS_DELAY and sb_cmd_ok='0'   then cfg_error <= '1';
            end if;
            
            --============================================================================================================================
            -- Drive RAM
            --============================================================================================================================
            -- RAM address
               if sb_state=FS_IDLE  then sb_ram_addr <= (others=>'0');
            elsif sb_state=FS_NEXT  then sb_ram_addr <= sb_ram_addr + 1;
            end if;
            
            -- RAM data valid
               if sb_state=FS_IDLE or sb_state=FS_NEXT  then sb_ram_rdvld <= (others=>'0');
            elsif sb_state=FS_RAM_READ                  then sb_ram_rdvld <= sb_ram_rdvld(sb_ram_rdvld'high-1 downto 0) & '1';
            end if;
            
            --============================================================================================================================
            -- Drive I2C controller
            --============================================================================================================================
            -- I2C start
               if sb_state=FS_I2C_START then s_i2c_start <= '1';
            else                             s_i2c_start <= '0';
            end if;
            
            -- I2C RWn
               if sb_state=FS_I2C_START then s_i2c_rwn      <= '0'; -- Only manage write accesses.
            end if;
            
            -- I2C data write
               if sb_state=FS_I2C_START then s_i2c_data_to  <= s_ram_data(31 downto 24);
            end if;
            
            -- I2C address
               if sb_state=FS_I2C_START then s_i2c_addr     <= s_ram_data(7 downto 1);
            end if;
                        
            -- I2C sub-address
               if sb_state=FS_I2C_START then s_i2c_sub_addr <= s_ram_data(15 downto 8);
            end if;
            
            -- I2C usa
               if sb_state=FS_I2C_START then s_i2c_usa      <= s_ram_data(16);
            end if;
            
        end if;
        end process;
        
    end block blk_main;
    
end architecture rtl;
