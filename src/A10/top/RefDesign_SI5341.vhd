----------------------------------------------------------------------------------------------------
-- Copyright (c) ReFLEX CES 1998-2015
--
-- Use of this source code through a simulator and/or a compiler tool
-- is illegal if not authorised through ReFLEX CES License agreement.
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- Author      : Fr�d�ric Lavenant       flavenant@reflexces.com
-- Company     : ReFLEX CES
--               2, rue du gevaudan
--               91047 LISSES
--               FRANCE
--               http://www.reflexces.com
----------------------------------------------------------------------------------------------------
-- Description :
--
--
----------------------------------------------------------------------------------------------------
-- Version      Date            Author               Description
-- 0.1          2015/03/17      FLA                  Creation
----------------------------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;

library lib_jlf;
use     lib_jlf.pkg_std.all;
use     lib_jlf.pkg_std_unsigned.all;

library pll_sys;
use     pll_sys.all;

library issp;
use     issp.all;

entity RefDesign_SI5341 is generic
	( DEVICE                            : string            := "Arria 10"                       -- Target Device
	); port (
      clk_100mhz_1                      : in    std_logic                                       --
	; clk_100mhz_2                      : in    std_logic                                       --
	; clk_100mhz_3                      : in    std_logic                                       --
	; clk_100mhz_4                      : in    std_logic                                       --
    ; clk_fpga_out                      : out   std_logic                                          
    
    --============================================================================================================================
    -- SI5341
    --============================================================================================================================
    ; si5341_sda                        : inout std_logic                   
    ; si5341_scl                        : inout std_logic                   
    --============================================================================================================================
    -- User interface
    --============================================================================================================================
    -- Switches and Buttons
    --; bp_n                              : in    std_logic_vector(   4 downto 1)
	; dipswitch_a                       : in    std_logic_vector(   4 downto 1)                  --
	; dipswitch_b                       : in    std_logic_vector(   4 downto 1)                  --
    
    -- LED                                                                                       --
	; led_fav_blue                      : out   std_logic                                        --
	; led_fav_green                     : out   std_logic                                        --
	; led_fav_red                       : out   std_logic                                        --
	; led_usr_green1_n                  : out   std_logic                                        --
	; led_usr_green2_n                  : out   std_logic                                        --
	; led_usr_orange1_n                 : out   std_logic                                        --
	; led_usr_red1_n                    : out   std_logic                                        --
    
    --============================================================================================================================
    -- Misc clocks for testing
    --============================================================================================================================
    ; clk_xcvr_dp_a                     : in    std_logic
    ; clk_xcvr_ext1                     : in    std_logic
    ; clk_xcvr_ext3                     : in    std_logic
    ; clk_xcvr_ext_a                    : in    std_logic
    ; clk_xcvr_fcm1_a                   : in    std_logic
    ; clk_xcvr_fcm2_a                   : in    std_logic
    ; clk_xcvr_qsfp_a                   : in    std_logic
    ; gbtclk0                           : in    std_logic
    ; gbtclk1                           : in    std_logic
    ; fmc_clk0_m2c                      : in    std_logic
    ; fmc_clk1_m2c                      : in    std_logic
    ; pcie_edge_refclk                  : in    std_logic
    ; ext_dp_clk_m2c                    : in    std_logic
    ; mem_ref_clk                       : in    std_logic
    
	);
end entity RefDesign_SI5341;

architecture rtl of RefDesign_SI5341 is

   --============================================================================================================================
    -- Function and Procedure declarations
    --============================================================================================================================
    
    --============================================================================================================================
    -- Constant and Type declarations
    --============================================================================================================================
    constant C_SYS_CLK_PERIOD           : time      := 10 ns;
    constant C_SYS_CLK_FREQUENCY        : integer   := 100000000;
    
    --============================================================================================================================
    -- Component declarations
    --============================================================================================================================
    
    --============================================================================================================================
    -- Signal declarations
    --============================================================================================================================
    signal s_clk                : sl;
    signal s_rst                : sl;
    signal s_sys_clk            : sl;
    signal s_2m5_clk            : sl;
    signal s_500m_clk           : sl;
    signal s_25m_clk            : sl;
    signal s_125m_clk           : sl;
    signal s_f2m_clk            : sl;
    signal s_m2f_clk            : sl;
    signal s_sys_rst            : sl;
    signal s_2m5_rst            : sl;
    signal s_500m_rst           : sl;
    signal s_25m_rst            : sl;
    signal s_125m_rst           : sl;
    signal s_f2m_rst            : sl;
    signal s_m2f_rst            : sl;
    signal s_sys_rstn           : sl;
    signal s_2m5_rstn           : sl;
    signal s_500m_rstn          : sl;
    signal s_25m_rstn           : sl;
    signal s_125m_rstn          : sl;
    signal s_f2m_rstn           : sl;
    signal s_m2f_rstn           : sl;
    signal s_o_reset_req        : sl;
            
    signal s_f2m_clk90          : sl;
            
    -- Tick and timestamp   
    signal s_tick_1s            : sl;
    signal s_tick_1s_t          : sl;
    signal s_tick_1ms           : sl;
    signal s_tick_1ms_t         : sl;
    signal s_tick_10us          : sl;
    signal s_tick_10us_t        : sl;
    signal s_tick_2hz           : sl;
    signal s_tick_2hz_t         : sl;
    signal s_tick_4hz           : sl;
    signal s_tick_4hz_t         : sl;
    signal s_tick_8hz           : sl;
    signal s_tick_8hz_t         : sl;
    signal s_tick_50hz          : sl;
    signal s_tick_50hz_t        : sl;
    signal s_timestamp_ns       : slv64;
    
    signal bp_n                 : std_logic_vector(2 downto 1);
            
begin
    --############################################################################################################################
    --############################################################################################################################
    -- System PLL, LVDS PLL and reset
    --############################################################################################################################
    --############################################################################################################################
    blk_clk_n_rst : block is
        constant C_RST_DELAY_NB_BITS    : integer           := 21*(1-conv_int(SIMULATION)) + 2; -- 2**23 cycles @100MHz => 84ms (but shorter during simulation)

        signal sb_pll_locked            : sl;
        signal sb_rst                   : sl    := '1';
        signal sb_rst_dly               : sl;
    begin
        --============================================================================================================================
        -- PLL reset    
        --============================================================================================================================
        process (clk_100mhz_1)
        begin
        if rising_edge(clk_100mhz_1) then
            sb_rst <=  not(bp_n(1));
        end if;
        end process;
        
        --============================================================================================================================
        -- PLL (inclock=100MHz)
        --  c0 : 100.0MHz (must be consistent with C_SYS_CLK_PERIOD)
        --  c1 :   2.5MHz
        --  c2 : 500.0MHz
        --  c3 :  25.0MHz
        --  c4 : 125.0MHz
        --  c5 :  50.0MHz
        --  c6 :  50.0MHz + 90�
        --============================================================================================================================    
        i_pll_sys : entity pll_sys.pll_sys
        port map (
              locked   => sb_pll_locked -- out std_logic        -- locked.export
            , outclk_0 => s_sys_clk     -- out std_logic        -- outclk0.clk
            , outclk_1 => s_2m5_clk     -- out std_logic        -- outclk1.clk
            , outclk_2 => s_500m_clk    -- out std_logic        -- outclk2.clk
            , outclk_3 => s_25m_clk     -- out std_logic        -- outclk3.clk
            , outclk_4 => s_125m_clk    -- out std_logic        -- outclk4.clk
            , outclk_5 => s_f2m_clk     -- out std_logic        -- outclk4.clk
            , outclk_6 => s_f2m_clk90   -- out std_logic        -- outclk4.clk
            , refclk   => clk_100mhz_3  -- in  std_logic := '0' -- refclk.clk
            , rst      => sb_rst        -- in  std_logic := '0' -- reset.reset
        );
        
        --============================================================================================================================
        -- Delayed reset.
        --============================================================================================================================
        i_delayed_rst : entity work.delayed_rst
        generic map (
              NB_BITS   => C_RST_DELAY_NB_BITS          --     integer   := 2   -- number of bits for the internal counter. Ex. 2 will generate a 2**NB_BITS+3 cycles reset
        ) port map (
              in_rst_n  => sb_pll_locked                -- in  std_logic := '1' -- asynchronous active low reset (choose only one between active low or high reset).
            , out_clk   => clk_100mhz_1                 -- in  std_logic        -- clock used to synchronize reset and for counter
            , out_rst   => sb_rst_dly                   -- out std_logic        -- synchronous de-asserted active high reset
        );
        
        --============================================================================================================================
        -- Generate reset
        --============================================================================================================================
        i_sync_rst : entity work.sync_rst
        generic map (
              NB_RESET      => 7                --     integer                               := 1             -- number of reset to synchronize
        ) port map (
              in_com_rst    => sb_rst_dly       -- in  std_logic                             := '0'           -- asynchronous active high reset  common to all clock domains /!\ choose only one reset source for each output /!\
            , in_rst   (0)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (1)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (2)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (3)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (4)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (5)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , in_rst   (6)  => '0'              -- in  std_logic_vector(NB_RESET-1 downto 0) := (others=>'0') -- asynchronous active high resets                            /!\ choose only one reset source for each output /!\
            , out_clk  (0)  => s_sys_clk        -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (1)  => s_2m5_clk        -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (2)  => s_500m_clk       -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (3)  => s_25m_clk        -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (4)  => s_125m_clk       -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (5)  => s_f2m_clk        -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_clk  (6)  => s_m2f_clk        -- in  std_logic_vector(NB_RESET-1 downto 0)                  -- clocks used to synchronize resets
            , out_rst_n(0)  => s_sys_rstn       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(1)  => s_2m5_rstn       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(2)  => s_500m_rstn      -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(3)  => s_25m_rstn       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(4)  => s_125m_rstn      -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(5)  => s_f2m_rstn       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst_n(6)  => s_m2f_rstn       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active low resets
            , out_rst  (0)  => s_sys_rst        -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (1)  => s_2m5_rst        -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (2)  => s_500m_rst       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (3)  => s_25m_rst        -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (4)  => s_125m_rst       -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (5)  => s_f2m_rst        -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
            , out_rst  (6)  => s_m2f_rst        -- out std_logic_vector(NB_RESET-1 downto 0)                  -- synchronous de-asserted active high resets
        );

        --============================================================================================================================
        -- ISSP
        --============================================================================================================================
        -- To SI5341        
        issp_comp : entity issp.issp
		port map (
			source => bp_n  -- sources.source
		);
        --============================================================================================================================
        -- Misc. clocks
        --============================================================================================================================
        -- To SI5341
        clk_fpga_out    <= s_sys_clk;
        
        --============================================================================================================================
        -- Tick generators and timestamp
        --============================================================================================================================
        -- 1s
        i_tick_gen_1s : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY        --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (        
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_1s                  -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_1s_t                -- out std_logic              -- inverted each time
        );
        
        -- 1ms
        i_tick_gen_1ms : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/1000   --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_1ms                 -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_1ms_t               -- out std_logic              -- inverted each time
        );
        
        -- 10us
        i_tick_gen_10us : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/100000 --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_10us                -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_10us_t              -- out std_logic              -- inverted each time
        );
        
        -- 2Hz
        i_tick_gen_2hz : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/2      --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_2hz                 -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_2hz_t               -- out std_logic              -- inverted each time
        );
        
        -- 4Hz
        i_tick_gen_4hz : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/4      --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_4hz                 -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_4hz_t               -- out std_logic              -- inverted each time
        );
        
        -- 8Hz
        i_tick_gen_8hz : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/8      --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_8hz                 -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_8hz_t               -- out std_logic              -- inverted each time
        );
        
        -- 50Hz
        i_tick_gen_50hz : entity work.tick_gen
        generic map (
              NB_CYCLE    => C_SYS_CLK_FREQUENCY/50     --     integer   := 160000000 -- generate one 'tick' every NB_CYCLE clock periodes
        ) port map (    
              rst         => s_sys_rst                  -- in  std_logic := '0'       -- asynchronous active high reset
            , clk         => s_sys_clk                  -- in  std_logic              -- module and base clock
            , tick        => s_tick_50hz                -- out std_logic              -- '1' for one cycle
            , tick_toggle => s_tick_50hz_t              -- out std_logic              -- inverted each time
        );
        
        -- Nano second timestamp
        process (s_sys_rst, s_sys_clk)
        begin
        if s_sys_rst='1' then
            s_timestamp_ns <= (others=>'0');
        elsif rising_edge(s_sys_clk) then
            s_timestamp_ns  <= s_timestamp_ns + (C_SYS_CLK_PERIOD/1 ns);
        end if;
        end process;
        
    end block blk_clk_n_rst;
    
    blk_si5341 : block is
        -- Signals of auto_i2c
        signal sb_cfg_req      : sl                      ; -- Assert to start configuration until ACK is set to '1'.
        signal sb_cfg_ack      : sl                      ; -- Asserted when configuration ended.
        signal sb_cfg_sel      : slv8    := (others=>'0'); -- Select configuration RAM to use.
        signal sb_cfg_error    : sl                      ; -- Asserted with ACK if an error occurs during configuration.
        signal sb_cfg_busy     : sl                      ; -- Asserted when module is doing i2c accesses.

    begin
        --============================================================================================================================
        -- Manager
        --============================================================================================================================
        process (s_sys_rst, s_sys_clk)
        begin
        if s_sys_rst='1' then
            sb_cfg_req <= '0';
        elsif rising_edge(s_sys_clk) then
               if sb_cfg_ack='1'                    then sb_cfg_req <= '0';
            elsif bp_n(2)='0' and s_tick_1ms='1'    then sb_cfg_req <= '1';
            end if;
        end if;
        end process;
        
        --============================================================================================================================
        -- I2C command generator
        --============================================================================================================================
        i_auto_i2c : entity work.auto_i2c
        generic map (
              CLOCK_PERIOD => C_SYS_CLK_PERIOD      --       time    := 10 ns         -- Clock period
            , I2C_MODE     => "FAST"                --       string  := "STANDARD"    -- "STANDARD" (100kHz), "FAST" (400kHz), "USER" (see I2C_PERIOD)
            , DEVICE       => DEVICE                --       string  := "Arria 10"    -- Target device family for RAM implementation.
            , NB_RAM       => 1                     --       integer := 1             -- Define the number of configuration RAM.
            , RAM_DEPTH    => 512                   --       integer := 512           -- Depth of each configuration RAM (must be a power of 2).
            , RAM_MIF_BASE => "config_ram"          --       string  := "config_ram"  -- First RAM will be init. with RAM_MIF_BASE0.mif, next one with RAM_MIF_BASE1.mif...
        ) port map (
              rst          => s_sys_rst             -- in    sl                       -- Active high (A)synchronous reset.
            , clk          => s_sys_clk             -- in    sl                       -- Clock.
            , delay_tick   => s_tick_50hz           -- in    sl      := '1'           -- Tick for delay counter after an I2C command.
            , cfg_req      => sb_cfg_req            -- in    sl                       -- Assert to start configuration until ACK is set to '1'.
            , cfg_ack      => sb_cfg_ack            -- out   sl                       -- Asserted when configuration ended.
            , cfg_sel      => sb_cfg_sel            -- in    slv8    := (others=>'0') -- Select configuration RAM to use.
            , cfg_error    => sb_cfg_error          -- out   sl                       -- Asserted with ACK if an error occurs during configuration.
            , cfg_busy     => sb_cfg_busy           -- out   sl                       -- Asserted when module is doing i2c accesses.
            , scl          => si5341_scl            -- inout sl                       -- SCL ('0' or 'Z', never drive to '1')
            , sda          => si5341_sda            -- inout sl                       -- SDA ('0' or 'Z', never drive to '1')
        );

    end block blk_si5341;
    
    --############################################################################################################################
    --############################################################################################################################
    -- Frequency measure
    --############################################################################################################################
    --############################################################################################################################
    blk_freq : block is
        constant NB_MEASURE_FREQ    : integer := 18;
        signal sb_clock             : slv(NB_MEASURE_FREQ-1 downto 0);
    begin
        --============================================================================================================================
        -- Clock mapping
        --============================================================================================================================
        sb_clock(0 ) <= clk_100mhz_1;
        sb_clock(1 ) <= clk_100mhz_2;
        sb_clock(2 ) <= clk_100mhz_3;
        sb_clock(3 ) <= clk_100mhz_4;
        sb_clock(4 ) <= clk_xcvr_dp_a;
        sb_clock(5 ) <= clk_xcvr_ext1;
        sb_clock(6 ) <= clk_xcvr_ext3;
        sb_clock(7 ) <= clk_xcvr_ext_a;
        sb_clock(8 ) <= clk_xcvr_fcm1_a;
        sb_clock(9 ) <= clk_xcvr_fcm2_a;
        sb_clock(10) <= clk_xcvr_qsfp_a;
        sb_clock(11) <= gbtclk0;
        sb_clock(12) <= gbtclk1;
        sb_clock(13) <= fmc_clk0_m2c;
        sb_clock(14) <= fmc_clk1_m2c;
        sb_clock(15) <= pcie_edge_refclk;
        sb_clock(16) <= ext_dp_clk_m2c;
        sb_clock(17) <= mem_ref_clk;
    
        --============================================================================================================================
        -- Measure
        --============================================================================================================================
        gen_counter : for i in sb_clock'range generate
            signal sb_rate_count            : slv32;
            attribute keep                  : boolean;
            attribute keep of sb_rate_count : signal is true;
        begin
            i_rate_count : entity work.rate_count
            generic map (
                  COUNT_WIDTH => sb_rate_count'length   --     integer                                  := 32            -- number of bits for output counter
            ) port map (
                  cycle_clk   => sb_clock(i)            -- in  std_logic                                                 -- clock for the input rate
                , cycle_rst   => s_sys_rst              -- in  std_logic                                := '0'           -- reset for input rate              /!\ only use one reset /!\
                , rate_rst    => s_sys_rst              -- in  std_logic                                := '0'           -- reset for the rate clock              /!\ only use one reset /!\
                , rate_clk    => s_sys_clk              -- in  std_logic                                := '0'           -- optionnal clock domain for rate counter
                , rate_count  => sb_rate_count          -- out std_logic_vector(COUNT_WIDTH-1 downto 0)                  -- output counter (rate) (rate_clk clock domain)
                , tick_rst    => s_sys_rst              -- in  std_logic                                := '0'           -- reset for the reference clock              /!\ only use one reset /!\
                , tick_clk    => s_sys_clk              -- in  std_logic                                                 -- reference clock for the measure
                , tick        => s_tick_1s              -- in  std_logic                                := '0'           -- pulse to define measure interval
            );
        end generate gen_counter;
    end block blk_freq;
    
end architecture rtl;
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
