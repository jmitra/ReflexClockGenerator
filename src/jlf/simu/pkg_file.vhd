/*--------------------------------------------------------------------------------------------------------------------------------------------------------------
Author      : Jean-Louis FLOQUET
Title       : Package for file accesses
File        : pkg_file.vhd
Application : Simulation
Created     : 2004, January 6th
Last update : 2014/02/19 11:21
Version     : 3.00.06
Dependency  : pkg_std, pkg_math_simu, pkg_simu
----------------------------------------------------------------------------------------------------------------------------------------------------------------
Description : This package contains primitives for RAW (binary) files access. It includes also several others procedures to simplify some specific file
              types (BitMap, Bayer...)
----------------------------------------------------------------------------------------------------------------------------------------------------------------
Restriction : This package is NOT compatible with VHDL-1987 for several reasons:
               1) Read / Write procedures don't exist in this VHDL version
               2) Only first 128 characters are defined, instead of 256. This 256 characters array is used for file accesses.
Simulator   : ModelSim AE, PE, SE (some crashes occur on this last version). Prefer PE version or versions greater than 6.6c.
----------------------------------------------------------------------------------------------------------------------------------------------------------------
  Rev.   |    Date    | Description
 3.00.06 | 2013/11/22 | 0) Chg : NOT COMPATIBLE with previous versions !!!
         |            | 1) Chg : VHDL-2008 required
         |            | 2) Chg : File procedures with protected shared variable. Old ones are removed
         |            | 3) Fix : WriteBMP had 4 bytes unspecified
         |            | 4) Enh : WriteBMP verify that output ByteArray is long enough
         |            | 5) Chg : Records moved to pkg_std.vhd (RTL compliant)
         | 2014/01/06 | 6) Enh : TXT_Write option to NOT clear line
         |            |
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 2.08.04 | 2013/01/30 | 1) New : TXT_Close procedure
         |            | 2) New : TXT_Write to perform open file / write text / close file
         | 2013/07/30 | 3) Enh : TXT_Write 2.08.02 function has overwrite parameter
         | 2013/10/10 | 4) New : AppendFile_8 to write-append to a file in byte mode
 2.07.01 | 2012/12/03 | 1) New : TXT_Read procedure
 2.06.01 | 2012/01/09 | 1) Fix : Message for TXT_Open when unable to open the file
 2.05.02 | 2011/03/22 | 1) Chg : Write to log file procedures moved from pkg_simu.vhd. WriteLog* also renamed to WriteHTML*
         | 2011/11/28 | 2) New : TXT_Write accept line as argument
 2.04.02 | 2009/07/20 | 1) Chg : Removed usage of pkg_std_unsigned because this package doesn't handle only unsigned objects (Wave procedures)
         |            | 2) Chg : Removed usage of pkg_math_simu (this package is merged with pkg_std, so no real modification)
 2.03.04 | 2008/05/16 | 1) New : Add verbose option to some functions
         | 2008/11/19 | 2) Fix : Accept non fully initialized ByteArray for WriteFile_8
         | 2008/12/01 | 3) Chg : PKG_FILE_ARRAY_SIZE constant moved to external package. This avoid editing this package just for changing this constant.
         | 2008/12/06 | 4) New : WriteWave procedure
 2.02.04 | 2006/11/07 | 1) New : WriteBMPFromYCbCr implemented
         |            | 2) Chg : RGB <-> YCbCr coefficient changed for computer systems (RGB & YCbCr in full [0;255] space)
         |            | 3) Fix : Removed warning about unprotected shared variables
         |            | 4) Enh : Use Matrix representation for YCbCr / RGB conversions
 2.01.01 | 2006/06/28 | 1) New : Another simplified call for "ReadFile_8" procedure
         |            | 2) Chg : Some cosmetics
 2.00.04 | 2005/09/09 | 1) New : Dynamic memory allocation with pointers !!
         |            | 2) New : Conversion BMP -> YUV (handling PAL, SECAM & NTSC video systems)
         |            | 3) New : Conversion BMP -> YCbCr
         |            | 4) Chg : ReadBayerFromBMP output renamed to "Bayer"
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 1.06.01 | 2004/09/22 | 1) Fix : Padding for ReadBMP & WriteBMP
 1.05.03 | 2004/08/11 | 1) New : 8/16 bits support for ReadBMP
         |            | 2) Enh : MODE_ERROR message for write operations
         |            | 3) New : Error message for ReadBMP when output array is too small
 1.04.01 | 2004/06/21 | 1) Chg : ReadBayerFromBMP call change for consistency with other procedures.
 1.03.01 | 2004/04/29 | 1) New : Procedures to not use Alpha layer (that is not yet implemented), to simplify all accesses from any other procedure.
 1.02.01 | 2004/02/11 | 1) Enh : Better support for BMP format.
 1.01.01 | 2004/02/07 | 1) New : Conversion BMP -> Bayer
 1.00.00 | 2004/01/06 | Initial Release
         |            |
----------------------------------------------------------------------------------------------------------------------------------------------------------------
いいいいいい
� Notes    �
いいいいいい
* [Note 1] : Procedures index
        +-----
        | Main procedures are indexed above. This is just a very short index to have a general view of available procedures.
        | For more details about arguments and usage, see procedure declaration and/or procedure body
        |
        |                       | Input                              | Output
        |  ---------------------+------------------------------------+-----------------------------------------------------------------
        |    ReadFile_8         | RAW binary file from host computer | Array of bytes
        |    ReadFile_3x8       | RAW binary file from host computer | Array of 3bytes
        |    ReadBMP            | BMP        file from host computer | Array of RGB pixels
        |    ReadBayerFromBMP   | BMP        file from host computer | Array of monochromatic pixels according to Bayer matrix
        |    ReadYUVFromBMP     | BMP        file from host computer | Array of YUV pixels
        |    ReadYCbCrFromBMP   | BMP        file from host computer | Array of YCbCr pixels
        |                       |                                    |
        |    WriteFile_8        | Array of bytes                     | RAW binary file to host computer, overwrite mode
        |    WriteBMP           | Array of RGB pixels                | BMP        file to host computer, overwrite mode
        |    WriteBMPFromYCbCr  | Array of YCbCr pixels              | BMP        file to host computer, overwrite mode
        |    WriteWave          | Array of bytes                     | WAV        file to host computer, overwrite mode
        |                       |                                    |
        |    AppendFile_8       | Array of bytes                     | RAW binary file to host computer, append mode
        |                       |                                    |
        |    TXT_Open           | File name                          | None (file is open for specified operation)
        |    TXT_Read           | None                               | Read line and EOF flag
        |    TXT_Write          | String                             | Write string to file
        |    TXT_Write          | Line                               | Write line   to file (line is cleared by default - IEEE library)
        |                       |                                    |
        |    WriteHTMLHeader    | File name                          | Add HTML Header to HTML file
        |    WriteHTML          | Up to 5 strings                    | Add strings to HTML file
        |                       |                                    |
        |    Free               | Array                              | None (array cleared and removed from host computer memory)
        |                       |                                    |
        +-----

* [Note 2] : Protected arrays
        +-----
        | All these arrays have an internal test to automatically create a new elementary record if required.
        | The old style (BEFORE this version) need the designer do something like that :
        |     if MyVariable(index)=null then
        |     	MyVariable(index) := new ByteRec;
        |     end if;
        |     MyVariable(index).d := data.d;
        | This is now done by the internal procedure 'Write' => DO NOT CREATE externally this new record :)
        |
        | In this counterpart, user has to provide a record. See the following example :
        |     ThisByteRec.d := My8bitsVector;
        |     MyByteArrayProtected.Write(index,ThisByteRec);
        +-----

--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
-- synthesis translate_off
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     ieee.math_real.all;
use     std.textio.all;

library work;
use     work.pkg_std.all;
use     work.pkg_simu.all;
use     work.pkg_file_cst.all;

package pkg_file is

	type RAWFileType is file of character;
	file RawFile : RAWFileType;

	----------------------------------------------------------------------
	--                                                                  --
	--     / \    Read important [Note 2] for protected types   / \     --
	--    /   \                                                /   \    --
	--   /  !  \       DO NOT USE 'new' anymore !!            /  !  \   --
	--  /       \  PROVIDE always a RECORD when writing      /       \  --
	--  ���������                                            ���������  --
	----------------------------------------------------------------------
	-- Byte                                                             --
	----------------------------------------------------------------------
--	subtype Byte is slv8;                                               -- Included in pkg_std.vhd
--	type ByteRec is record                                              -- Included in pkg_std.vhd
--		d : Byte;                                                       -- Included in pkg_std.vhd
--	end record ByteRec;                                                 -- Included in pkg_std.vhd
	type BytePtr   is access ByteRec;                                   --
	type ByteArray is array (0 to PKG_FILE_ARRAY_SIZE) of BytePtr;      -- Use this type for VHDL 1993-2002
	type ByteArrayProtected is protected                                -- Use this type for VHDL-2008
		impure function Exist(index : nat) return boolean  ;            -- Check if a specific record exist
		procedure       Kill (index : nat)                 ;            -- Destroy/Deallocate the memory for this index
		procedure       KillAll                            ;            -- Destroy/Deallocate the memory for this entire variable
		impure function Length             return nat      ;            -- Return array length
		impure function Read (index : nat) return ByteRec  ;            -- Get value
		procedure       Write(index : nat; data : ByteRec );            -- Set value
	end protected ByteArrayProtected;                                   --
	----------------------------------------------------------------------
	-- Byte3                                                            --
	----------------------------------------------------------------------
--	type Byte3Rec is record                                             -- Included in pkg_std.vhd
--		d0 : Byte;                                                      -- Included in pkg_std.vhd
--		d1 : Byte;                                                      -- Included in pkg_std.vhd
--		d2 : Byte;                                                      -- Included in pkg_std.vhd
--	end record Byte3Rec;                                                -- Included in pkg_std.vhd
	type Byte3Ptr   is access Byte3Rec;                                 --
	type Byte3Array is array (0 to PKG_FILE_ARRAY_SIZE) of Byte3Ptr;    -- Use this type for VHDL 1993-2002
	type Byte3ArrayProtected is protected                               -- Use this type for VHDL-2008
		impure function Exist(index : nat) return boolean  ;            -- Check if a specific record exist
		procedure       Kill (index : nat)                 ;            -- Destroy/Deallocate the memory for this index
		procedure       KillAll                            ;            -- Destroy/Deallocate the memory for this entire variable
		impure function Length             return nat      ;            -- Return array length
		impure function Read (index : nat) return Byte3Rec ;            -- Get value
		procedure       Write(index : nat; data : Byte3Rec);            -- Set value
	end protected Byte3ArrayProtected;                                  --
	----------------------------------------------------------------------
	-- RGB                                                              --
	----------------------------------------------------------------------
--	type RgbRec is record                                               -- Included in pkg_std.vhd
--		R : Byte;                                                       -- Included in pkg_std.vhd
--		G : Byte;                                                       -- Included in pkg_std.vhd
--		B : Byte;                                                       -- Included in pkg_std.vhd
--	end record RgbRec;                                                  -- Included in pkg_std.vhd
	type RgbPtr   is access RgbRec;                                     --
	type RgbArray is array (0 to PKG_FILE_ARRAY_SIZE) of RgbPtr;        -- Use this type for VHDL 1993-2002
	type RgbArrayProtected is protected                                 -- Use this type for VHDL-2008
		impure function Exist(index : nat) return boolean;              -- Check if a specific record exist
		procedure       Kill (index : nat)               ;              -- Destroy/Deallocate the memory for this index
		procedure       KillAll                          ;              -- Destroy/Deallocate the memory for this entire variable
		impure function Length             return nat    ;              -- Return array length
		impure function Read (index : nat) return RgbRec ;              -- Get value
		procedure       Write(index : nat; data : RgbRec);              -- Set value
	end protected RgbArrayProtected;                                    --
	----------------------------------------------------------------------
	-- YUV                                                              --
	----------------------------------------------------------------------
--	type YuvRec is record                                               -- Included in pkg_std.vhd
--		U  : Byte;                                                      -- Included in pkg_std.vhd
--		Ya : Byte;                                                      -- Included in pkg_std.vhd
--		V  : Byte;                                                      -- Included in pkg_std.vhd
--		Yb : Byte;                                                      -- Included in pkg_std.vhd
--	end record YuvRec;                                                  -- Included in pkg_std.vhd
	type YuvPtr    is access YuvRec;                                    --
	type YuvArray  is array (0 to PKG_FILE_ARRAY_SIZE) of YuvPtr;       -- Use this type for VHDL 1993-2002
	type YuvArrayProtected is protected                                 -- Use this type for VHDL-2008
		impure function Exist(index : nat) return boolean;              -- Check if a specific record exist
		procedure       Kill (index : nat)               ;              -- Destroy/Deallocate the memory for this index
		procedure       KillAll                          ;              -- Destroy/Deallocate the memory for this entire variable
		impure function Length             return nat    ;              -- Return array length
		impure function Read (index : nat) return YuvRec ;              -- Get value
		procedure       Write(index : nat; data : YuvRec);              -- Set value
	end protected YuvArrayProtected;                                    --
                                                                        --
	type YuvBlanking is record                                          --
		Field0Start : int range 1 to 625;                               --
		Field0Stop  : int range 1 to 625;                               --
		Field1Start : int range 1 to 625;                               --
		Field1Stop  : int range 1 to 625;                               --
	end record YuvBlanking;                                             --
                                                                        --
	constant YUV_BLK_PAL : YuvBlanking := YuvBlanking'(23,310,336,623); --
	----------------------------------------------------------------------
	-- YCbCr                                                            --
	----------------------------------------------------------------------
--	type YCbCrRec is record                                             -- Included in pkg_std.vhd
--		Cb : Byte;                                                      -- Included in pkg_std.vhd
--		Ya : Byte;                                                      -- Included in pkg_std.vhd
--		Cr : Byte;                                                      -- Included in pkg_std.vhd
--		Yb : Byte;                                                      -- Included in pkg_std.vhd
--	end record YCbCrRec;                                                -- Included in pkg_std.vhd
	type YCbCrPtr    is access YCbCrRec;                                --
	type YCbCrArray  is array (0 to PKG_FILE_ARRAY_SIZE) of YCbCrPtr;   -- Use this type for VHDL 1993-2002
	type YCbCrArrayProtected is protected                               -- Use this type for VHDL-2008
		impure function Exist(index : nat) return boolean  ;            -- Check if a specific record exist
		procedure       Kill (index : nat)                 ;            -- Destroy/Deallocate the memory for this index
		procedure       KillAll                            ;            -- Destroy/Deallocate the memory for this entire variable
		impure function Length             return nat      ;            -- Return array length
		impure function Read (index : nat) return YCbCrRec ;            -- Get value
		procedure       Write(index : nat; data : YCbCrRec);            -- Set value
	end protected YCbCrArrayProtected;

	type YCbCrTimings is record
		NbLine      : int range 1 to 625; -- Total number of lines (both fields with blanking)
		Field0Start : int range 1 to 625; -- First line of Field #0
		Field1Start : int range 1 to 625; -- First line of Field #1
		Vbi0Stop    : int range 1 to 625; -- Last  line of VBI in Field 0
		Vbi0Start   : int range 1 to 625; -- First line of VBI in Field 0
		Vbi1Stop    : int range 1 to 625; -- Last  line of VBI in Field 1
		Vbi1Start   : int range 1 to 625; -- First line of VBI in Field 1
	end record YCbCrTimings;

	constant YCBCR_PAL : YCbCrTimings := YCbCrTimings'(625,1,313,23,311,336,624); -- Constants for a standard PAL video
	constant YCBCR_8X8 : YCbCrTimings := YCbCrTimings'( 32,1, 17, 2, 10, 20, 28); -- Constants for a custom 8x8 square video (for small & fast simulations)
	--==================================================================================================
	--                                             R E A D
	--==================================================================================================
	-- Low-level Read File
	procedure ReadFile_8       ( Name        : in    string              -- Name File, including path with "/"
	                           ; verbose     : in    boolean             -- Return success message
	                           ; RawArray    : inout ByteArrayProtected  -- Output bytes array
	                           ; error       : in    severity_level      -- Severity level for error at opening (note/warning/error/failure)
	                           ; Size        : inout IntegerProtected    -- Number of bytes read
	                           ; Status      : out   boolean             -- True if entire file has been read
	                           );                                        --
	                                                                     --
	-- Low-level Read File, simplified call                              --
	procedure ReadFile_8       ( Name        : in    string              -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected  -- Output bytes array
	                           ; error       : in    severity_level      -- Severity level for error at opening (note/warning/error/failure)
	                           ; Size        : inout IntegerProtected    -- Number of bytes read
	                           ; Status      : out   boolean             -- True if entire file has been read
	                           );                                        --
	                                                                     --
	-- Low-level Read File, ultra simplified call                        --
	procedure ReadFile_8       ( Name        : in    string              -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected  -- Output bytes array
	                           ; Size        : inout IntegerProtected    -- Number of bytes read
	                           );                                        --
	                                                                     --
	-- Read File with 3bytes data                                        --
	-- (usefull for RAW 24bits pictures)                                 --
	procedure ReadFile_3x8     ( Name        : in    string              -- Name File, including path with "/"
	                           ; RawArray    : inout Byte3ArrayProtected -- Output 3bytes array
	                           );                                        --
	                                                                     --
	-- Read BitMap (may contain Alpha layer)                             --
	procedure ReadBMP          ( Name        : in    string              -- Name File, including path with "/"
	                           ; RGB         : inout RgbArrayProtected   -- RGB array
	                           ; Alpha       : inout ByteArrayProtected  -- Alpha array
	                           ; RdAlpha     : in    boolean := true     -- Extract Alpha layer from file
	                           ; Width       : inout IntegerProtected    -- BitMap Width
	                           ; Height      : inout IntegerProtected    -- BitMap Height
	                           );                                        --
	                                                                     --
	-- Read 24bits BitMap                                                --
	procedure ReadBMP          ( Name        : in    string              -- Name File, including path with "/"
	                           ; RGB         : inout RgbArrayProtected   -- RGB array
	                           ; Width       : inout IntegerProtected    -- BitMap Width
	                           ; Height      : inout IntegerProtected    -- BitMap Height
	                           );                                        --
	                                                                     --
	-- Read a 24bits BitMap and convert it to a Bayer Matrix             --
	procedure ReadBayerFromBMP ( Name        : in    string              -- Name File, including path with "/"
	                           ; FirstPix    : in    string              -- 2 first pixels are "RG", "GR", "BG" or "GB"
	                           ; Bayer       : inout ByteArrayProtected  -- Output bytes array
	                           ; Width       : inout IntegerProtected    -- BitMap Horizontal size
	                           ; Height      : inout IntegerProtected    -- BitMap Vertical   size
	                           );                                        --
	                                                                     --
	-- Read a 24bits BitMap and convert it to YUV                        --
	procedure ReadYUVFromBMP   ( Name        : in    string              -- Name File, including path with "/"
	                           ; FirstPix    : in    string              -- first pixels type
	                           ; VideoType   : in    string              -- "PAL", "SECAM", "NTSC"
	                           ; YUV         : inout YuvArrayProtected   -- Output bytes array
	                           ; Width       : inout IntegerProtected    -- BitMap Horizontal size
	                           ; Height      : inout IntegerProtected    -- BitMap Vertical   size
	                           );                                        --
	                                                                     --
	-- Read a 24bits BitMap and convert it to YCbCr                      --
	procedure ReadYCbCrFromBMP ( Name        : in    string              -- Name File, including path with "/"
	                           ; FirstPix    : in    string              -- first pixels type
	                           ; YCbCr       : inout YCbCrArrayProtected -- Output bytes array
	                           ; Width       : inout IntegerProtected    -- BitMap Horizontal size
	                           ; Height      : inout IntegerProtected    -- BitMap Vertical   size
	                           );                                        --
	                                                                     --
	--==================================================================================================
	--                                   O V E R W R I T E
	--==================================================================================================
	-- Low-level Write File
	procedure WriteFile_8      ( Name        : in    string               -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected   -- Byte array
	                           ; Size        : in    int                  -- Number of bytes to write
	                           ; verbose     : in    boolean     :=  true -- Display "success" message (but always display errors)
	                           ; Status      : out   boolean              -- True if entire file has been written
	                           );                                         --
	                                                                      --
	-- Low-level Write File, simplified call                              --
	procedure WriteFile_8      ( Name        : in    string               -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected   -- Byte array
	                           ; Size        : in    int                  -- Number of bytes to write
	                           ; verbose     : in    boolean     :=  true -- Display "success" message (but always display errors)
	                           );                                         --
	                                                                      --
	procedure WriteBMP         ( Name        : in    string               -- Name File, including path with "/"
	                           ; RGB         : inout RgbArrayProtected    -- RGB array
	                           ; Width       : in    int                  -- BitMap Width
	                           ; Height      : in    int                  -- BitMap Height
	                           ; BitPerPix   : in    int         :=    24 -- Number of bits per pixel
	                           ; CompMode    : in    int         :=     0 -- Compression Mode
	                           ; verbose     : in    boolean     :=  true -- Verbose mode
	                           );                                         --
	                                                                      --
	procedure WriteBMP         ( Name        : in    string               -- Name File, including path with "/"
	                           ; RGB         : inout RgbArrayProtected    -- RGB array
	                           ; Alpha       : inout ByteArrayProtected   -- Alpha array
	                           ; Width       : in    int                  -- BitMap Width
	                           ; Height      : in    int                  -- BitMap Height
	                           ; BitPerPix   : in    int         :=    24 -- Number of bits per pixel
	                           ; CompMode    : in    int         :=     0 -- Compression Mode
	                           ; verbose     : in    boolean     :=  true -- Verbose mode
	                           );                                         --
	                                                                      --
	procedure WriteBMPFromYCbCr( Name        : in    string               -- Name File, including path with "/"
	                           ; YCbCr       : inout YCbCrArrayProtected  -- YCbCr Array
	                           ; Width       : in    int                  -- BitMap Width
	                           ; Height      : in    int                  -- BitMap Height
	                           );                                         --
	                                                                      --
	procedure WriteWave        ( Name        : in    string               -- Name File, including path with "/"
	                           ; DataSize    : in    int                  -- Final wave file size = this value + 44
	                           ; Sampling    : in    int                  -- Number of samples per second (44.1KHz, 48KHz...)
	                           ; BitsPerSamp : in    int                  -- Number of bits per sample
	                           ; NbChannels  : in    int     range 1 to 6 -- Number of audio channels
	                           ; Wav         : inout ByteArrayProtected   -- RAW wave file (without header)
	                           );                                         --
	                                                                      --
	--==================================================================================================
	--                                          A P P E N D
	--==================================================================================================
	procedure AppendFile_8     ( Name        : in    string               -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected   -- Byte array
	                           ; Size        : in    int                  -- Number of bytes to write
	                           ; verbose     : in    boolean     :=  true -- Display "success" message (but always display errors)
	                           ; Status      : out   boolean              -- True if entire file has been written
	                           );                                         --
	                                                                      --
	-- Low-level Append File, simplified call                             --
	procedure AppendFile_8     ( Name        : in    string               -- Name File, including path with "/"
	                           ; RawArray    : inout ByteArrayProtected   -- Byte array
	                           ; Size        : in    int                  -- Number of bytes to write
	                           ; verbose     : in    boolean     :=  true -- Display "success" message (but always display errors)
	                           );                                         --
	                                                                      --
	--==================================================================================================
	--                                        T X T   F I L E
	--==================================================================================================
	procedure TXT_Open         ( file     f_handle     :       text                -- File handle
	                           ;          f_name       : in    string              -- File to be opened
	                           ;          f_mode       : in    file_open_kind      -- Open mode (READ_MODE / WRITE_MODE / APPEND_MODE)
	                           );                                                  --
	                                                                               ------------------------------------------------------
	procedure TXT_Close        ( file     f_handle     :       text                -- File's handle to close
	                           );                                                  --
	                                                                               --
	procedure TXT_Write        ( file     f_handle     :       text                -- File's handle to write to
	                           ;          s            : in    string              -- String to write
	                           );                                                  --
	                                                                               ------------------------------------------------------
	procedure TXT_Write        ( file     f_handle     :       text                -- File's handle to write to
	                           ; variable l            : inout line                -- Line to write (cleared by IEEE library)
	                           ;          clear_l      : in    boolean := true     -- Allow not clearing 'l' by IEEE library
	                           );                                                  --
	                                                                               ------------------------------------------------------
	procedure TXT_Write        (          f_name       : in    string              -- Open/Write/Close
	                           ;          s            : in    string              -- String to write
	                           ;          overwrite    : in    boolean := false    -- Overwrite content
	                           );                                                  --
	                                                                               ------------------------------------------------------
	procedure TXT_Read         ( file     f_handle     :       text                -- File's handle to write to
	                           ; variable l            : inout line                -- Line to read
	                           ; variable eof          : out   boolean             -- End Of File reached
	                           );                                                  --
	--==================================================================================================
	--                                W R I T E   T O   H T M L   F I L E
	--==================================================================================================
	procedure WriteHTMLHeader  ( file f : text; s  : in string);                               -- Write HTML Header to file
	procedure WriteHTML        ( file f : text; s1 : in string; stamp : in boolean :=true);
	procedure WriteHTML        ( file f : text; s1 : in string;
	                                            s2 : in string; stamp : in boolean :=true);
	procedure WriteHTML        ( file f : text; s1 : in string;
	                                            s2 : in string;
	                                            s3 : in string; stamp : in boolean :=true);
	procedure WriteHTML        ( file f : text; s1 : in string;
	                                            s2 : in string;
	                                            s3 : in string;
	                                            s4 : in string; stamp : in boolean :=true);
	procedure WriteHTML        ( file f : text; s1 : in string;
	                                            s2 : in string;
	                                            s3 : in string;
	                                            s4 : in string;
	                                            s5 : in string; stamp : in boolean :=true);
	--==================================================================================================
	--                                             M I S C
	--==================================================================================================
	procedure Free ( variable MyArray   : inout ByteArray ); procedure Free ( variable MyArray   : inout ByteArrayProtected );
	procedure Free ( variable MyArray   : inout Byte3Array); procedure Free ( variable MyArray   : inout Byte3ArrayProtected);
	procedure Free ( variable MyArray   : inout RgbArray  ); procedure Free ( variable MyArray   : inout RgbArrayProtected  );
	procedure Free ( variable MyArray   : inout YuvArray  ); procedure Free ( variable MyArray   : inout YuvArrayProtected  );
	procedure Free ( variable MyArray   : inout YCbCrArray); procedure Free ( variable MyArray   : inout YCbCrArrayProtected);

	--==================================================================================================
	--                                VARIABLE   <==>   SHARED  VARIABLE
	--==================================================================================================
	procedure to_SharedVariable(variable v : in int       ; sv : inout IntegerProtected   );
	procedure to_SharedVariable(variable v : in ByteArray ; sv : inout ByteArrayProtected );
	procedure to_SharedVariable(variable v : in Byte3Array; sv : inout Byte3ArrayProtected);
	procedure to_SharedVariable(variable v : in RgbArray  ; sv : inout RgbArrayProtected  );
	procedure to_SharedVariable(variable v : in YuvArray  ; sv : inout YuvArrayProtected  );
	procedure to_SharedVariable(variable v : in YCbCrArray; sv : inout YCbCrArrayProtected);

	procedure to_RegularVariable(sv : inout IntegerProtected   ; v : out int       );
	procedure to_RegularVariable(sv : inout ByteArrayProtected ; v : out ByteArray );
	procedure to_RegularVariable(sv : inout Byte3ArrayProtected; v : out Byte3Array);
	procedure to_RegularVariable(sv : inout RgbArrayProtected  ; v : out RgbArray  );
	procedure to_RegularVariable(sv : inout YuvArrayProtected  ; v : out YuvArray  );
	procedure to_RegularVariable(sv : inout YCbCrArrayProtected; v : out YCbCrArray);
end package pkg_file;
--==================================================================================================
--==================================================================================================
--==================================================================================================
package body pkg_file is
	--==================================================================================================
	-- ByteArrayProtected
	--==================================================================================================
	type ByteArrayProtected is protected body
		variable ram : ByteArray;

		impure function Exist(index : nat) return boolean is
		begin
			if ram(index)/=null then return true ;
			else                     return false; end if;
		end function Exist;

		procedure Kill(index : nat) is
		begin
			deallocate(ram(index));
		end procedure;

		procedure KillAll is
			variable i : int; -- Loop index
		begin
			i := 0;
			while (ram(i)/=null) loop
				deallocate(ram(i));
				i := i + 1;
			end loop;
		end procedure KillAll;

		impure function Length return nat is
		begin
			return ram'length;
		end function Length;

		impure function Read(index : nat) return ByteRec is
			variable result_Ptr : BytePtr;
			variable result_Rec : ByteRec;
		begin
			result_Ptr := ram(index);
			if result_Ptr/=null then
				result_Rec.d := result_Ptr.d;
			else
				result_Rec.d := (others=>'-');
			end if;
			return result_Rec;
		end function Read;

		procedure Write(index : nat; data : ByteRec) is
		begin
			if ram(index)=null then
				ram(index) := new ByteRec;
			end if;
			ram(index).d := data.d;
		end procedure Write;
	end protected body ByteArrayProtected;
	--==================================================================================================
	-- Byte3ArrayProtected
	--==================================================================================================
	type Byte3ArrayProtected is protected body
		variable ram : Byte3Array;

		impure function Exist(index : nat) return boolean is
		begin
			if ram(index)/=null then return true ;
			else                     return false; end if;
		end function Exist;

		procedure Kill(index : nat) is
		begin
			deallocate(ram(index));
		end procedure;

		procedure KillAll is
			variable i : int; -- Loop index
		begin
			i := 0;
			while (ram(i)/=null) loop
				deallocate(ram(i));
				i := i + 1;
			end loop;
		end procedure KillAll;

		impure function Length return nat is
		begin
			return ram'length;
		end function Length;

		impure function Read(index : nat) return Byte3Rec is
			variable result_Ptr : Byte3Ptr;
			variable result_Rec : Byte3Rec;
		begin
			result_Ptr := ram(index);
			if result_Ptr/=null then
				result_Rec.d0 := result_Ptr.d0;
				result_Rec.d1 := result_Ptr.d1;
				result_Rec.d2 := result_Ptr.d2;
			else
				result_Rec.d0 := (others=>'-');
				result_Rec.d1 := (others=>'-');
				result_Rec.d2 := (others=>'-');
			end if;
			return result_Rec;
		end function Read;

		procedure Write(index : nat; data : Byte3Rec) is
		begin
			if ram(index)=null then
				ram(index) := new Byte3Rec;
			end if;
			ram(index).d0 := data.d0;
			ram(index).d1 := data.d1;
			ram(index).d2 := data.d2;
		end procedure Write;
	end protected body Byte3ArrayProtected;
	--==================================================================================================
	-- RgbArrayProtected
	--==================================================================================================
	type RgbArrayProtected is protected body
		variable ram : RgbArray;

		impure function Exist(index : nat) return boolean is
		begin
			if ram(index)/=null then return true ;
			else                     return false; end if;
		end function Exist;

		procedure Kill(index : nat) is
		begin
			deallocate(ram(index));
		end procedure;

		procedure KillAll is
			variable i : int; -- Loop index
		begin
			i := 0;
			while (ram(i)/=null) loop
				deallocate(ram(i));
				i := i + 1;
			end loop;
		end procedure KillAll;

		impure function Length return nat is
		begin
			return ram'length;
		end function Length;

		impure function Read(index : nat) return RgbRec is
			variable result_Ptr : RgbPtr;
			variable result_Rec : RgbRec;
		begin
			result_Ptr := ram(index);
			if result_Ptr/=null then
				result_Rec.R := result_Ptr.R;
				result_Rec.G := result_Ptr.G;
				result_Rec.B := result_Ptr.B;
			else
				result_Rec.R := (others=>'-');
				result_Rec.G := (others=>'-');
				result_Rec.B := (others=>'-');
			end if;
			return result_Rec;
		end function Read;

		procedure Write(index : nat; data : RgbRec) is
		begin
			if ram(index)=null then
				ram(index) := new RgbRec;
			end if;
			ram(index).R := data.R;
			ram(index).G := data.G;
			ram(index).B := data.B;
		end procedure Write;
	end protected body RgbArrayProtected;
	--==================================================================================================
	-- YuvArrayProtected
	--==================================================================================================
	type YuvArrayProtected is protected body
		variable ram : YuvArray;

		impure function Exist(index : nat) return boolean is
		begin
			if ram(index)/=null then return true ;
			else                     return false; end if;
		end function Exist;

		procedure Kill(index : nat) is
		begin
			deallocate(ram(index));
		end procedure;

		procedure KillAll is
			variable i : int; -- Loop index
		begin
			i := 0;
			while (ram(i)/=null) loop
				deallocate(ram(i));
				i := i + 1;
			end loop;
		end procedure KillAll;

		impure function Length return nat is
		begin
			return ram'length;
		end function Length;

		impure function Read(index : nat) return YuvRec is
			variable result_Ptr : YuvPtr;
			variable result_Rec : YuvRec;
		begin
			result_Ptr := ram(index);
			if result_Ptr/=null then
				result_Rec.U  := result_Ptr.U ;
				result_Rec.Ya := result_Ptr.Ya;
				result_Rec.V  := result_Ptr.V ;
				result_Rec.Yb := result_Ptr.Yb;
			else
				result_Rec.U  := (others=>'-');
				result_Rec.Ya := (others=>'-');
				result_Rec.V  := (others=>'-');
				result_Rec.Yb := (others=>'-');
			end if;
			return result_Rec;
		end function Read;

		procedure Write(index : nat; data : YuvRec) is
		begin
			if ram(index)=null then
				ram(index) := new YuvRec;
			end if;
			ram(index).U  := data.U ;
			ram(index).Ya := data.Ya;
			ram(index).V  := data.V ;
			ram(index).Yb := data.Yb;
		end procedure Write;
	end protected body YuvArrayProtected;
	--==================================================================================================
	-- YCbCrArrayProtected
	--==================================================================================================
	type YCbCrArrayProtected is protected body
		variable ram : YCbCrArray;

		impure function Exist(index : nat) return boolean is
		begin
			if ram(index)/=null then return true ;
			else                     return false; end if;
		end function Exist;

		procedure Kill(index : nat) is
		begin
			deallocate(ram(index));
		end procedure;

		procedure KillAll is
			variable i : int; -- Loop index
		begin
			i := 0;
			while (ram(i)/=null) loop
				deallocate(ram(i));
				i := i + 1;
			end loop;
		end procedure KillAll;

		impure function Length return nat is
		begin
			return ram'length;
		end function Length;

		impure function Read(index : nat) return YCbCrRec is
			variable result_Ptr : YCbCrPtr;
			variable result_Rec : YCbCrRec;
		begin
			result_Ptr := ram(index);
			if result_Ptr/=null then
				result_Rec.Cb := result_Ptr.Cb;
				result_Rec.Ya := result_Ptr.Ya;
				result_Rec.Cr := result_Ptr.Cr;
				result_Rec.Yb := result_Ptr.Yb;
			else
				result_Rec.Cb := (others=>'-');
				result_Rec.Ya := (others=>'-');
				result_Rec.Cr := (others=>'-');
				result_Rec.Yb := (others=>'-');
			end if;
			return result_Rec;
		end function Read;

		procedure Write(index : nat; data : YCbCrRec) is
		begin
			if ram(index)=null then
				ram(index) := new YCbCrRec;
			end if;
			ram(index).Cb := data.Cb;
			ram(index).Ya := data.Ya;
			ram(index).Cr := data.Cr;
			ram(index).Yb := data.Yb;
		end procedure Write;
	end protected body YCbCrArrayProtected;

--**************************************************************************************************
--*                                    LOW-LEVEL READ OPERATION                                    *
--**************************************************************************************************
-- Read a binary file (any kind of file) and return each byte into a separated vector.
-- Each read operation (from source file) is made by 8bits access.
procedure ReadFile_8( Name      : in    string             --
                    ; verbose   : in    boolean            -- Display "success" message Yes/No (Errors are always displayed)
                    ; RawArray  : inout ByteArrayProtected --
                    ; error     : in    severity_level     --
                    ; Size      : inout IntegerProtected   --
                    ; Status    : out   boolean) is        --
	variable fstatus      : file_open_status                 ; -- Status when opening file
	constant FOPENKIND    : file_open_kind := READ_MODE      ; -- READ_MODE / WRITE_MODE / APPEND_MODE
	variable buff_8bit    : character                        ; -- Read byte is stored into a character object
	variable message      : line                             ; -- Message to display to simulator console
	variable i            : int                              ; -- Loop index
	variable restricted   : boolean        := false          ; -- Read file prematurely stopped
	variable ThisRec      : ByteRec                          ;
	constant ARRAY_LENGTH : nat            := RawArray.length;
begin
	Status := true;

	-- Open file in read-only mode
	file_open(fstatus,RawFile,Name,FOPENKIND);

	-- Check status after open operation
	case fstatus is
		when OPEN_OK      => null;
		when STATUS_ERROR => report "Error Code : STATUS_ERROR";
		                     Status := false;
		when NAME_ERROR   => write(message,string'("File "     )); write(message,Name);
		                     write(message,string'(" NOT found"));
		                     writeline(output,message);
		                     Status := false;
		                     report "Error Code : NAME_ERROR" severity failure;
		when MODE_ERROR   => report "Error Code : MODE_ERROR" severity failure;
		                     Status := false;
	end case;

	Free(RawArray); -- Deallocate all memory if this variable has been already used (second call of this procedure)

	i := 0;
	while not(endfile(RawFile)) loop
		read(RawFile,buff_8bit);                                   -- Read next byte
		ThisRec.d := slv(to_unsigned(character'pos(buff_8bit),8)); -- Character Position is equal to its own value
		RawArray.Write(i,ThisRec);

		-- Restrict analyse to Output Bytes Array Size
		if i=ARRAY_LENGTH then
			restricted := true;
			Status     := false;
			exit;
		end if;
		i := i + 1;
	end loop;
	Size.Set(i);

	if restricted then
		write(message,string'("File "                   )); write(message,Name);
		write(message,string'(" analyzed up to "        )); write(message,i   );
		write(message,string'("th byte"                 ));
		writeline(output,message);
		report "Can't load entire file in memory." severity failure;
	elsif verbose then
		write(message,string'("File "                   )); write(message,Name);
		write(message,string'(" successfully read (all ")); write(message,i   );
		write(message,string'(" bytes were read)."      ));
		writeline(output,message);
	end if;

	-- Close File
	file_close(RawFile);
end procedure ReadFile_8;

procedure ReadFile_8( Name      : in    string             -- Name File, including path with "/"
                    ; RawArray  : inout ByteArrayProtected -- Output bytes array
                    ; error     : in    severity_level     -- Severity level for error at opening (note/warning/error/failure)
                    ; Size      : inout IntegerProtected   -- Number of bytes read
                    ; Status    : out   boolean) is        -- True if entire file has been read
begin
	ReadFile_8(Name=>Name,verbose=>true,RawArray=>RawArray,error=>failure,Size=>Size,Status=>Status);
end procedure;

procedure ReadFile_8( Name     : in    string
                    ; RawArray : inout ByteArrayProtected
                    ; Size     : inout IntegerProtected) is
	variable Status : boolean; -- Status for read operation. It isn't forwarded to caller
begin
	ReadFile_8(Name=>Name,verbose=>true,RawArray=>RawArray,error=>failure,Size=>Size,Status=>Status);
end procedure ReadFile_8;
--*************************************************************************************************
--*                                    Others Read Operations                                     *
--*************************************************************************************************
--=================================================================================================
-- R A W , 3 x 8bits
--=================================================================================================
-- Read a binary file as 3 x 1byte
-- This is usefull for RGB files
procedure ReadFile_3x8( Name     : in    string
                      ; RawArray : inout Byte3ArrayProtected) is
	variable temp         : ByteArrayProtected          ; -- Temporary buffer
	variable Size         : IntegerProtected            ; -- Read size (not forwarded to caller)
	variable i            : int                         ; -- Loop index
	variable ThisRec      : Byte3Rec                    ;
begin
	ReadFile_8(Name,temp,Size);

	i := 0;
	while(temp.Exist(3*i)) loop
		ThisRec.d0 := temp.Read(3*i+0).d;
		ThisRec.d1 := temp.Read(3*i+1).d;
		ThisRec.d2 := temp.Read(3*i+2).d;
		RawArray.Write(i,ThisRec);
		i := i + 1;
	end loop;

	Free(temp); -- Deallocate all memory if this variable has been already used (second call of this procedure)
end procedure ReadFile_3x8;
--=================================================================================================
-- B M P
--=================================================================================================
procedure ReadBMP( Name     : in    string                 -- Name File, including path with "/"
                 ; RGB      : inout RgbArrayProtected      -- RGB array
                 ; Alpha    : inout ByteArrayProtected     -- Alpha array
                 ; RdAlpha  : in    boolean := true        -- Extract Alpha layer from file
                 ; Width    : inout IntegerProtected       -- BitMap Width
                 ; Height   : inout IntegerProtected) is   -- BitMap Height
/*
BitMap file format description
All multiple word are LSB first
--------------------
Header
--------------------
00 - 01 : ASCII 2byte "BM" bitmap identifier
02 - 05 : Total length of bitmap file in bytes. Four byte integer.
06 - 09 : Reserved (possibly for image ID or revision). Four byte integer.
10 - 13 : Offset to start of actual pixel data. Four byte integer.
--------------------
Info Header
--------------------
14 - 17 : Size of data header, usually 40bytes. Four byte integer.
18 - 21 : Width of bitmap in pixels. Four byte integer.
22 - 25 : Height of bitmap in pixels. Four byte integer.
26 - 27 : Number of color planes. Usually 01. Two byte integer.
28 - 29 : Number of bits per pixel. Sets color mode.
           1 : monochrome
           4 : 16 lookup colors
           8 : 256 lookup colors
          16 : 65.536 lookup colors
          24 : 16.777.216 RGB colors
          32 : 16.777.216 RGB colors + alpha
30 - 33 : Compression mode in use. Four byte integer.
           0 : None
           1 : 8bit Run Length Encoded
           2 : 4bit Run Length Encoded
           3 : RGB bitmap with mask
34 - 37 : Size of stored pixel data. Four byte integer.
38 - 41 : Width resolution in pixels per meter. Four byte integer.
42 - 45 : Height resolution in pixels per meter. Four byte integer.
46 - 49 : Number of colors actually used. Four byte integer.
50 - 53 : Number of important colors. Four byte integer.
--------------------
Optionnal Palette
--------------------
Size is 4 * Number of colors used (set in Info Header, byte 46-49)
--------------------
Image data
--------------------
*/
	variable bmp          : ByteArrayProtected            ; -- BitMap array
	variable BmpSize      : int                           ; -- BitMap Total length (in bytes)
	variable ImageData    : int                           ; -- Offset to start of actual pixel data
	variable InfHdrSize   : int                           ; -- Info Header Size
	variable NbPlanes     : int                           ; -- Number of color planes
	variable BitPerPix    : int                           ; -- Number of bits per pixel
	variable CompMode     : int                           ; -- Compression Mode
	variable DataSize     : int                           ; -- Size of stored pixel data
	variable ResX         : int                           ; -- Width resolution in pixels per meter
	variable ResY         : int                           ; -- Height resolution in pixels per meter
	variable NbColors     : int                           ; -- Number of colors actually used
	variable NbImpColors  : int                           ; -- Number of important colors

	variable Size         : IntegerProtected              ; -- Size (rounded up to 32bits)
	variable message      : line                          ; -- Message to output to simulator console
	variable p,x,y        : int                           ; -- Current Output pixel, (x,y) position
	variable i            : int                           ; -- Current Input byte
	variable Status       : boolean                       ; -- Status opened file
	variable Padding      : int     range 0 to 3          ; -- Number of bytes for 32-bits line alignment

	variable ColorLUT     : Byte3Array                    ; -- Color Lookup Table (65535 max)
	variable ThisRGBRec   : RgbRec                        ;
	variable ThisAlphaRec : ByteRec                       ;
begin
	ReadFile_8(Name=>Name,verbose=>false,RawArray=>bmp,error=>failure,Size=>Size,Status=>Status);
	------------------------------
	-- Check BMP Header
	------------------------------
	-- Byte 00-01 : ASCII 2bytes "BM" bitmap identifier.
	if not(uns(bmp.Read(0).d)=character'pos('B') and uns(bmp.Read(1).d)=character'pos('M')) then
		write(message,string'("File ")); write(message,Name);
		write(message,string'(" is not a BitMap picture."));
		writeline(output,message);
		report "Simulation stopped due to previous error." severity failure;
	elsif not(Status) then
		report "Can't load entire bitmap in memory." severity failure;
	end if;

	BmpSize     := conv_int(uns(bmp.Read(05).d) & uns(bmp.Read(04).d) & uns(bmp.Read(03).d) & uns(bmp.Read(02).d)); -- Total length of bitmap file in bytes
	ImageData   := conv_int(uns(bmp.Read(13).d) & uns(bmp.Read(12).d) & uns(bmp.Read(11).d) & uns(bmp.Read(10).d)); -- Offset to start of actual pixel data

	if Size.value-BmpSize>3 then
		write(message,string'("Error : Opened bitmap may be corrupted !"));
		writeline(output,message);
		report "Simulation stopped due to previous error." severity failure;
	else
		write(message,string'("File "                   )); write(message,Name   );
		write(message,string'(" successfully read (all ")); write(message,BmpSize);
		write(message,string'(" bytes were read)."      ));
		writeline(output,message);
	end if;
	------------------------------
	-- Check BMP Info Header
	------------------------------
	InfHdrSize  := conv_int(uns(bmp.Read(17).d) & uns(bmp.Read(16).d) & uns(bmp.Read(15).d) & uns(bmp.Read(14).d)) ; -- Size of data header, usually 40bytes
	Width .Set    (conv_int(uns(bmp.Read(21).d) & uns(bmp.Read(20).d) & uns(bmp.Read(19).d) & uns(bmp.Read(18).d))); -- Width of bitmap in pixels
	Height.Set    (conv_int(uns(bmp.Read(25).d) & uns(bmp.Read(24).d) & uns(bmp.Read(23).d) & uns(bmp.Read(22).d))); -- Height of bitmap in pixels
	NbPlanes    := conv_int(                                            uns(bmp.Read(27).d) & uns(bmp.Read(26).d)) ; -- Number of color planes. Usually 01
	BitPerPix   := conv_int(                                            uns(bmp.Read(29).d) & uns(bmp.Read(28).d)) ; -- Number of bits per pixel
	CompMode    := conv_int(uns(bmp.Read(33).d) & uns(bmp.Read(32).d) & uns(bmp.Read(31).d) & uns(bmp.Read(30).d)) ; -- Compression Mode
	DataSize    := conv_int(uns(bmp.Read(37).d) & uns(bmp.Read(36).d) & uns(bmp.Read(35).d) & uns(bmp.Read(34).d)) ; -- Size of stored pixel data
	ResX        := conv_int(uns(bmp.Read(41).d) & uns(bmp.Read(40).d) & uns(bmp.Read(39).d) & uns(bmp.Read(38).d)) ; -- Width resolution in pixels per meter
	ResY        := conv_int(uns(bmp.Read(45).d) & uns(bmp.Read(44).d) & uns(bmp.Read(43).d) & uns(bmp.Read(42).d)) ; -- Height resolution in pixels per meter
	NbColors    := conv_int(uns(bmp.Read(49).d) & uns(bmp.Read(48).d) & uns(bmp.Read(47).d) & uns(bmp.Read(46).d)) ; -- Number of colors actually used
	NbImpColors := conv_int(uns(bmp.Read(53).d) & uns(bmp.Read(52).d) & uns(bmp.Read(51).d) & uns(bmp.Read(50).d)) ; -- Number of important colors

	-- Check that bitmap contains only one color plane
	if NbPlanes/=1 then
		write(message,string'("Only one color plane is allowed"));
		writeline(output,message);
		report "Simulation stopped due to previous error." severity failure;
	end if;

	-- Check number of bits per pixel
	case BitPerPix is
		when  1     =>
			write(message,string'("Unsupported 1bit color mode!!"));
			writeline(output,message);
			report "Simulation stopped due to previous error." severity failure;
		when  4     =>
			for i in 0 to 15 loop
				ColorLUT(i)    := new Byte3Rec;
				ColorLUT(i).d0 := bmp.Read(54+i*4+2).d; -- Red
				ColorLUT(i).d1 := bmp.Read(54+i*4+1).d; -- Green
				ColorLUT(i).d2 := bmp.Read(54+i*4  ).d; -- Blue
			end loop;
			write(message,string'("Bitmap is in 4bits per pixel"));
			writeline(output,message);
		when  8     =>
			for i in 0 to 255 loop
				ColorLUT(i)    := new Byte3Rec;
				ColorLUT(i).d0 := bmp.Read(54+i*4+2).d; -- Red
				ColorLUT(i).d1 := bmp.Read(54+i*4+1).d; -- Green
				ColorLUT(i).d2 := bmp.Read(54+i*4  ).d; -- Blue
			end loop;
			write(message,string'("Bitmap is in 8bits per pixel"));
			writeline(output,message);
		when 16     =>
			for i in 0 to 65535 loop
				ColorLUT(i)    := new Byte3Rec;
				ColorLUT(i).d0 := bmp.Read(54+i*4+2).d; -- Red
				ColorLUT(i).d1 := bmp.Read(54+i*4+1).d; -- Green
				ColorLUT(i).d2 := bmp.Read(54+i*4  ).d; -- Blue
			end loop;
			write(message,string'("Bitmap is in 16bits per pixel"));
			writeline(output,message);
		when 24     =>
		when 32     =>
		when others =>
			write(message,string'("Unknown Number of bits per pixel format"));
			writeline(output,message);
			report "Simulation stopped due to previous error." severity failure;
	end case;

	-- Check that bitmap is not compressed
	if CompMode/=0 then
		write(message,string'("BitMap must not be compressed"));
		writeline(output,message);
		report "Simulation stopped due to previous error." severity failure;
	end if;
	------------------------------
	-- Extract BMP Image Data
	------------------------------
	-- Determine Padding value
	if ((Width.value*BitPerPix/8) mod 4)/=0 then Padding := 4 - ((Width.value*BitPerPix/8) mod 4);
	else                                         Padding := 0                                    ; end if;

	-- Extract BMP Picture (bmp is reverse written)
	i :=      ImageData; -- Go to Image Data section (usually 54th byte)
	x :=              0;
	y := Height.value-1; -- BitMap is line-reverse written
	while not(y<0) loop
		p := y*Width.value + x;
		case BitPerPix is
			when  4     =>
				ThisRGBRec.R := ColorLUT( conv_int ( uns ( bmp.Read(i).d(7 downto 4) ) ) ).d0; -- Red   (1/2 pixel)
				ThisRGBRec.G := ColorLUT( conv_int ( uns ( bmp.Read(i).d(7 downto 4) ) ) ).d1; -- Green (1/2 pixel)
				ThisRGBRec.B := ColorLUT( conv_int ( uns ( bmp.Read(i).d(7 downto 4) ) ) ).d2; -- Blue  (1/2 pixel)
				RGB.Write(p,ThisRGBRec);

				ThisRGBRec.R := ColorLUT( conv_int ( uns ( bmp.Read(i).d(3 downto 0) ) ) ).d0; -- Red   (2/2 pixel)
				ThisRGBRec.G := ColorLUT( conv_int ( uns ( bmp.Read(i).d(3 downto 0) ) ) ).d1; -- Green (2/2 pixel)
				ThisRGBRec.B := ColorLUT( conv_int ( uns ( bmp.Read(i).d(3 downto 0) ) ) ).d2; -- Blue  (2/2 pixel)
				RGB.Write(p+1,ThisRGBRec);
				i := i+1;                                                                      -- 1byte   forward
				x := x+2;                                                                      -- 2pixels forward
			when  8     =>
				ThisRGBRec.R := ColorLUT( conv_int ( uns ( bmp.Read(i).d             ) ) ).d0; -- Red
				ThisRGBRec.G := ColorLUT( conv_int ( uns ( bmp.Read(i).d             ) ) ).d1; -- Green
				ThisRGBRec.B := ColorLUT( conv_int ( uns ( bmp.Read(i).d             ) ) ).d2; -- Blue
				RGB.Write(p,ThisRGBRec);
				i := i+1;                                                                      -- 1byte   forward
				x := x+1;                                                                      -- 1pixel  forward
			when 24     =>
				ThisRGBRec.R := bmp.Read(i+2).d;                                               -- Red
				ThisRGBRec.G := bmp.Read(i+1).d;                                               -- Green
				ThisRGBRec.B := bmp.Read(i  ).d;                                               -- Blue
				RGB.Write(p,ThisRGBRec);
				i := i+3;                                                                      -- 3bytes  forward
				x := x+1;                                                                      -- 1pixel  forward
			when 32     =>
				ThisRGBRec.R   := bmp.Read(i+2).d;                                             -- Red
				ThisRGBRec.G   := bmp.Read(i+1).d;                                             -- Green
				ThisRGBRec.B   := bmp.Read(i  ).d;                                             -- Blue
				ThisAlphaRec.d := bmp.Read(i+3).d;                                             -- Alpha

				RGB.Write  (p,ThisRGBRec  );
				Alpha.Write(p,ThisAlphaRec);
				i := i+4;                                                                      -- 4bytes  forward
				x := x+1;                                                                      -- 1pixel  forward
			when others => null;
		end case;

		if x=Width.value then
			x :=   0;
			y := y-1;
			i := i + Padding; -- Each line is 32bits aligned (some 0s may be added, BitMap specificity)
		end if;
	end loop;
end procedure ReadBMP;

-- Read 24bits BitMap
procedure ReadBMP( Name   : in    string               -- Name File, including path with "/"
                 ; RGB    : inout RgbArrayProtected    -- RGB array
                 ; Width  : inout IntegerProtected     -- BitMap Width
                 ; Height : inout IntegerProtected) is -- BitMap Height
	variable Alpha : ByteArrayProtected; -- Temporary buffer to give something to called function
begin
	ReadBMP( Name    => Name
	       , RGB     => RGB
	       , Alpha   => Alpha
	       , RdAlpha => false
	       , Width   => Width
	       , Height  => Height
	      );

	Free(Alpha); -- Deallocate all memory if this variable has been already used (second call of this procedure)
end procedure ReadBMP;
--=================================================================================================
-- Convert BMP24 to Bayer
--=================================================================================================
procedure ReadBayerFromBMP( Name     : in    string
                          ; FirstPix : in    string               -- "RG", "GR", "BG", "GB"
                          ; Bayer    : inout ByteArrayProtected
                          ; Width    : inout IntegerProtected
                          ; Height   : inout IntegerProtected) is
-- Bayer Matrix :
--        R G R G R G R G R G R G R G R G R G R G R G R G --> Red  Line
--        G B G B G B G B G B G B G B G B G B G B G B G B --> Blue Line
--        R G R G R G R G R G R G R G R G R G R G R G R G --> Red  Line
--        G B G B G B G B G B G B G B G B G B G B G B G B --> Blue Line
-- Only 1 primary color is kept for each 24bits pixel
	type Color is (Red, Green, Blue);
	variable RGB     : RgbArrayProtected; -- Read bitmap
	variable CurPix  : Color            ; -- Current pixel
	variable CurLine : Color            ; -- Current line
	variable i       : int              ; -- Loop index
	variable ThisRec : ByteRec          ;
begin
	ReadBMP(Name=>Name,RGB=>RGB,Width=>Width,Height=>Height);

	-- Determine First Line & First Pixel color
	   if FirstPix="RG" then CurLine := Red ; CurPix := Red  ;
	elsif FirstPix="GR" then CurLine := Red ; CurPix := Green;
	elsif FirstPix="BG" then CurLine := Blue; CurPix := Blue ;
	elsif FirstPix="GB" then CurLine := Blue; CurPix := Green;
	else
		assert false report "Wrong FirstPix value" severity failure;
	end if;

	for y in 0 to Height.value-1 loop                                            -- Height lines
		for x in 0 to Width.value-1 loop                                         -- Width  pixels
			if    CurPix=Red   then ThisRec.d := RGB.Read(Width.value*y+x).R;
			elsif CurPix=Green then ThisRec.d := RGB.Read(Width.value*y+x).G;
			else                    ThisRec.d := RGB.Read(Width.value*y+x).B; end if;
			Bayer.Write(Width.value*y+x,ThisRec);

			-- Switch pixel color
			if    CurLine=Red  then
				if CurPix=Red  then CurPix := Green;
				else                CurPix := Red  ; end if;
			elsif CurLine=Blue then
				if CurPix=Blue then CurPix := Green;
				else                CurPix := Blue ; end if;
			else
				assert false report "Wrong Color Line" severity failure;
			end if;
		end loop;

		-- Switch line color
		if CurLine=Red then CurLine := Blue;
		else                CurLine := Red ; end if;

		-- Compute color for the First pixel of the next line
		if CurLine=Red then
			if    FirstPix="RG" then CurPix := Red  ;
			elsif FirstPix="GR" then CurPix := Green;
			elsif FirstPix="BG" then CurPix := Green;
			elsif FirstPix="GB" then CurPix := Red  ; end if;
		elsif CurLine=Blue then
			if    FirstPix="RG" then CurPix := Green;
			elsif FirstPix="GR" then CurPix := Blue ;
			elsif FirstPix="BG" then CurPix := Blue ;
			elsif FirstPix="GB" then CurPix := Green; end if;
		end if;
	end loop;

	Free(RGB);
end procedure ReadBayerFromBMP;
--=================================================================================================
-- Convert BMP24 to YUV
--=================================================================================================
procedure ReadYUVFromBMP( Name      : in    string            -- Name File, including path with "/"
                        ; FirstPix  : in    string            -- first pixels type
                        ; VideoType : in    string            -- "PAL, "SECAM", "NTSC"
                        ; YUV       : inout YuvArrayProtected -- Output bytes array
                        ; Width     : inout IntegerProtected  -- BitMap Horizontal size
                        ; Height    : inout IntegerProtected  -- BitMap Vertical   size
                        ) is
-- YUV stream : U0 Y0 V0 Y1 U2 Y2 V2 Y3 U4 Y4 V4...
--
-- PAL & NTSC
--      Y                  =   0.299*R + 0.587*G + 0.114*B
--      U =  0.492 (B - Y) = - 0.147*R - 0.289*G + 0.436*B
--      V =  0.877 (R - Y) =   0.615*R - 0.515*G - 0.100*B
-- SECAM
--      Y                  =   0.299*R + 0.587*G + 0.114*B
--      U =  1.500 (B - Y) = - 0.449*R - 0.881*G + 1.329*B
--      V = -1.900 (R - Y) = - 1.332*R + 1.115*G - 0.217*B
--
	variable RGB     : RgbArrayProtected; -- Read bitmap
	variable i       : int              ; -- Loop index
	variable i_rgb   : int              ; -- Index for RGB stream
	variable i_yuv   : int              ; -- Index for YUV stream
	variable ThisRec : YuvRec           ; -- Temporary record
begin
	assert FirstPix="U0Y0V0Y1" report "FirstPix unknown value !! Allowed values : U0Y0V0Y1" severity failure;
	assert VideoType="PAL" or VideoType="SECAM" or VideoType="NTSC" report "VideoType unknown value !!" severity failure;

	ReadBMP(Name=>Name,RGB=>RGB,Width=>Width,Height=>Height);

	if FirstPix="U0Y0V0Y1" then
		i_rgb := 0;
		i_yuv := 0;
		if VideoType="PAL" or VideoType="NTSC" then
			while (i_rgb<=Width.value*Height.value-1) loop
				ThisRec.Ya := conv_slv(conv_int(    299*uns(RGB.Read(i_rgb  ).R) +  587*uns(RGB.Read(i_rgb  ).G) +  114*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.U  := conv_slv(conv_int(0 - 147*uns(RGB.Read(i_rgb  ).R) -  289*uns(RGB.Read(i_rgb  ).G) +  436*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.V  := conv_slv(conv_int(    615*uns(RGB.Read(i_rgb  ).R) -  515*uns(RGB.Read(i_rgb  ).G) -  100*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.Yb := conv_slv(conv_int(    299*uns(RGB.Read(i_rgb+1).R) +  587*uns(RGB.Read(i_rgb+1).G) +  114*uns(RGB.Read(i_rgb+1).B) ) / 1000,8);
				YUV.Write(i_yuv,ThisRec);
				i_yuv := i_yuv + 1;
				i_rgb := i_rgb + 2;
			end loop;
		else -- "SECAM"
			while (i_rgb<=Width.value*Height.value-1) loop
				ThisRec.Ya := conv_slv(conv_int(    299*uns(RGB.Read(i_rgb  ).R) +  587*uns(RGB.Read(i_rgb  ).G) +  114*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.U  := conv_slv(conv_int(0 - 449*uns(RGB.Read(i_rgb  ).R) -  881*uns(RGB.Read(i_rgb  ).G) + 1329*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.V  := conv_slv(conv_int(0 -1332*uns(RGB.Read(i_rgb  ).R) + 1115*uns(RGB.Read(i_rgb  ).G) -  217*uns(RGB.Read(i_rgb  ).B) ) / 1000,8);
				ThisRec.Yb := conv_slv(conv_int(    299*uns(RGB.Read(i_rgb+1).R) +  587*uns(RGB.Read(i_rgb+1).G) +  114*uns(RGB.Read(i_rgb+1).B) ) / 1000,8);
				YUV.Write(i_yuv,ThisRec);
				i_yuv := i_yuv + 1;
				i_rgb := i_rgb + 2;
			end loop;
		end if;
	end if;

	Free(RGB);
end procedure ReadYUVFromBMP;
--=================================================================================================
-- Convert BMP24 to YCbCr
--=================================================================================================
procedure ReadYCbCrFromBMP( Name      : in    string              -- Name File, including path with "/"
                          ; FirstPix  : in    string              -- First pixels type
                          ; YCbCr     : inout YCbCrArrayProtected -- Bytes array
                          ; Width     : inout IntegerProtected    -- BitMap Horizontal size
                          ; Height    : inout IntegerProtected    -- BitMap Vertical   size
                          ) is
-- YCbCr stream : Cb0 Y0 Cr0 Y1 Cb2 Y2 Cr2 Y3 Cb4 Y4 Cr4...
-- For RGB in [16;235], use following coefficients
-- Y                          = (  77R + 150G +  29B) / 256
-- Cb = (B - Y) / 1.772 + 0.5 = (- 43R -  85G + 128B) / 256 + 128
-- Cr = (R - Y) / 1.402 + 0.5 = ( 128R - 107G -  21B) / 256 + 128
--
-- For RGB in [0;255], use following coefficients. This set is used because source is a computer BitMap
-- Y                          =  0.257R + 0.504G + 0.098B +  16
-- Cb = (B - Y) / 1.772 + 0.5 = -0.148R - 0.291G + 0.439B + 128
-- Cr = (R - Y) / 1.402 + 0.5 =  0.439R - 0.368G - 0.071B + 128
	variable RGB     : RgbArrayProtected; -- Read bitmap
	variable i       : int              ; -- Loop index
	variable i_rgb   : int              ; -- Index for RGB   stream
	variable i_ycbcr : int              ; -- Index for YCbCr stream
	variable ThisRec : YCbCrRec         ; -- Temporary record
	--                                                              R        G        B
	constant COEFF   : RealMatrix(1 to 3,1 to 3) := RealMatrix'( ( 0.257 ,  0.504 ,  0.098)    -- Y
	                                                           , (-0.148 , -0.291 ,  0.439)    -- Cb
	                                                           , ( 0.439 , -0.368 , -0.071) ); -- Cr

	--                                                                     Y       Cb      Cr
	constant OFFSET  : RealMatrix(1 to 3,1 to 1) := MatrixCol(RealArray'( 16.0 , 128.0 , 128.0 )); -- YCbCr offset

	variable Sample0 : RealMatrix(1 to 3,1 to 1); -- RGB pixel
	variable Sample1 : RealMatrix(1 to 3,1 to 1); -- RGB pixel
	variable Result0 : RealMatrix(1 to 3,1 to 1); -- YCbCr pixel (full result : Y, Cb and Cr)
	variable Result1 : RealMatrix(1 to 3,1 to 1); -- YCbCr pixel (full result : Y, Cb and Cr)
begin
	assert FirstPix="Cb0Y0Cr0Y1" report "FirstPix unknown value !! Allowed values : Cb0Y0Cr0Y1" severity failure;

	ReadBMP(Name=>Name,RGB=>RGB,Width=>Width,Height=>Height);

	if FirstPix="Cb0Y0Cr0Y1" then
		i_rgb   := 0;
		i_ycbcr := 0;

		-- Scan all RGB pixels
		while (i_rgb<=Width.value*Height.value-1) loop
			-- Extract both RGB pixels
			Sample0(1,1) := real(conv_int(uns(RGB.Read(i_rgb).R)));    Sample1(1,1) := real(conv_int(uns(RGB.Read(i_rgb+1).R)));
			Sample0(2,1) := real(conv_int(uns(RGB.Read(i_rgb).G)));    Sample1(2,1) := real(conv_int(uns(RGB.Read(i_rgb+1).G)));
			Sample0(3,1) := real(conv_int(uns(RGB.Read(i_rgb).B)));    Sample1(3,1) := real(conv_int(uns(RGB.Read(i_rgb+1).B)));

			-- Convert both RGB to YCbCr (one Cb and one Cr for second pixel are useless, but let's go anyway...)
			Result0 := (COEFF * Sample0) + OFFSET;
			Result1 := (COEFF * Sample1) + OFFSET;

			-- Avoid negative or too high values
			for i in 1 to 3 loop
				if    Result0(i,1)<=  0.0 then Result0(i,1) :=   0.0;
				elsif Result0(i,1)>=255.0 then Result0(i,1) := 255.0; end if;
			end loop;

			-- Avoid negative or too high values
			for i in 1 to 3 loop
				if    Result1(i,1)<=  0.0 then Result1(i,1) :=   0.0;
				elsif Result1(i,1)>=255.0 then Result1(i,1) := 255.0; end if;
			end loop;

			-- Construct YCbCr pixel
			ThisRec.Ya := conv_slv(int(Result0(1,1)),8); -- Y  from pixel #0
			ThisRec.Yb := conv_slv(int(Result1(1,1)),8); -- Y  from pixel #1
			ThisRec.Cb := conv_slv(int(Result0(2,1)),8); -- Cb from pixel #0
			ThisRec.Cr := conv_slv(int(Result0(3,1)),8); -- Cr from pixel #0
			YCbCr.Write(i_ycbcr,ThisRec);
			i_ycbcr := i_ycbcr + 1;
			i_rgb   := i_rgb   + 2;
		end loop;
	end if;

	Free(RGB);
end procedure ReadYCbCrFromBMP;
--*************************************************************************************************
--*                                   LOW-LEVEL WRITE OPERATION                                   *
--*************************************************************************************************
-- Write a binary file (any kind of file) and return each byte into a separated vector.
-- Each Write operation (from source file) is made by 8bits accesses.
procedure WriteAppendFile_8( Name     : in    string             -- Name File, including path with "/"
                           ; OpenMode : in    file_open_kind     -- READ_MODE / WRITE_MODE / APPEND_MODE
                           ; RawArray : inout ByteArrayProtected -- Byte array
                           ; Size     : in    int                -- Number of bytes to write
                           ; verbose  : in    boolean   :=  true -- Display "success" message (but always display errors)
                           ; Status   : out   boolean) is        -- True if entire file has been written

	variable fstatus    : file_open_status; -- Status when opening file
	variable buff_8bit  : character       ; -- Read byte is stored into a character object
	variable message    : line            ; -- Message to display to simulator console
	variable Size_Done  : int             ; -- Effective size
begin
	Status := true;

	-- Open file in write mode
	file_open(fstatus,RawFile,Name,OpenMode);

	-- Check status after open operation
	case fstatus is
		when OPEN_OK      => null;
		when STATUS_ERROR => report "Error Code : STATUS_ERROR";
		                     Status := false;
		when NAME_ERROR   => write(message,string'("File "     )); write(message,Name);
		                     write(message,string'(" can NOT be created"));
		                     writeline(output,message);
		                     Status := false;
		                     report "Error Code : NAME_ERROR" severity failure;
		when MODE_ERROR   => write(message,string'("Error : File '")); write(message,Name); write(message,string'("' cannot be created !! Please check following :")); writeline(output,message);
		                     write(message,string'("     * directory already exists"                           )); writeline(output,message);
		                     write(message,string'("     * you have permission to write to this destination"   )); writeline(output,message);
		                     write(message,string'("     * disk is not full"                                   )); writeline(output,message);
		                     report "Error Code : MODE_ERROR" severity failure;
		                     Status := false;
	end case;

	-- Default value is full array length
	Size_Done := Size;

	for i in 0 to Size-1 loop
		if not(RawArray.Exist(i)) then
			Size_Done := i;
			exit;
		end if;
		buff_8bit := character'val(conv_int(uns(RawArray.Read(i).d)));
		write(RawFile,buff_8bit);
	end loop;

	if verbose then
		write(message,string'("File "                      )); write(message,Name     );
		write(message,string'(" successfully written (all ")); write(message,Size_Done);
		write(message,string'(" bytes were written)."      ));
		writeline(output,message);
	end if;

	file_close(RawFile);
end procedure WriteAppendFile_8;

-- Write a binary file (any kind of file) and return each byte into a separated vector.
-- Each Write operation (from source file) is made by 8bits accesses.
-- OVERWRITE mode
procedure WriteFile_8( Name     : in    string             -- Name File, including path with "/"
                     ; RawArray : inout ByteArrayProtected -- Byte array
                     ; Size     : in    int                -- Number of bytes to write
                     ; verbose  : in    boolean   :=  true -- Display "success" message (but always display errors)
                     ; Status   : out   boolean) is        -- True if entire file has been written
begin
	WriteAppendFile_8( Name     => Name                --in  string             -- Name File, including path with "/"
	                 , OpenMode => WRITE_MODE          --in  file_open_kind     -- READ_MODE / WRITE_MODE / APPEND_MODE
	                 , RawArray => RawArray            --in  ByteArrayProtected -- Byte array
	                 , Size     => Size                --in  int                -- Number of bytes to write
	                 , verbose  => verbose             --in  boolean   :=  true -- Display "success" message (but always display errors)
	                 , Status   => Status              --out boolean
	                 );
end procedure WriteFile_8;

-- Write a binary file (any kind of file) and return each byte into a separated vector.
-- Each Write operation (from source file) is made by 8bits accesses.
-- APPEND mode
procedure AppendFile_8( Name     : in    string             -- Name File, including path with "/"
                      ; RawArray : inout ByteArrayProtected -- Byte array
                      ; Size     : in    int                -- Number of bytes to write
                      ; verbose  : in    boolean   :=  true -- Display "success" message (but always display errors)
                      ; Status   : out   boolean) is        -- True if entire file has been written
begin
	WriteAppendFile_8( Name     => Name                --in  string             -- Name File, including path with "/"
	                 , OpenMode => APPEND_MODE         --in  file_open_kind     -- READ_MODE / WRITE_MODE / APPEND_MODE
	                 , RawArray => RawArray            --in  ByteArrayProtected -- Byte array
	                 , Size     => Size                --in  int                -- Number of bytes to write
	                 , verbose  => verbose             --in  boolean   :=  true -- Display "success" message (but always display errors)
	                 , Status   => Status              --out boolean
	                 );
end procedure AppendFile_8;

-- Low-level Write File, simplified call
procedure WriteFile_8( Name     : in    string
                     ; RawArray : inout ByteArrayProtected
                     ; Size     : in    int
                     ; verbose  : in    boolean     :=  true -- Display "success" message (but always display errors)
                     ) is
	variable Status : boolean; -- Status for write operation. It isn't forwarded to caller
begin

	WriteFile_8( Name      => Name         --in  string
	           , RawArray  => RawArray     --in  ByteArrayProtected
	           , Size      => Size         --in  int
	           , verbose   => verbose      --in  boolean     -- Display "success" message (but always display errors)
	           , Status    => Status);     --out boolean)

end procedure WriteFile_8;

-- Low-level Append File, simplified call
procedure AppendFile_8( Name     : in    string
                      ; RawArray : inout ByteArrayProtected
                      ; Size     : in    int
                      ; verbose  : in    boolean     :=  true -- Display "success" message (but always display errors)
                      ) is
	variable Status : boolean; -- Status for write operation. It isn't forwarded to caller
begin

	AppendFile_8( Name      => Name         --in  string
	            , RawArray  => RawArray     --in  ByteArrayProtected
	            , Size      => Size         --in  int
	            , verbose   => verbose      --in  boolean     -- Display "success" message (but always display errors)
	            , Status    => Status);     --out boolean)
end procedure AppendFile_8;
--*************************************************************************************************
--*                                    Others Write Operations                                    *
--*************************************************************************************************
procedure WriteBMP( Name      : in    string               -- Name File, including path with "/"
                  ; RGB       : inout RgbArrayProtected    -- RGB array
                  ; Alpha     : inout ByteArrayProtected   -- Alpha array
                  ; Width     : in    int                  -- BitMap Width
                  ; Height    : in    int                  -- BitMap Height
                  ; BitPerPix : in    int         :=    24 -- Number of bits per pixel
                  ; CompMode  : in    int         :=     0 -- Compression Mode
                  ; verbose   : in    boolean     :=  true -- Verbose mode
                  ) is
--=================================================================================================
-- B M P
--=================================================================================================
-- BitMap file format description
-- All multiple word are LSB first.
----------------------
-- Header
----------------------
--  00 - 01 : ASCII 2byte "BM" bitmap identifier
--  02 - 05 : Total length of bitmap file in bytes. Four byte integer.
--  06 - 09 : Reserved (possibly for image ID or revision). Four byte integer.
--  10 - 13 : Offset to start of actual pixel data. Four byte integer.
----------------------
-- Info Header
----------------------
--  14 - 17 : Size of data header, usually 40bytes. Four byte integer.
--  18 - 21 : Width of bitmap in pixels. Four byte integer.
--  22 - 25 : Height of bitmap in pixels. Four byte integer.
--  26 - 27 : Number of color planes. Usually 01. Two byte integer.
--  28 - 29 : Number of bits per pixel. Sets color mode.
--             1 : monochrome
--             4 : 16 lookup colors
--             8 : 256 lookup colors
--            16 : 65.536 lookup colors
--            24 : 16.777.216 RGB colors
--            32 : 16.777.216 RGB colors + alpha
--  30 - 33 : Compression mode in use. Four byte integer.
--             0 : None
--             1 : 8bit Run Length Encoded
--             2 : 4bit Run Length Encoded
--             3 : RGB bitmap with mask
--  34 - 37 : Size of stored pixel data. Four byte integer.
--  38 - 41 : Width resolution in pixels per meter. Four byte integer.
--  42 - 45 : Height resolution in pixels per meter. Four byte integer.
--  46 - 49 : Number of colors actually used. Four byte integer.
--  50 - 53 : Number of important colors. Four byte integer.
----------------------
-- Optionnal Palette
----------------------
-- Size is 4 * Number of colors used (set in Info Header, byte 46-49)
----------------------
-- Image data
----------------------
	variable bmp         : ByteArrayProtected       ; -- Can writte BitMap up to 2048x2048 pixels
	variable ThisRec     : ByteRec                  ;
	variable BmpSize     : int                  := 0; -- BitMap Total length (in bytes)
	variable ImageData   : int                  := 0; -- Offset to start of actual pixel data
	variable InfHdrSize  : int                  := 0; -- Info Header Size
	variable NbPlanes    : int                  := 0; -- Number of color planes
	variable DataSize    : int                  := 0; -- Size of stored pixel data
	variable ResX        : int                  := 0; -- Width resolution in pixels per meter
	variable ResY        : int                  := 0; -- Height resolution in pixels per meter
	variable NbColors    : int                  := 0; -- Number of colors actually used
	variable NbImpColors : int                  := 0; -- Number of important colors
	variable Padding     : int     range 0 to 3 := 0; -- Number of bytes for 32-bits line alignment
	variable p,x,y       : int                  := 0; -- Current Output pixel, (x,y) position
	variable i           : int                  := 0; -- Current Input byte

begin

	--------------------------
	-- Create BMP Header
	--------------------------
	-- Byte 00-01 : ASCII 2bytes "BM" bitmap identifier.
	ThisRec.d := x"42"; bmp.Write(0,ThisRec);
	ThisRec.d := x"4D"; bmp.Write(1,ThisRec);

	ImageData := 54;
	DataSize  := (Width*3 + (3 - (Width*3 - 1) mod 4))*Height;

	if BitPerPix=24 then BmpSize := ImageData + DataSize;
	else report "Number of bits per pixels not allowed" severity failure;
	end if;

	-- Total length of bitmap file in bytes
	ThisRec.d := conv_slv( BmpSize              mod 256,8); bmp.Write(02,ThisRec);
	ThisRec.d := conv_slv((BmpSize    /(2** 8)) mod 256,8); bmp.Write(03,ThisRec);
	ThisRec.d := conv_slv((BmpSize    /(2**16)) mod 256,8); bmp.Write(04,ThisRec);
	ThisRec.d := conv_slv((BmpSize    /(2**24)) mod 256,8); bmp.Write(05,ThisRec);

	-- Reserved (possibly for image ID or revision). Four byte integer.
	ThisRec.d := conv_slv(                            0,8); bmp.Write(06,ThisRec);
	ThisRec.d := conv_slv(                            0,8); bmp.Write(07,ThisRec);
	ThisRec.d := conv_slv(                            0,8); bmp.Write(08,ThisRec);
	ThisRec.d := conv_slv(                            0,8); bmp.Write(09,ThisRec);

	-- Offset to start of actual pixel data
	ThisRec.d := conv_slv( ImageData            mod 256,8); bmp.Write(10,ThisRec);
	ThisRec.d := conv_slv((ImageData  /(2** 8)) mod 256,8); bmp.Write(11,ThisRec);
	ThisRec.d := conv_slv((ImageData  /(2**16)) mod 256,8); bmp.Write(12,ThisRec);
	ThisRec.d := conv_slv((ImageData  /(2**24)) mod 256,8); bmp.Write(13,ThisRec);
	--------------------------
	-- Create BMP Info Header
	--------------------------
	InfHdrSize  := 40;
	ThisRec.d   := conv_slv( InfHdrSize           mod 256,8); bmp.Write(14,ThisRec);
	ThisRec.d   := conv_slv((InfHdrSize /(2** 8)) mod 256,8); bmp.Write(15,ThisRec);
	ThisRec.d   := conv_slv((InfHdrSize /(2**16)) mod 256,8); bmp.Write(16,ThisRec);
	ThisRec.d   := conv_slv((InfHdrSize /(2**24)) mod 256,8); bmp.Write(17,ThisRec);

	-- Width of bitmap in pixels
	ThisRec.d   := conv_slv( Width                mod 256,8); bmp.Write(18,ThisRec);
	ThisRec.d   := conv_slv((Width      /(2** 8)) mod 256,8); bmp.Write(19,ThisRec);
	ThisRec.d   := conv_slv((Width      /(2**16)) mod 256,8); bmp.Write(20,ThisRec);
	ThisRec.d   := conv_slv((Width      /(2**24)) mod 256,8); bmp.Write(21,ThisRec);

	-- Height of bitmap in pixels
	ThisRec.d   := conv_slv( Height               mod 256,8); bmp.Write(22,ThisRec);
	ThisRec.d   := conv_slv((Height     /(2** 8)) mod 256,8); bmp.Write(23,ThisRec);
	ThisRec.d   := conv_slv((Height     /(2**16)) mod 256,8); bmp.Write(24,ThisRec);
	ThisRec.d   := conv_slv((Height     /(2**24)) mod 256,8); bmp.Write(25,ThisRec);

	-- Number of color planes
	NbPlanes    := 1;
	ThisRec.d   := conv_slv( NbPlanes             mod 256,8); bmp.Write(26,ThisRec);
	ThisRec.d   := conv_slv((NbPlanes   /(2** 8)) mod 256,8); bmp.Write(27,ThisRec);

	-- Number of bits per pixel
	ThisRec.d   := conv_slv( BitPerPix            mod 256,8); bmp.Write(28,ThisRec);
	ThisRec.d   := conv_slv((BitPerPix  /(2** 8)) mod 256,8); bmp.Write(29,ThisRec);

	-- Compression Mode
	ThisRec.d   := conv_slv( CompMode             mod 256,8); bmp.Write(30,ThisRec);
	ThisRec.d   := conv_slv((CompMode   /(2** 8)) mod 256,8); bmp.Write(31,ThisRec);
	ThisRec.d   := conv_slv((CompMode   /(2**16)) mod 256,8); bmp.Write(32,ThisRec);
	ThisRec.d   := conv_slv((CompMode   /(2**24)) mod 256,8); bmp.Write(33,ThisRec);

	-- Size of stored pixel data
	ThisRec.d   := conv_slv( DataSize             mod 256,8); bmp.Write(34,ThisRec);
	ThisRec.d   := conv_slv((DataSize   /(2** 8)) mod 256,8); bmp.Write(35,ThisRec);
	ThisRec.d   := conv_slv((DataSize   /(2**16)) mod 256,8); bmp.Write(36,ThisRec);
	ThisRec.d   := conv_slv((DataSize   /(2**24)) mod 256,8); bmp.Write(37,ThisRec);

	-- Width resolution in pixels per meter
	ResX        := 3780;
	ThisRec.d   := conv_slv( ResX                 mod 256,8); bmp.Write(38,ThisRec);
	ThisRec.d   := conv_slv((ResX       /(2** 8)) mod 256,8); bmp.Write(39,ThisRec);
	ThisRec.d   := conv_slv((ResX       /(2**16)) mod 256,8); bmp.Write(40,ThisRec);
	ThisRec.d   := conv_slv((ResX       /(2**24)) mod 256,8); bmp.Write(41,ThisRec);

	-- Height resolution in pixels per meter
	ResY        := 3780;
	ThisRec.d   := conv_slv( ResY                 mod 256,8); bmp.Write(42,ThisRec);
	ThisRec.d   := conv_slv((ResY       /(2** 8)) mod 256,8); bmp.Write(43,ThisRec);
	ThisRec.d   := conv_slv((ResY       /(2**16)) mod 256,8); bmp.Write(44,ThisRec);
	ThisRec.d   := conv_slv((ResY       /(2**24)) mod 256,8); bmp.Write(45,ThisRec);

	-- Number of colors actually used
	NbColors    := 0;
	ThisRec.d   := conv_slv( NbColors             mod 256,8); bmp.Write(46,ThisRec);
	ThisRec.d   := conv_slv((NbColors   /(2** 8)) mod 256,8); bmp.Write(47,ThisRec);
	ThisRec.d   := conv_slv((NbColors   /(2**16)) mod 256,8); bmp.Write(48,ThisRec);
	ThisRec.d   := conv_slv((NbColors   /(2**24)) mod 256,8); bmp.Write(49,ThisRec);

	-- Number of important colors
	NbImpColors := 0;
	ThisRec.d   := conv_slv( NbImpColors          mod 256,8); bmp.Write(50,ThisRec);
	ThisRec.d   := conv_slv((NbImpColors/(2** 8)) mod 256,8); bmp.Write(51,ThisRec);
	ThisRec.d   := conv_slv((NbImpColors/(2**16)) mod 256,8); bmp.Write(52,ThisRec);
	ThisRec.d   := conv_slv((NbImpColors/(2**24)) mod 256,8); bmp.Write(53,ThisRec);

	-- Determine Padding value
	if ((Width*BitPerPix/8) mod 4)/=0 then Padding := 4 - ((Width*BitPerPix/8) mod 4);
	else                                   Padding := 0                              ; end if;
	--------------------------
	-- Create BMP Image Data
	--------------------------
	i := ImageData; -- Go to Image Data section
	x :=        0 ;
	y := Height-1 ; -- BitMap is line-reverse written

	if 3*Width*Height+i>PKG_FILE_ARRAY_SIZE then
		printf(failure,"[pkg_file] : Unable to write bitmap because PKG_FILE_ARRAY_SIZE constant is too small !!");
	end if;

	while not(y<0) loop
		p := y*Width+x;  -- compute current position (x,y)

		if RGB.Exist(p) then
			ThisRec.d := RGB.Read(p).R; bmp.Write(i+2,ThisRec);
			ThisRec.d := RGB.Read(p).G; bmp.Write(i+1,ThisRec);
			ThisRec.d := RGB.Read(p).B; bmp.Write(i+0,ThisRec); -- Write Red/Green/Blue
		else
			printf(failure,"[pkg_file / WriteBMP] : Input RGB (type RgbArray) doesn't have vector #%d",p);
		end if;

		i := i+3; -- Go to next 3x8bits
		x := x+1; -- Go to next input pixel (RGB)
		if x=Width then
			x :=   0;
			y := y-1;
			for zeroing in 1 to Padding loop -- Each line is 32bits aligned (Line padding : some 0s may be added, BitMap specificity)
				ThisRec.d := (others=>'0');
				bmp.Write(i,ThisRec);
				i := i + 1;
			end loop;
		end if;
	end loop;

	WriteFile_8( Name     => Name           --in    string
	           , RawArray => bmp            --in    ByteArrayProtected
	           , Size     => BmpSize        --in    int
	           , verbose  => verbose
	           );
end procedure WriteBMP;

procedure WriteBMP(          Name      : in    string               -- Name File, including path with "/"
                  ; variable RGB       : inout RgbArrayProtected    -- RGB array
                  ;          Width     : in    int                  -- BitMap Width
                  ;          Height    : in    int                  -- BitMap Height
                  ;          BitPerPix : in    int         :=    24 -- Number of bits per pixel
                  ;          CompMode  : in    int         :=     0 -- Compression Mode
                  ;          verbose   : in    boolean     :=  true -- Verbose mode
                  ) is
	variable Alpha : ByteArrayProtected; -- Temporary dummy alpha layer
begin
	WriteBMP( Name      => Name        -- Name File, including path with "/"
            , RGB       => RGB         -- RGB array
            , Alpha     => Alpha       -- Alpha array
            , Width     => Width       -- BitMap Width
            , Height    => Height      -- BitMap Height
            , BitPerPix => BitPerPix   -- Number of bits per pixel
            , CompMode  => CompMode    -- Compression Mode
            , verbose   => verbose     -- Verbose mode
           );
	Free(Alpha);
end procedure WriteBMP;
--=================================================================================================
-- Convert YCbCr to BMP24
--=================================================================================================
procedure WriteBMPFromYCbCr( Name      : in    string              -- Name File, including path with "/"
                           ; YCbCr     : inout YCbCrArrayProtected -- YCbCr Array
                           ; Width     : in    int                 -- BitMap Width
                           ; Height    : in    int                 -- BitMap Height
                          ) is
	variable RGB      : RgbArrayProtected; -- Bitmap to write
	variable ThisRec  : RgbRec           ; -- Temporary record

	--                                                              Y        Cb       Cr
	constant COEFF    : RealMatrix(1 to 3,1 to 3) := RealMatrix'( (1.164  ,  0.0   ,  1.596)    -- R
	                                                            , (1.164  , -0.391 , -0.813)    -- G
	                                                            , (1.164  ,  2.018 ,  0.0  ) ); -- B

	--                                                                         Y     Cb      Cr
	constant OFFSET   : RealMatrix(1 to 3,1 to 1) := MatrixCol(RealArray'( - 16.0, -128.0, -128.0)); -- YCbCr offset

	variable Sample   : RealMatrix(1 to 3,1 to 1); -- YCbCr pixel read from input array
	variable Result   : RealMatrix(1 to 3,1 to 1); -- RGB pixel resulting of YCbCr --> RGB conversion

begin

	for i in 0 to Width*Height/2 - 1 loop -- We have Width*Height pixels, so Width*Height/2 record (two Y per record)

		-- Compute first RGB pixel
		Sample(1,1) := real(conv_int(uns(YCbCr.Read(i).Ya)));
		Sample(2,1) := real(conv_int(uns(YCbCr.Read(i).Cb)));
		Sample(3,1) := real(conv_int(uns(YCbCr.Read(i).Cr)));

		Result := COEFF * (Sample + OFFSET);

		for i in 1 to 3 loop
			if    Result(i,1)<=  0.0 then Result(i,1) :=   0.0;
			elsif Result(i,1)>=255.0 then Result(i,1) := 255.0; end if;
		end loop;

		ThisRec.R := conv_slv(int(Result(1,1)),8);
		ThisRec.G := conv_slv(int(Result(2,1)),8);
		ThisRec.B := conv_slv(int(Result(3,1)),8);
		RGB.Write(2*i,ThisRec);

		-- Compute second RGB pixel
		Sample(1,1) := real(conv_int(uns(YCbCr.Read(i).Yb))); -- Update only Y information (Cb & Cr are valid for both pixels)

		Result := COEFF * (Sample + OFFSET);

		for i in 1 to 3 loop
			if    Result(i,1)<=  0.0 then Result(i,1) :=   0.0;
			elsif Result(i,1)>=255.0 then Result(i,1) := 255.0; end if;
		end loop;

		ThisRec.R := conv_slv(int(Result(1,1)),8);
		ThisRec.G := conv_slv(int(Result(2,1)),8);
		ThisRec.B := conv_slv(int(Result(3,1)),8);
		RGB.Write(2*i+1,ThisRec);
	end loop;

	WriteBMP( Name      => Name     --in    string            -- Name File, including path with "/"
	        , RGB       => RGB      --inout RgbArrayProtected -- RGB array
	        , Width     => Width    --in    int               -- BitMap Width
	        , Height    => Height   --in    int               -- BitMap Height
	        , BitPerPix => 24       --in    int         := 24 -- Number of bits per pixel
	       );
end procedure WriteBMPFromYCbCr;
--=================================================================================================
-- Write wave file
--=================================================================================================
procedure WriteWave( Name        : in    string               -- Name File, including path with "/"
	               ; DataSize    : in    int                  -- Final wave file size = this value + 44
	               ; Sampling    : in    int                  -- Number of samples per second (44.1KHz, 48KHz...)
	               ; BitsPerSamp : in    int                  -- Number of bits per sample
	               ; NbChannels  : in    int     range 1 to 6 -- Number of audio channels
	               ; Wav         : inout ByteArrayProtected
	              ) is
-- Detail of channels values
--  1 : mono
--  2 : stereo
--  3 : left, right, center
--  4 : front left, front right, rear left, rear right
--  5 : front left, front center, front right, rear left, rear right
--  6 : center left, left, center right, right, rear left, rear right
	variable WaveFile   : ByteArrayProtected                             ; -- Byte Array for the audio waveform
	variable ThisRec    : ByteRec                                        ; -- Temporary record
	variable FileSize   : slv32    := conv_slv(DataSize+36           ,32); -- This value doesn't include header (8bytes)
	variable BlocSize   : slv32    := conv_slv(         16           ,32); -- Number of bytes per block - 8
	variable v_Freq     : slv32    := conv_slv(Sampling              ,32); -- Samples frequency
	variable BytePerSec : slv32    := conv_slv(Sampling*BitsPerSamp/8,32); -- Number of bytes per second
	variable v_DataSize : slv32    := conv_slv(DataSize              ,32); -- Field value (32bits) for "Data Size"
	constant PCM        : slv8     := x"01"                              ; -- Field value to say waveform is in "PCM" format
begin
	--FileTypeBlocID
	ThisRec.d := conv_slv(character'pos('R'),8);      WaveFile.Write(00,ThisRec);
	ThisRec.d := conv_slv(character'pos('I'),8);      WaveFile.Write(01,ThisRec);
	ThisRec.d := conv_slv(character'pos('F'),8);      WaveFile.Write(02,ThisRec);
	ThisRec.d := conv_slv(character'pos('F'),8);      WaveFile.Write(03,ThisRec);
	--FileSize
	ThisRec.d := FileSize(BYTE_0);                    WaveFile.Write(04,ThisRec);
	ThisRec.d := FileSize(BYTE_1);                    WaveFile.Write(05,ThisRec);
	ThisRec.d := FileSize(BYTE_2);                    WaveFile.Write(06,ThisRec);
	ThisRec.d := FileSize(BYTE_3);                    WaveFile.Write(07,ThisRec);
	--FileFormatID
	ThisRec.d := conv_slv(character'pos('W'),8);      WaveFile.Write(08,ThisRec);
	ThisRec.d := conv_slv(character'pos('A'),8);      WaveFile.Write(09,ThisRec);
	ThisRec.d := conv_slv(character'pos('V'),8);      WaveFile.Write(10,ThisRec);
	ThisRec.d := conv_slv(character'pos('E'),8);      WaveFile.Write(11,ThisRec);
	--FormatBlocID
	ThisRec.d := conv_slv(character'pos('f'),8);      WaveFile.Write(12,ThisRec);
	ThisRec.d := conv_slv(character'pos('m'),8);      WaveFile.Write(13,ThisRec);
	ThisRec.d := conv_slv(character'pos('t'),8);      WaveFile.Write(14,ThisRec);
	ThisRec.d := conv_slv(character'pos(' '),8);      WaveFile.Write(15,ThisRec);
	--BlocSize
	ThisRec.d := BlocSize(BYTE_0);                    WaveFile.Write(16,ThisRec);
	ThisRec.d := BlocSize(BYTE_1);                    WaveFile.Write(17,ThisRec);
	ThisRec.d := BlocSize(BYTE_2);                    WaveFile.Write(18,ThisRec);
	ThisRec.d := BlocSize(BYTE_3);                    WaveFile.Write(19,ThisRec);
	--AudioFormat
	ThisRec.d :=   PCM;                               WaveFile.Write(20,ThisRec);
	ThisRec.d := x"00";                               WaveFile.Write(21,ThisRec);
	--NbrChannels
	ThisRec.d := conv_slv(NbChannels,8);              WaveFile.Write(22,ThisRec);
	ThisRec.d := x"00";                               WaveFile.Write(23,ThisRec);
	--Frequency
	ThisRec.d := v_Freq(BYTE_0);                      WaveFile.Write(24,ThisRec);
	ThisRec.d := v_Freq(BYTE_1);                      WaveFile.Write(25,ThisRec);
	ThisRec.d := v_Freq(BYTE_2);                      WaveFile.Write(26,ThisRec);
	ThisRec.d := v_Freq(BYTE_3);                      WaveFile.Write(27,ThisRec);
	--BytePerSec
	ThisRec.d := BytePerSec(BYTE_0);                  WaveFile.Write(28,ThisRec);
	ThisRec.d := BytePerSec(BYTE_1);                  WaveFile.Write(29,ThisRec);
	ThisRec.d := BytePerSec(BYTE_2);                  WaveFile.Write(30,ThisRec);
	ThisRec.d := BytePerSec(BYTE_3);                  WaveFile.Write(31,ThisRec);
	--BytePerBloc
	ThisRec.d := conv_slv(BitsPerSamp/8,8);           WaveFile.Write(32,ThisRec);
	ThisRec.d := x"00";                               WaveFile.Write(33,ThisRec);
	--BitsPerSample
	ThisRec.d := conv_slv(BitsPerSamp,8);             WaveFile.Write(34,ThisRec);
	ThisRec.d := x"00";                               WaveFile.Write(35,ThisRec);
	--DataBlocID
	ThisRec.d := conv_slv(character'pos('d'),8);      WaveFile.Write(36,ThisRec);
	ThisRec.d := conv_slv(character'pos('a'),8);      WaveFile.Write(37,ThisRec);
	ThisRec.d := conv_slv(character'pos('t'),8);      WaveFile.Write(38,ThisRec);
	ThisRec.d := conv_slv(character'pos('a'),8);      WaveFile.Write(39,ThisRec);
	--DataSize
	ThisRec.d := v_DataSize(BYTE_0);                  WaveFile.Write(40,ThisRec);
	ThisRec.d := v_DataSize(BYTE_1);                  WaveFile.Write(41,ThisRec);
	ThisRec.d := v_DataSize(BYTE_2);                  WaveFile.Write(42,ThisRec);
	ThisRec.d := v_DataSize(BYTE_3);                  WaveFile.Write(43,ThisRec);

	for i in 0 to DataSize-1 loop
		ThisRec.d := Wav.Read(i).d;
		WaveFile.Write(i+44,ThisRec);
	end loop;

	WriteFile_8(Name=>Name,RawArray=>WaveFile,Size=>DataSize+44,verbose=>true);
	Free(WaveFile);
end procedure WriteWave;
--*************************************************************************************************
--*                                            TXT file                                           *
--*************************************************************************************************
procedure TXT_Open( file f_handle :    text
                  ;      f_name   : in string
                  ;      f_mode   : in file_open_kind) is -- READ_MODE / WRITE_MODE / APPEND_MODE
	variable Status     : boolean          := true; -- File successfully opened
	variable fstatus    : file_open_status        ; -- Status when opening file
	variable message    : line                    ; -- Message to display to simulator console
begin

	-- Open file in specified mode
	file_open(fstatus,f_handle,f_name,f_mode);

	-- Check status after open operation
	case fstatus is
		when OPEN_OK      => null;
		when STATUS_ERROR => report "Error Code : STATUS_ERROR";
		                     Status := false;
		when NAME_ERROR   => write(message,string'("File "     )); write(message,f_name);
		                     if f_mode=READ_MODE then write(message,string'(" can NOT be opened"));
		                     else                     write(message,string'(" can NOT be created")); end if;
		                     writeline(output,message);
		                     Status := false;
		                     report "Error Code : NAME_ERROR" severity failure;
		when MODE_ERROR   => write(message,string'("Error : File '")); write(message,f_name); write(message,string'("' cannot be created !! Please check following :")); writeline(output,message);
		                     write(message,string'("     * directory already exists"                           )); writeline(output,message);
		                     write(message,string'("     * you have permission to write to this destination"   )); writeline(output,message);
		                     write(message,string'("     * disk is not full"                                   )); writeline(output,message);
		                     report "Error Code : MODE_ERROR" severity failure;
		                     Status := false;
	end case;
end procedure TXT_Open;

procedure TXT_Close(file f_handle : text) is
begin
	file_close(f_handle);
end procedure TXT_Close;

procedure TXT_Write( file f_handle :    text
                   ; s             : in string
                   ) is
	variable l : line; -- Line under construction
begin
	write    (l       ,s);
	writeline(f_handle,l);
end procedure TXT_Write;

procedure TXT_Write( file     f_handle     :       text
                   ; variable l            : inout line
                   ;          clear_l      :       boolean := true
                   ) is
	variable MyLine : line;
begin
	if clear_l then
		writeline(f_handle,l); -- Automatically cleared by IEEE library
	else
		LineCopy(l,MyLine);
		writeline(f_handle,MyLine);
	end if;
end procedure TXT_Write;

procedure TXT_Write(f_name : in string; s : in string; overwrite : in boolean := false) is
	file f_handle : text;
begin
	if overwrite then TXT_Open (f_handle,f_name, WRITE_MODE);
	else              TXT_Open (f_handle,f_name,APPEND_MODE); end if;
	TXT_Write(f_handle,s);
	TXT_Close(f_handle);
end procedure TXT_Write;

procedure TXT_Read( file     f_handle :       text
                  ; variable l        : inout line
                  ; variable eof      : out   boolean
                  ) is
begin
	if not(endfile(f_handle)) then -- Check that end of file is not reached
		readline(f_handle,l);      -- Read line
	end if;
	eof := endfile(f_handle);      -- Return EOF information
end procedure TXT_Read;
--*************************************************************************************************
--*                                     Write to HTML file                                        *
--*************************************************************************************************
-- Write HTML Header
procedure WriteHTMLHeader( file f :    text
                         ; s      : in string) is
begin
	WriteHTML(f,"<br><hr><p align=center><font size=5>" & s & "</font></p><hr>",false);
	WriteHTML(f,"<table border=1>",false);
end procedure WriteHTMLHeader;

procedure WriteHTML( file f :    text
                   ; s1     : in string
                   ; stamp  : in boolean:=true
                   ) is
	variable l          : line                                 ; -- Line under construction
	constant COL1_START : string(1 to 16) := "<td align=right>"; -- Column #1 Start
	constant COL1_END   : string(1 to  5) := "</td>"           ; -- Column #1 Stop
	constant COL2_START : string(1 to  4) := "<td>"            ; -- Column #2 Start
	constant COL2_END   : string(1 to  5) := "</td>"           ; -- Column #2 Stop
	constant LINE_END   : string(1 to  5) := "</tr>"           ; -- Line end
begin
	-- Time stamp
	if stamp then
		write(l,COL1_START);
		write(l,now,right,4,us);
		write(l,COL1_END);
	end if;

	-- String
	write(l,COL2_START); write(l,s1); write(l,COL2_END);
	write(l,LINE_END);

	writeline(f,l);
end procedure WriteHTML;

procedure WriteHTML( file f :    text
                   ; s1     : in string
                   ; s2     : in string
                   ; stamp  : in boolean :=true
                   ) is
	variable l          : line                                 ; -- Line under construction
	constant COL1_START : string(1 to 16) := "<td align=right>"; -- Column #1 Start
	constant COL1_END   : string(1 to  5) := "</td>"           ; -- Column #1 Stop
	constant COL2_START : string(1 to  4) := "<td>"            ; -- Column #2 Start
	constant COL2_END   : string(1 to  5) := "</td>"           ; -- Column #2 Stop
	constant LINE_END   : string(1 to  5) := "</tr>"           ; -- Line end
begin
	-- Time stamp
	if stamp then
		write(l,COL1_START);
		write(l,now,right,4,us);
		write(l,COL1_END);
	end if;

	write(l,COL2_START); write(l,s1); write(l,COL2_END);
	write(l,COL2_START); write(l,s2); write(l,COL2_END);
	write(l,LINE_END);

	writeline(f,l);
end procedure WriteHTML;

procedure WriteHTML( file f :    text
                   ; s1     : in string
                   ; s2     : in string
                   ; s3     : in string
                   ; stamp  : in boolean :=true
                   ) is
	variable l          : line                                 ; -- Line under construction
	constant COL1_START : string(1 to 16) := "<td align=right>"; -- Column #1 Start
	constant COL1_END   : string(1 to  5) := "</td>"           ; -- Column #1 Stop
	constant COL2_START : string(1 to  4) := "<td>"            ; -- Column #2 Start
	constant COL2_END   : string(1 to  5) := "</td>"           ; -- Column #2 Stop
	constant LINE_END   : string(1 to  5) := "</tr>"           ; -- Line end
begin
	-- Time stamp
	if stamp then
		write(l,COL1_START);
		write(l,now,right,4,us);
		write(l,COL1_END);
	end if;

	write(l,COL2_START); write(l,s1); write(l,COL2_END);
	write(l,COL2_START); write(l,s2); write(l,COL2_END);
	write(l,COL2_START); write(l,s3); write(l,COL2_END);
	write(l,LINE_END);

	writeline(f,l);
end procedure WriteHTML;

procedure WriteHTML( file f :    text
                   ; s1     : in string
                   ; s2     : in string
                   ; s3     : in string
                   ; s4     : in string
                   ; stamp  : in boolean :=true
                   ) is
	variable l          : line                                 ; -- Line under construction
	constant COL1_START : string(1 to 16) := "<td align=right>"; -- Column #1 Start
	constant COL1_END   : string(1 to  5) := "</td>"           ; -- Column #1 Stop
	constant COL2_START : string(1 to  4) := "<td>"            ; -- Column #2 Start
	constant COL2_END   : string(1 to  5) := "</td>"           ; -- Column #2 Stop
	constant LINE_END   : string(1 to  5) := "</tr>"           ; -- Line end
begin
	-- Time stamp
	if stamp then
		write(l,COL1_START);
		write(l,now,right,4,us);
		write(l,COL1_END);
	end if;

	write(l,COL2_START); write(l,s1); write(l,COL2_END);
	write(l,COL2_START); write(l,s2); write(l,COL2_END);
	write(l,COL2_START); write(l,s3); write(l,COL2_END);
	write(l,COL2_START); write(l,s4); write(l,COL2_END);
	write(l,LINE_END);

	writeline(f,l);
end procedure WriteHTML;

procedure WriteHTML( file f :    text
                   ; s1     : in string
                   ; s2     : in string
                   ; s3     : in string
                   ; s4     : in string
                   ; s5     : in string
                   ; stamp  : in boolean :=true
                   ) is
	variable l          : line                                 ; -- Line under construction
	constant COL1_START : string(1 to 16) := "<td align=right>"; -- Column #1 Start
	constant COL1_END   : string(1 to  5) := "</td>"           ; -- Column #1 Stop
	constant COL2_START : string(1 to  4) := "<td>"            ; -- Column #2 Start
	constant COL2_END   : string(1 to  5) := "</td>"           ; -- Column #2 Stop
	constant LINE_END   : string(1 to  5) := "</tr>"           ; -- Line end
begin
	-- Time stamp
	if stamp then
		write(l,COL1_START);
		write(l,now,right,4,us);
		write(l,COL1_END);
	end if;

	write(l,COL2_START); write(l,s1); write(l,COL2_END);
	write(l,COL2_START); write(l,s2); write(l,COL2_END);
	write(l,COL2_START); write(l,s3); write(l,COL2_END);
	write(l,COL2_START); write(l,s4); write(l,COL2_END);
	write(l,COL2_START); write(l,s5); write(l,COL2_END);
	write(l,LINE_END);

	writeline(f,l);
end procedure WriteHTML;
--*************************************************************************************************
--*                                         Misc                                                  *
--*************************************************************************************************
-- Deallocate all memory used by Array
procedure Free( variable MyArray   : inout ByteArray ) is
	variable i : int; -- Loop index
begin
	i := 0;
	while (MyArray(i)/=null) loop
		deallocate(MyArray(i));
		i := i + 1;
	end loop;
end procedure Free;

procedure Free( variable MyArray   : inout Byte3Array) is
	variable i : int; -- Loop index
begin
	i := 0;
	while (MyArray(i)/=null) loop
		deallocate(MyArray(i));
		i := i + 1;
	end loop;
end procedure Free;

procedure Free( variable MyArray   : inout RgbArray  ) is
	variable i : int; -- Loop index
begin
	i := 0;
	while (MyArray(i)/=null) loop
		deallocate(MyArray(i));
		i := i + 1;
	end loop;
end procedure Free;

procedure Free( variable MyArray   : inout YuvArray  ) is
	variable i : int; -- Loop index
begin
	i := 0;
	while (MyArray(i)/=null) loop
		deallocate(MyArray(i));
		i := i + 1;
	end loop;
end procedure Free;

procedure Free( variable MyArray   : inout YCbCrArray) is
	variable i : int; -- Loop index
begin
	i := 0;
	while (MyArray(i)/=null) loop
		deallocate(MyArray(i));
		i := i + 1;
	end loop;
end procedure Free;

procedure Free( variable MyArray   : inout ByteArrayProtected ) is begin MyArray.KillAll; end procedure Free;
procedure Free( variable MyArray   : inout Byte3ArrayProtected) is begin MyArray.KillAll; end procedure Free;
procedure Free( variable MyArray   : inout RgbArrayProtected  ) is begin MyArray.KillAll; end procedure Free;
procedure Free( variable MyArray   : inout YuvArrayProtected  ) is begin MyArray.KillAll; end procedure Free;
procedure Free( variable MyArray   : inout YCbCrArrayProtected) is begin MyArray.KillAll; end procedure Free;
--*************************************************************************************************
--*                                VARIABLE  <==>   SHARED  VARIABLE                              *
--*************************************************************************************************
procedure to_SharedVariable(variable v : in int; sv : inout IntegerProtected) is
begin
	sv.set(v);
end procedure to_SharedVariable;

procedure to_SharedVariable(variable v : in ByteArray; sv : inout ByteArrayProtected) is
	variable i       : int     := 0;
	variable ThisRec : ByteRec     ;
begin
	while (v(i)/=null) loop
		ThisRec.d := v(i).d;
		sv.Write(i,ThisRec);
		i := i + 1;
	end loop;
end procedure to_SharedVariable;

procedure to_SharedVariable(variable v : in Byte3Array; sv : inout Byte3ArrayProtected) is
	variable i       : int      := 0;
	variable ThisRec : Byte3Rec     ;
begin
	while (v(i)/=null) loop
		ThisRec.d0 := v(i).d0;
		ThisRec.d1 := v(i).d1;
		ThisRec.d2 := v(i).d2;
		sv.Write(i,ThisRec);
		i := i + 1;
	end loop;
end procedure to_SharedVariable;

procedure to_SharedVariable(variable v : in RgbArray  ; sv : inout RgbArrayProtected  ) is
	variable i       : int      := 0;
	variable ThisRec : RgbRec       ;
begin
	while (v(i)/=null) loop
		ThisRec.R := v(i).R;
		ThisRec.G := v(i).G;
		ThisRec.B := v(i).B;
		sv.Write(i,ThisRec);
		i := i + 1;
	end loop;
end procedure to_SharedVariable;

procedure to_SharedVariable(variable v : in YuvArray  ; sv : inout YuvArrayProtected  ) is
	variable i       : int      := 0;
	variable ThisRec : YuvRec       ;
begin
	while (v(i)/=null) loop
		ThisRec.U  := v(i).U ;
		ThisRec.Ya := v(i).Ya;
		ThisRec.V  := v(i).V ;
		ThisRec.Yb := v(i).Yb;
		sv.Write(i,ThisRec);
		i := i + 1;
	end loop;
end procedure to_SharedVariable;

procedure to_SharedVariable(variable v : in YCbCrArray; sv : inout YCbCrArrayProtected) is
	variable i       : int      := 0;
	variable ThisRec : YCbCrRec     ;
begin
	while (v(i)/=null) loop
		ThisRec.Cb := v(i).Cb;
		ThisRec.Ya := v(i).Ya;
		ThisRec.Cr := v(i).Cr;
		ThisRec.Yb := v(i).Yb;
		sv.Write(i,ThisRec);
		i := i + 1;
	end loop;
end procedure to_SharedVariable;

procedure to_RegularVariable(sv : inout IntegerProtected; v : out int) is
begin
	v := sv.value;
end procedure to_RegularVariable;

procedure to_RegularVariable(sv : inout ByteArrayProtected ; v : out ByteArray ) is
	variable i : int := 0;
begin
	while(sv.Exist(i)) loop
		v(i)   := new ByteRec;
		v(i).d := sv.Read(i).d;
		i := i + 1;
	end loop;
end procedure to_RegularVariable;

procedure to_RegularVariable(sv : inout Byte3ArrayProtected; v : out Byte3Array) is
	variable i : int := 0;
begin
	while(sv.Exist(i)) loop
		v(i)    := new Byte3Rec;
		v(i).d0 := sv.Read(i).d0;
		v(i).d1 := sv.Read(i).d1;
		v(i).d2 := sv.Read(i).d2;
		i := i + 1;
	end loop;
end procedure to_RegularVariable;

procedure to_RegularVariable(sv : inout RgbArrayProtected  ; v : out RgbArray  ) is
	variable i : int := 0;
begin
	while(sv.Exist(i)) loop
		v(i)   := new RgbRec;
		v(i).R := sv.Read(i).R;
		v(i).G := sv.Read(i).G;
		v(i).B := sv.Read(i).B;
		i := i + 1;
	end loop;
end procedure to_RegularVariable;

procedure to_RegularVariable(sv : inout YuvArrayProtected  ; v : out YuvArray  ) is
	variable i : int := 0;
begin
	while(sv.Exist(i)) loop
		v(i)    := new YuvRec;
		v(i).U  := sv.Read(i).U ;
		v(i).Ya := sv.Read(i).Ya;
		v(i).V  := sv.Read(i).V ;
		v(i).Yb := sv.Read(i).Yb;
		i := i + 1;
	end loop;
end procedure to_RegularVariable;

procedure to_RegularVariable(sv : inout YCbCrArrayProtected; v : out YCbCrArray) is
	variable i : int := 0;
begin
	while(sv.Exist(i)) loop
		v(i)    := new YCbCrRec;
		v(i).Cb := sv.Read(i).Cb;
		v(i).Ya := sv.Read(i).Ya;
		v(i).Cr := sv.Read(i).Cr;
		v(i).Yb := sv.Read(i).Yb;
		i := i + 1;
	end loop;
end procedure to_RegularVariable;

end package body pkg_file;
-- synthesis translate_on
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--synthesis translate_off
library ieee;
use     ieee.std_logic_1164.all;

library work;
use     work.pkg_std.all;
use     work.pkg_file.all;
use     work.pkg_simu.all;

entity pkg_file_tb is
end entity pkg_file_tb;

architecture testbench of pkg_file_tb is

begin
-- Low-level Read File
ReadFile_8_block : block is
	begin

	read8 : process
		variable RawArray : ByteArrayProtected; -- Array of bytes
		variable Size     : IntegerProtected  ; -- Number of elements
		variable Status   : boolean           ; -- Staus (file successfully opened)
	begin
		ReadFile_8(Name=>"../sources/stratix/test1.txt",verbose=>true,RawArray=>RawArray,error=>failure,Size=>Size,Status=>Status);
		wait for 1 ns;
		ReadFile_8(Name=>"../sources/stratix/test2.txt",RawArray=>RawArray,Size=>Size);
		wait;
	end process;
end block ReadFile_8_block;

ReadFile_3x8_block : block is
	begin

	read3x8 : process
		 variable RawArray : Byte3ArrayProtected; -- Array of 3bytes
	begin
		ReadFile_3x8(Name=>"../sources/stratix/test1.txt",RawArray=>RawArray);
		wait;
	end process;
end block ReadFile_3x8_block;

end architecture testbench;
--synthesis translate_on
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
