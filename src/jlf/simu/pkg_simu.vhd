/*--------------------------------------------------------------------------------------------------------------------------------------------------------------
Author      : Jean-Louis FLOQUET
Title       : Package for simulations
File        : pkg_simu.vhd
Application : Simulation
Created     : 2003, September 10th
Last update : 2014/06/05 11:00
Version     : 4.04.01
Dependency  : pkg_std
VHDL        : NOT compatible with VHDL'87 and VHDL'93 and VHDL 2002 => use VHDL-2008 !!
----------------------------------------------------------------------------------------------------------------------------------------------------------------
Description : This package :
   * provides string manipulation tools
   * extends native edge detection
   * provides tools for HTML logs
   * defines some new units

printf tags
   * "%4d"  : integer / use at least 4 digit (right aligned with spaces)
   * "%04d" : integer / use at least 4 digit (right aligned with 0s)
   * "%x"   : hexadecimal
   * "%f"   : float
----------------------------------------------------------------------------------------------------------------------------------------------------------------
   Rev.  |    Date    | Description
 4.04.01 | 2014/06/05 | 1) New : Imported 'RS232_SendChar' from removed pkg_rs232.vhd
         |            |
 4.03.06 | 2014/01/06 | 1) New : LineCopy & LineLength (moved from pkg_script)
         |            | 2) New : LineCmp
         | 2014/01/22 | 3) Enh : Memory deallocation for clearing a line
         | 2014/03/20 | 4) New : WaitClk for record 'domain'
         | 2014/03/25 | 5) New : BooleanProtected, SeverityLevelProtected
         | 2014/04/15 | 6) New : LineCmpNb
 4.02.04 | 2013/11/19 | 1) Fix : Compatibility with VHDL-2008
         |            | 2) Chg : VHDL-2008 required
         | 2013/11/20 | 3) Chg : SharedInteger renamed to IntegerProtected for global coherency
         | 2013/11/29 | 4) Chg : FreqPeriod renamed to FreqPeriodProtected for coherency
 4.01.02 | 2013/09/17 | 1) Fix : printf for slv displayed always a 512 bits value
         | 2013/10/08 | 2) New : ASCII Table print
 4.00.00 | 2013/07/22 | 0) Chg : printf syntax similar to C-language
         |            |
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 COMPATIBILITY BREAK OUT
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 3.02.01 | 2013/05/03 | 1) New : lprintf procedures
         |            |
 3.01.04 | 2012/06/01 | 1) Enh : Robustness to malformed tags on printf (missing last % at the end of the string)
         | 2012/07/27 | 2) Enh : Malformed tags on printf return a detailed "user friendly" message (show exact error position)
         | 2013/01/22 | 3) Fix : printf was case sensitive for floating values
         | 2013/01/30 | 4) Fix : printf's internal array was created for only 10 first values
         |            |
 3.00.00 | 2012/05/16 | 0) Chg : All string manipulations moved to pkg_std
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 COMPATIBILITY BREAK OUT
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 2.06.09 | 2011/03/22 | 1) Chg : Write to log file procedures moved to pkg_file.vhd
         | 2011/03/23 | 2) Chg : StrHex_SLV changed from procedure to function type
         | 2011/06/06 | 3) Fix : Non 0/1 value for to_StrHex argument is already checked in SLV4_Char subfunction. Old returned value was length fixed
         | 2011/08/01 | 4) Enh : User can constraint width for printf. Tag is now enclosed tag (%d% instead of %d). Code reduction
         | 2011/10/12 | 5) Chg : Moved Timings from pkg_simu
         | 2011/10/20 | 6) New : PosLSB/PosMSB return the position of the first non null LSB/MSB
         |            | 7) Enh : Version 2.06.04 (printf width user constraint) available for printf on SLV
         | 2011/12/07 | 8) Enh : printf support up to 20 arguments
         | 2012/01/02 | 9) New : Add protected type for integer
         |            |
 2.05.02 | 2009/07/20 | 1) Chg : Updated with new subtypes from pkg_std
         | 2009/09/11 | 2) Fix : Time_Str function when decimal part is longer than expected
 2.04.08 | 2008/12/02 | 1) New : Add LineArray type
         |            | 2) Chg : to_StrHex return "XXXX" for wrong input data
         | 2009/02/10 | 3) New : to_StrBin function
         |            | 4) Enh : Extend printf support to std_logic
         | 2009/02/17 | 5) New : Time_Str function
         | 2009/02/24 | 6) New : clear for line
         |            | 7) Chg : Updated from pkg_std 1.03.26
         | 2009/03/11 | 8) New : Char <-> Int functions
 2.03.04 | 2008/06/10 | 1) New : GetArchName function
         | 2008/10/01 | 2) Fix : StrCmpNoCase for empty string or starting with null character
         | 2008/11/14 | 3) Chg : Renamed
         |            |             * Hex2Char     -> SLV4_Char
         |            |             * Char2SLV4    -> Char_SLV4
         |            |             * Str2SLV      -> StrHex_SLV
         |            |             * Str2Int      -> Str_Int
         |            |             * Str2TimeUnit -> Str_TimeUnit
         |            | 4) New : Add Char_SLV8 & SLV8_Char functions
 2.02.14 | 2007/11/26 | 1) Chg : Script support has moved to pkg_script
         |            | 2) New : Add Timings record containing Min & Typ & Max timing values
         |            | 3) Fix : Printf for a truncated line (first valid character is not #1)
         |            | 4) New : Add StrLen function
         |            | 5) Fix : to_StrHex(slv) with a non 4x vector
         |            | 6) Chg : Renamed functions/procedures for consistency with others packages
         |            | 7) New : Str2SLV
         |            | 8) New : FreqPeriod as protected type for Frequency <--> Period. Require VHDL 2002 !!
         |            | 9) New : Can set length of returned string of to_StrHex
         |            |10) New : Can set length of returned string of Int_Str
         |            |11) Chg : Moved Int_Str to pkg_std
         |            |12) New : Characters comparator
         |            |13) New : StrIsInt function
         |            |14) New : "%D" support for printf with SLV
 2.01.07 | 2007/05/23 | 1) Add : UpperCase function
         |            | 2) Add : Char2SLV4
         |            | 3) Chg : Moved 'Is_01' to pkg_std
         |            | 4) Fix : Support for printf with SLV longer than 32bits
         |            | 5) Enh : WaitClk doesn't require anymore integer parameter is equal to 1
         |            | 6) New : Severity_level support for printf
         |            | 7) Fix : to_StrHex with a invalid SLV as input
 2.00.01 | 2006/06/20 | 1) Chg : printf handling for hexadecimal values
 2.00.00 | 2006/03/07 | 1) Add : Add support for script reader
         |            | 2) New : Add support of "line" for printf
         |            | 3) Enh : Int_Str function rewritten
         |            | 4) Fix : to_StrHex function might return 1 extra digit
         |            | 5) Chg : Merged 'str2int' and 'str2hex' into 'Str2Int' with 'base' as a new argument
---------+------------+-----------------------------------------------------------------------------------------------------------------------------------------
 1.01.01 | 2004/06/22 | 1) New : Add printf procedure
 1.00.00 | 2003/11/10 | Initial release
         |            |
--------------------------------------------------------------------------------------------------------------------------------------------------------------*/
-- synthesis translate_off
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

library work;
use     work.pkg_std.all;

package pkg_simu is
	type StrPtr    is access string;
	type StrArray  is array(nat range <>) of StrPtr;

	type LineArray is array(nat range <>) of line;

	------------------------------------------------------------------------------------------------
	-- Edge detection & Clock manipulation
	------------------------------------------------------------------------------------------------
	function  rising_edge (signal s   : boolean) return boolean;     -- This function will be standardized in VHDL 2008 !! :)
	function  falling_edge(signal s   : boolean) return boolean;     -- This function will be standardized in VHDL 2008 !! :)
	procedure WaitClk     (signal clk : in sl    ; nb : in nat := 1);
	procedure WaitClk     (signal dmn : in domain; nb : in nat := 1);
	------------------------------------------------------------------------------------------------
	-- Clear operations
	------------------------------------------------------------------------------------------------
	procedure clear(s : inout line  );
	procedure clear(s : inout string);
	procedure clear(s : inout slv   );
	------------------------------------------------------------------------------------------------
	-- String
	------------------------------------------------------------------------------------------------
	function  Time_Str    (t,unit : time  ; s_int,s_dec : nat   ) return string   ; -- Return time with fixed resolution (integral/decimal part sizes defined with s_xxx)
	------------------------------------------------------------------------------------------------
	-- line printf
	------------------------------------------------------------------------------------------------
	procedure lprintf(l : inout line                                                 ); -- Write line to simulator's console
	procedure lprintf(l : inout line; s : string                                     ); -- Append the string  at the end of the line
	procedure lprintf(l : inout line; x : int; size : nat; padding : character := ' '); -- Append the integer at the end of the line (minimum width in characters and padding character if required)
	------------------------------------------------------------------------------------------------
	-- line operations
	------------------------------------------------------------------------------------------------
	procedure LineCopy  (variable li : line; lo  : out line);
	procedure LineLength(variable l  : line; len : out nat );

	-- It would be nice to use a function here, but a function cannot receive a 'line'
	procedure LineCmp   (variable l1,l2 : line; cmp : out int          ); -- Compare both lines (-1 : l1<l2 / 0 : l1=l2 / 1 : l1>l2)
	procedure LineCmpNb (variable l1,l2 : line; cmp : out int; Nb : nat); -- Compare both lines (-1 : l1<l2 / 0 : l1=l2 / 1 : l1>l2). Only first Nb characters are taken into account

	------------------------------------------------------------------------------------------------
	-- printf
	------------------------------------------------------------------------------------------------
	procedure printf(level : in severity_level; Txt : in string; Val00 : in real; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in real := 0.0);
	procedure printf(level : in severity_level; Txt : in string; Val00 : in int ; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in int  := 0  );
	procedure printf(level : in severity_level; Txt : in string                                                                                                                                  );
	procedure printf(level : in severity_level; Txt : in string; Val00                                                                                                                   : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01                                                                                                             : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02                                                                                                       : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in sl );
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in sl );

	procedure printf(level : in severity_level; Txt : in string; Val00                                                                                                                   : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01                                                                                                             : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02                                                                                                       : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in slv);
	procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in slv);
	procedure printf(level : in severity_level; Txt : inout line                                                                                                                                 );

	procedure printf(                           Txt : in string; Val00 : in real; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in real := 0.0);
	procedure printf(                           Txt : in string; Val00 : in int ; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in int  := 0  );
	procedure printf(                           Txt : in string                                                                                                                                  );
	procedure printf(                           Txt : in string; Val00                                                                                                                   : in sl );
	procedure printf(                           Txt : in string; Val00,Val01                                                                                                             : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02                                                                                                       : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in sl );
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in sl );

	procedure printf(                           Txt : in string; Val00                                                                                                                   : in slv);
	procedure printf(                           Txt : in string; Val00,Val01                                                                                                             : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02                                                                                                       : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in slv);
	procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in slv);
	procedure printf(                           Txt : inout line                                                                                                                                 );

	procedure print_ascii_table;
	------------------------------------------------------------------------------------------------
	-- Misc.
	------------------------------------------------------------------------------------------------
	-- Define some usefull units
	type frequency is range int'low to int'high
		units
			 Hz           ;    --      Hertz
			KHz = 1000  Hz;    -- Kilo Hertz
			MHz = 1000 KHz;    -- Mega Hertz
			GHz = 1000 MHz;    -- Giga Hertz
			THz = 1000 GHz;    -- Tera Hertz
		end units;

	function "/" (l : int; r : frequency) return time;

	type FreqPeriodProtected is protected
		procedure SetFreq  (freq   : frequency);
		procedure SetPeriod(period : time     );
		impure function  Freq   return frequency;
		impure function  Period return time     ;
	end protected FreqPeriodProtected;
	------------------------------------------------------------------------------------------------
	-- Protected type for shared variables
	------------------------------------------------------------------------------------------------
	type BooleanProtected is protected
		procedure set      (n : boolean     );
		impure function value return boolean;
	end protected BooleanProtected;

	type IntegerProtected is protected
		procedure increment(n : integer := 1);
		procedure decrement(n : integer := 1);
		procedure set      (n : integer     );
		impure function value return integer;
	end protected IntegerProtected;

	type SeverityLevelProtected is protected
		procedure set      (n : severity_level     );
		impure function value return severity_level;
	end protected SeverityLevelProtected;

	------------------------------------------------------------------------------------------------
	-- From "pkg_rs232.vhd"
	------------------------------------------------------------------------------------------------
	procedure RS232_SendChar( signal clk        : in  sl      -- System clock
	                        ;        bps        :     natural -- Bit Per Second
	                        ;        data       :     slv8    -- Data to send
	                        ;        has_parity :     boolean -- Generate parity ?
	                        ; signal tx         : out sl      -- Tx line
	                        );

end package pkg_simu;
--==================================================================================================
--==================================================================================================
--==================================================================================================
package body pkg_simu is
----------------------------------------------------------------------------------------------------
-- Edge detection & Clock manipulation
----------------------------------------------------------------------------------------------------
-- "signal" must be added in declaration because EVENT and LAST_VALUE
-- operators require a static signal prefix.

function rising_edge(signal s : boolean) return boolean is
begin
	return s'event and s=true and s'last_value=false;
end function rising_edge;

function falling_edge(signal s : boolean) return boolean is
begin
	return s'event and s=false and s'last_value=true;
end function falling_edge;

procedure WaitClk(signal clk : in sl; nb : in nat := 1) is
begin
	for i in 1 to nb loop wait until rising_edge(clk); end loop;
end procedure WaitClk;

procedure WaitClk(signal dmn : in domain; nb : in nat := 1) is
begin
	for i in 1 to nb loop wait until rising_edge(dmn.clk); end loop;
end procedure WaitClk;
----------------------------------------------------------------------------------------------------
-- Clear Operations
----------------------------------------------------------------------------------------------------
procedure clear(s : inout line) is
begin
	-- For more details, see
	--    * IEEE Std 1076-2002, 3.3.2 Allocation and deallocation of objects, page  47
	--    * IEEE Std 1076-2002, 7.3.6 Allocators                            , page 112

	deallocate(s); -- This will remove the content of the line but KEEP the current pointer
--	s := null;     -- This will allocate a new pointer. Old amount of memory is still here !!!!
end procedure clear;

-- OTHERS choice cannot be used in unconstrained array aggregate (for string and/or slv)
-- As described with "verror 1076" under ModelSim, please report to IEEE Std 1076-2002, 7.3.2.2 Array aggregates, page 109
procedure clear(s : inout string) is
begin
	for i in s'range loop s(i) := character'val(0); end loop;
end procedure clear;

procedure clear(s : inout slv) is
begin
	for i in s'range loop s(i) := '0'; end loop;
end procedure clear;
--**************************************************************************************************
-- String manipulation
--**************************************************************************************************
--------------------------------------------------------------------------------
-- Convert a time to a string, with a fixed resolution for decimal part
--------------------------------------------------------------------------------
-- Parameters
--     t      : time to convert
--     unit   : time unit base
--     res    : number of digit to
function Time_Str(t,unit : time; s_int,s_dec : nat) return string is
	variable time_int : int ; -- Integral part of time
	variable time_dec : int ; -- Decimal  part of time
	variable l        : line; -- Temporary line for message to be displayed
	variable l_temp   : line; -- Temporary part of line for adding 0s
begin
	-- Create integral part
	time_int := t / unit;
	write(l_temp,to_Str(time_int));            -- Return integral part into 'l_temp'. Now, we have the length of this string
	for i in 1 to s_int-l_temp'length loop     -- Fill left part with 0s if decimal part is shorter than requested
		write(l,string'("0"));
	end loop;
	write(l,l_temp(1 to l_temp'length));       -- Add integral part : 'l' contains the integral with correct length

	if s_dec>0 then
		-- Write decimal separator
		write(l,string'("."));

		-- Clear line
		clear(l_temp);

		-- Create decimal part
		time_dec := (t - time_int * unit) / 1 ps;  -- Retrieve decimal part
		write(l_temp,to_Str(time_dec));            -- Return decimal part into 'l_temp'. Now, we have the length of this string
		                                           --
		if l_temp'length>=s_dec then               -- The decimal part is longer than requested (
			write(l,l_temp(1 to s_dec));           --     Add decimal part (only most signifiant digits
		else                                       -- The decimal part is shorter than requested
			for i in 1 to s_dec-l_temp'length loop --     Fill left part with 0s if decimal part is shorter than requested
				write(l,string'("0"));             --
			end loop;                              --
			write(l,l_temp(1 to l_temp'length));   --     Add decimal part
		end if;
	end if;

	-- Add unit
	--  Note : doing a case on 'unit' variable is impossible because the expression shall be a discrete type or one-dimensional array
	--         See IEEE1076-2000 �8.8, page 125 (PDF page 134/299), line 420
	   if unit=1 ps then write(l,string'(" ps"));
	elsif unit=1 ns then write(l,string'(" ns"));
	elsif unit=1 us then write(l,string'(" us"));
	elsif unit=1 ms then write(l,string'(" ms"));
	else printf(failure,"[PKG_SIMU] : Time_Str unsupported time unit !!");
	end if;

	return l(1 to l'length);
end function Time_Str;
--**************************************************************************************************
-- lprintf
--**************************************************************************************************
procedure lprintf(l : inout line; s : string) is
begin
	write(l,s);
end procedure lprintf;

procedure lprintf(l : inout line; x : int; size : nat; padding : character := ' ') is
begin
	write(l,string'(to_Str(x,size,padding)));
end procedure lprintf;

procedure lprintf(l : inout line) is
begin
	writeline(output,l);
end procedure lprintf;
--**************************************************************************************************
-- line operations
--**************************************************************************************************
--��������������������������������������
-- Duplicate a line with a NEW pointer
--��������������������������������������
-- We have to copy each character, one by one. A standard copy would result in NOT creating a new line but only a new pointer with the same properties
-- --> modifying one line would have modify the other one, and we don't want this behavior.
procedure LineCopy(variable li : line; lo : out line) is
	variable i   : int     := 1; -- Character index
	variable buf : character   ; -- Character read from source line
	variable lt  : line        ; -- Temporary line
begin
	if li/=null then
		for idx in li'left to li'right loop
			buf := li(idx);
			write(lt,buf);
		end loop;
		lo := lt;
	end if;
end procedure LineCopy;
--��������������������������������������
-- Return the length of the line
--��������������������������������������
procedure LineLength(variable l : line; len : out nat) is
begin
	len := l'right - l'left + 1;
end procedure LineLength;

--��������������������������������������
-- Compare lines (like strings)
--��������������������������������������
procedure LineCmp(variable l1,l2 : line; cmp : out int) is
	variable idx_l2 : int;
begin
	idx_l2 := l2'left;
	cmp    := 0;

	for idx_l1 in l1'left to l1'right loop
		   if idx_l2>l2'right       then cmp :=  1; exit;
		elsif l1(idx_l1)<l2(idx_l2) then cmp := -1; exit;
		elsif l1(idx_l1)>l2(idx_l2) then cmp :=  1; exit;
		else                                              end if;
		idx_l2 := idx_l2 + 1;
	end loop;
end procedure LineCmp;

procedure LineCmpNb(variable l1,l2 : line; cmp : out int; Nb : nat) is
	variable idx_l2 : int;
begin
	idx_l2 := l2'left;
	cmp    := 0;

	for idx_l1 in l1'left to Mini(l1'left+Nb-1,l1'right) loop
		   if idx_l2>l2'right       then cmp :=  1; exit;
		elsif l1(idx_l1)<l2(idx_l2) then cmp := -1; exit;
		elsif l1(idx_l1)>l2(idx_l2) then cmp :=  1; exit;
		else                                              end if;
		idx_l2 := idx_l2 + 1;
	end loop;
end procedure LineCmpNb;
--**************************************************************************************************
-- Printf
--**************************************************************************************************
-- Printf-like "C" command.
-- %b    : display value as bit (for std_logic)
-- %d    : display value as integer
-- %03d  : display value as integer with at least 3 digits          (add 0s   )
-- %3d   : display value as integer with at least 3 character width (add space)
-- %f    : display value as real
-- %x    : display value as hexadecimal

--��������������������������������������
-- Printf on REAL
--��������������������������������������
procedure printf_CheckLength(Txt : in string; i : in int) is
	variable l               : line            ; -- Line under construction
begin
	if i>Txt'length then
		clear(l); write(l,"printf error. Str ='" & Txt & "'"); writeline(output,l);
		clear(l); write(l,Dup(" ",20+i) & string'("^"));       writeline(output,l);
		report "[PKG_SIMU / PRINTF] : Illegal tag !!" severity failure;
	end if;
end procedure printf_CheckLength;

procedure printf_IllegalTag(Txt : in string; i : in int) is
	variable l               : line            ; -- Line under construction
begin
	clear(l); write(l,"printf error. Str ='" & Txt & "'"); writeline(output,l);
	clear(l); write(l,Dup(" ",20+i) & string'("^"));       writeline(output,l);
	report "[PKG_SIMU / PRINTF] : Illegal tag !!" severity failure;
end procedure printf_IllegalTag;

procedure printf(level   : in severity_level; Txt : in string; Val00 : in real; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in real := 0.0) is
	variable l               : line            ; -- Line under construction
	variable i               : int         := 1; -- Source character index
	variable tag_idx         : int         := 0; -- Tag index
	variable val_array       : Real20Array     ; -- Array of values
	variable tag_int_usr_len : int             ; -- Tag / Integer / User length (effective length as providen by parameter value)
	variable tag_int_line    : line            ; -- Tag / Integer / Line under construction
	variable tag_wide        : int             ; -- Number of characters requested for this tag
	variable tag_add_0s      : boolean         ; -- Add 0s     to before value (if value is shorter than requested tag)
	variable tag_add_space   : boolean         ; -- Add spaces to before value (if value is shorter than requested tag)
begin
	val_array(0) := Val00;    val_array(5) := Val05;    val_array(10) := Val10;    val_array(15) := Val15;
	val_array(1) := Val01;    val_array(6) := Val06;    val_array(11) := Val11;    val_array(16) := Val16;
	val_array(2) := Val02;    val_array(7) := Val07;    val_array(12) := Val12;    val_array(17) := Val17;
	val_array(3) := Val03;    val_array(8) := Val08;    val_array(13) := Val13;    val_array(18) := Val18;
	val_array(4) := Val04;    val_array(9) := Val09;    val_array(14) := Val14;    val_array(19) := Val19;

	write(l,Time_Str(now,1 us,3,6));
	write(l,string'(" : "));
	while i<=Txt'length loop
		if Txt(i)='%' then
			tag_wide      :=     0;
			tag_add_0s    := false;
			tag_add_space := false;

			i := i + 1; printf_CheckLength(Txt,i); -- Go to next character and verify coherency

			if Txt(i)='0' then tag_add_0s    := true;
			else               tag_add_space := true; end if;

			while true loop
				if Txt(i)='0' or Txt(i)='1' or Txt(i)='2' or Txt(i)='3' or Txt(i)='4' or Txt(i)='5' or Txt(i)='6' or Txt(i)='7' or Txt(i)='8' or Txt(i)='9' then
					tag_wide := 10*tag_wide + character'pos(Txt(i)) - 16#30#;
					i := i + 1; printf_CheckLength(Txt,i); -- Go to next character and verify coherency
				else
					exit;
				end if;
			end loop;

			tag_wide := Maxi(tag_wide,1);
			------------------------------------------------------------------
			-- Float
			------------------------------------------------------------------
			if Txt(i)='f' or Txt(i)='F' then
				write(l,val_array(tag_idx));
				tag_idx := tag_idx + 1;
			------------------------------------------------------------------
			-- Integer
			------------------------------------------------------------------
			elsif Txt(i)='d' or Txt(i)='D' then
				clear(tag_int_line);                         -- Clear the line (important because this sub-procedure may be called several times)
				write(tag_int_line,int(val_array(tag_idx))); -- Write integer to the temporary line
				tag_int_usr_len := tag_int_line'length;      -- Check effective length for parameter value given from user

				-- Insert space or 0s according to user requirement
				for tag_int_add_chr in 1 to tag_wide-tag_int_usr_len loop
					if tag_add_space then write(l,string'(" ")); end if;
					if tag_add_0s    then write(l,string'("0")); end if;
				end loop;

				-- Finaly, write the integer (regular sub-part)
				write(l,int(val_array(tag_idx)));
				tag_idx := tag_idx + 1;
			------------------------------------------------------------------
			-- Hexadecimal
			------------------------------------------------------------------
			elsif Txt(i)='x' or Txt(i)='X' then
				write(l,to_StrHex(int(val_array(tag_idx))));
				tag_idx := tag_idx + 1;
			------------------------------------------------------------------
			-- Only regular character
			------------------------------------------------------------------
			elsif Txt(i)='%' then
				write(l,'%');
			else
				printf_IllegalTag(Txt,i);
			end if;

			i := i + 1;
		else
			write(l,Txt(i));
			i := i + 1;
		end if;
	end loop;

	writeline(output,l); -- Send to standard output (simulator console)

	if level=failure then report "Stopping simulation..." severity level; end if;
end procedure printf;
procedure printf(                           Txt : in string; Val00 : in real; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in real := 0.0) is begin printf(note ,Txt,     Val00 ,     Val01 ,     Val02 ,     Val03 ,     Val04 ,     Val05 ,     Val06 ,     Val07 ,     Val08 ,     Val09 ,     Val10 ,     Val11 ,     Val12 ,     Val13 ,     Val14 ,     Val15 ,     Val16 ,     Val17 ,     Val18 ,     Val19 ); end procedure printf;

--��������������������������������������
-- Printf on INTEGER
--��������������������������������������
procedure printf(level : in severity_level; Txt : in string; Val00 : in int ; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in int  := 0  ) is begin printf(level,Txt,real(Val00),real(Val01),real(Val02),real(Val03),real(Val04),real(Val05),real(Val06),real(Val07),real(Val08),real(Val09),real(Val10),real(Val11),real(Val12),real(Val13),real(Val14),real(Val15),real(Val16),real(Val17),real(Val18),real(Val19)); end procedure printf;
procedure printf(                           Txt : in string; Val00 : in int ; Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in int  := 0  ) is begin printf(      Txt,real(Val00),real(Val01),real(Val02),real(Val03),real(Val04),real(Val05),real(Val06),real(Val07),real(Val08),real(Val09),real(Val10),real(Val11),real(Val12),real(Val13),real(Val14),real(Val15),real(Val16),real(Val17),real(Val18),real(Val19)); end procedure printf;

--��������������������������������������
-- Printf on SL
--��������������������������������������
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in sl) is
	variable l   : line     ; -- Line under construction
	variable i   : int  := 1; -- Source character index
	variable Idx : int  := 0; -- Line   character index
begin
	write(l,Time_Str(now,1 us,3,6));
	write(l,string'(" : "));
	while i<=Txt'length loop
		if Txt(i)='%' then
			if Txt(i+1)='b' or Txt(i+1)='B' then
				case Idx is
					when 00     => write (l,to_StrBin(Val00));
					when 01     => write (l,to_StrBin(Val01));
					when 02     => write (l,to_StrBin(Val02));
					when 03     => write (l,to_StrBin(Val03));
					when 04     => write (l,to_StrBin(Val04));
					when 05     => write (l,to_StrBin(Val05));
					when 06     => write (l,to_StrBin(Val06));
					when 07     => write (l,to_StrBin(Val07));
					when 08     => write (l,to_StrBin(Val08));
					when 09     => write (l,to_StrBin(Val09));
					when 10     => write (l,to_StrBin(Val10));
					when 11     => write (l,to_StrBin(Val11));
					when 12     => write (l,to_StrBin(Val12));
					when 13     => write (l,to_StrBin(Val13));
					when 14     => write (l,to_StrBin(Val14));
					when 15     => write (l,to_StrBin(Val15));
					when 16     => write (l,to_StrBin(Val16));
					when 17     => write (l,to_StrBin(Val17));
					when 18     => write (l,to_StrBin(Val18));
					when 19     => write (l,to_StrBin(Val19));
					when others => null;
				end case;
				Idx := Idx + 1;
				i   := i   + 2;
			else
				printf_IllegalTag(Txt,i);
			end if;
		else
			write(l,Txt(i));
			i := i + 1;
		end if;
	end loop;

	writeline(output,l); -- Send to standard output (simulator console)

	if level=failure then report "Stopping simulation..." severity level; end if;
end procedure printf;

--��������������������������������������
-- Printf on SLV
--��������������������������������������
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in slv) is
	variable l               : line                                               ; -- Line under construction
	variable i               : int                      :=                    1   ; -- Source character index
	variable tag_idx         : int                      :=                    0   ; -- Tag index
	variable val_array       : slv512array(19 downto 0) := (others=>(others=>'0')); -- Array of values
	variable len_array       : Int20Array               :=          (others=> 0 ) ; -- Array of length
	variable tag_int_req_len : int                                                ; -- Tag / Integer / Request length
	variable tag_int_usr_len : int                                                ; -- Tag / Integer / User length (effective length as providen by parameter value)
	variable tag_int_line    : line                                               ; -- Tag / Integer / Line under construction
	variable tag_wide        : int                                                ; -- Number of characters requested for this tag
	variable tag_add_0s      : boolean                                            ; -- Add 0s     to before value (if value is shorter than requested tag)
	variable tag_add_space   : boolean                                            ; -- Add spaces to before value (if value is shorter than requested tag)
begin
	val_array(00)(Val00'length-1 downto 0) := Val00;    len_array(00) := Val00'length;    assert Val00'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(01)(Val01'length-1 downto 0) := Val01;    len_array(01) := Val01'length;    assert Val01'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(02)(Val02'length-1 downto 0) := Val02;    len_array(02) := Val02'length;    assert Val02'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(03)(Val03'length-1 downto 0) := Val03;    len_array(03) := Val03'length;    assert Val03'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(04)(Val04'length-1 downto 0) := Val04;    len_array(04) := Val04'length;    assert Val04'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(05)(Val05'length-1 downto 0) := Val05;    len_array(05) := Val05'length;    assert Val05'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(06)(Val06'length-1 downto 0) := Val06;    len_array(06) := Val06'length;    assert Val06'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(07)(Val07'length-1 downto 0) := Val07;    len_array(07) := Val07'length;    assert Val07'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(08)(Val08'length-1 downto 0) := Val08;    len_array(08) := Val08'length;    assert Val08'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(09)(Val09'length-1 downto 0) := Val09;    len_array(09) := Val09'length;    assert Val09'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(10)(Val10'length-1 downto 0) := Val10;    len_array(10) := Val10'length;    assert Val10'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(11)(Val11'length-1 downto 0) := Val11;    len_array(11) := Val11'length;    assert Val11'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(12)(Val12'length-1 downto 0) := Val12;    len_array(12) := Val12'length;    assert Val12'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(13)(Val13'length-1 downto 0) := Val13;    len_array(13) := Val13'length;    assert Val13'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(14)(Val14'length-1 downto 0) := Val14;    len_array(14) := Val14'length;    assert Val14'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(15)(Val15'length-1 downto 0) := Val15;    len_array(15) := Val15'length;    assert Val15'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(16)(Val16'length-1 downto 0) := Val16;    len_array(16) := Val16'length;    assert Val16'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(17)(Val17'length-1 downto 0) := Val17;    len_array(17) := Val17'length;    assert Val17'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(18)(Val18'length-1 downto 0) := Val18;    len_array(18) := Val18'length;    assert Val18'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;
	val_array(19)(Val19'length-1 downto 0) := Val19;    len_array(19) := Val19'length;    assert Val19'length<=512 report "[pkg_simu] : printf doesn't support vector greater than 512 bits !!" severity failure;

	write(l,Time_Str(now,1 us,3,6));
	write(l,string'(" : "));
	while i<=Txt'length loop
		if Txt(i)='%' then
			tag_wide      :=     0;
			tag_add_0s    := false;
			tag_add_space := false;

			i := i + 1; printf_CheckLength(Txt,i); -- Go to next character and verify coherency

			if Txt(i)='0' then tag_add_0s    := true;
			else               tag_add_space := true; end if;

			while true loop
				if Txt(i)='0' or Txt(i)='1' or Txt(i)='2' or Txt(i)='3' or Txt(i)='4' or Txt(i)='5' or Txt(i)='6' or Txt(i)='7' or Txt(i)='8' or Txt(i)='9' then
					tag_wide := 10*tag_wide + character'pos(Txt(i)) - 16#30#;
					i := i + 1; printf_CheckLength(Txt,i); -- Go to next character and verify coherency
				else
					exit;
				end if;
			end loop;

			tag_wide := Maxi(tag_wide,1);

			------------------------------------------------------------------
			-- Hexadecimal
			------------------------------------------------------------------
			if Txt(i)='x' or Txt(i)='X' then
				write (l,to_StrHex(val_array(tag_idx)(len_array(tag_idx)-1 downto 0)));
				tag_idx := tag_idx + 1;
			------------------------------------------------------------------
			-- Integer
			------------------------------------------------------------------
			elsif Txt(i)='d' or Txt(i)='D' then
				clear(tag_int_line);                                                  -- Clear the line (important because this sub-procedure may be called several times)
				write(tag_int_line,to_Str(to_integer(unsigned(val_array(tag_idx))))); -- Write integer to the temporary line
				tag_int_usr_len := tag_int_line'length;                               -- Check effective length for parameter value given from user

				-- Insert space or 0s according to user requirement
				for tag_int_add_chr in 1 to tag_wide-tag_int_usr_len loop
					if tag_add_space then write(l,string'(" ")); end if;
					if tag_add_0s    then write(l,string'("0")); end if;
				end loop;

				-- Finaly, write the integer (regular sub-part)
				write(l,to_Str(to_integer(unsigned(val_array(tag_idx)))));
				tag_idx := tag_idx + 1;
			------------------------------------------------------------------
			-- Only regular character
			------------------------------------------------------------------
			elsif Txt(i)='%' then
				write(l,'%');
			else
				printf_IllegalTag(Txt,i);
			end if;

			i := i + 1;
		else
			write(l,Txt(i));
			i := i + 1;
		end if;
	end loop;

	writeline(output,l); -- Send to standard output (simulator console)

	if level=failure then report "Stopping simulation..." severity level; end if;
end procedure printf;

--��������������������������������������
-- Printf on LINE
--��������������������������������������
procedure printf(level : in severity_level; Txt : inout line) is
	-- Given line 'Txt' may not start at char #1. So, usefull length is computed with right&left attributes.
	-- Result 'str' is always of the form (1 to xx), which is mandatory for others 'printf' procedures.
	variable str : string(1 to Txt'right-Txt'left+1); -- Equivalent string
	variable i   : int := 1                         ; -- Current position
begin
	for TxtPos in Txt'left to Txt'right loop
		str(i) := Txt(TxtPos);
		i      := i + 1;
	end loop;
	printf(level,str);
end procedure printf;

--��������������������������������������
-- Printf without severity level
--��������������������������������������
procedure printf(level : in severity_level; Txt : in string                                                                                                                                  ) is begin printf(level,Txt,0                                                                                                                      ); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00                                                                                                                   : in sl ) is begin printf(level,Txt,Val00,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01                                                                                                             : in sl ) is begin printf(level,Txt,Val00,Val01,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02                                                                                                       : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,  '0',  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,  '0',  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in sl ) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,  '0'); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00                                                                                                                   : in slv) is begin printf(level,Txt,Val00, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01                                                                                                             : in slv) is begin printf(level,Txt,Val00,Val01, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02                                                                                                       : in slv) is begin printf(level,Txt,Val00,Val01,Val02, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12, x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13, x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14, x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15, x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16, x"0", x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17, x"0", x"0"); end procedure printf;
procedure printf(level : in severity_level; Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in slv) is begin printf(level,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18, x"0"); end procedure printf;

--Identical to the previous procedures, just add 'note' as severity level
procedure printf(                           Txt : in string                                                                                                                                  ) is begin printf(note ,Txt,0                                                                                                                      ); end procedure printf;
procedure printf(                           Txt : in string; Val00                                                                                                                   : in sl ) is begin printf(note ,Txt,Val00,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01                                                                                                             : in sl ) is begin printf(note ,Txt,Val00,Val01,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02                                                                                                       : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,  '0',  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,  '0',  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,  '0',  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,  '0',  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,  '0',  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,  '0',  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,  '0',  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,  '0'); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in sl ) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19); end procedure printf;
procedure printf(                           Txt : in string; Val00                                                                                                                   : in slv) is begin printf(note ,Txt,Val00, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01                                                                                                             : in slv) is begin printf(note ,Txt,Val00,Val01, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02                                                                                                       : in slv) is begin printf(note ,Txt,Val00,Val01,Val02, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03                                                                                                 : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04                                                                                           : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05                                                                                     : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06                                                                               : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07                                                                         : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08                                                                   : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09                                                             : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10                                                       : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11                                                 : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11, x"0", x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12                                           : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12, x"0", x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13                                     : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13, x"0", x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14                               : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14, x"0", x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15                         : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15, x"0", x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16                   : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16, x"0", x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17             : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17, x"0", x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18       : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18, x"0"); end procedure printf;
procedure printf(                           Txt : in string; Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19 : in slv) is begin printf(note ,Txt,Val00,Val01,Val02,Val03,Val04,Val05,Val06,Val07,Val08,Val09,Val10,Val11,Val12,Val13,Val14,Val15,Val16,Val17,Val18,Val19); end procedure printf;
procedure printf(Txt : inout line) is begin printf(note,Txt); end procedure printf;

--**************************************************************************************************
-- Printf ASCII Table
--**************************************************************************************************
procedure print_ascii_table is
	variable MyLine : line;
begin
	write(MyLine,string'("  0 1 2 3 4 5 6 7 8 9 A B C D E F")); writeline(output,MyLine);
	clear(MyLine);

	for i in 0 to 15 loop
		lprintf(MyLine,to_StrHex(i));
		for j in 0 to 15 loop
			case 16*i+j is
				when 16#00#  => lprintf(MyLine,"  ");
				when 16#01#  => lprintf(MyLine,"  ");
				when 16#09#  => lprintf(MyLine,"  ");
				when 16#0A#  => lprintf(MyLine,"  ");
				when 16#0D#  => lprintf(MyLine,"  ");
				when others  => lprintf(MyLine," " & character'val(16*i+j));
			end case;
		end loop;
		writeline(output,MyLine);
		clear(MyLine);
	end loop;
end procedure print_ascii_table;
--**************************************************************************************************
-- Frequency <==> Period
--**************************************************************************************************
-- This protected type provides a simple way to use Period or Frequency in simulation models:
--    1) User declares a shared variable of this type => shared variable MyVar : FreqPeriod;
--    2) User can affect frequency or period with following commands:
--          a) MyVar.SetPeriod(xx <time unit>) ==> call internal procedure and also compute frequency
--          b) MyVar.SetFreq  (xx <frequency>) ==> call internal procedure and also compute period
--    3) User can call back any value:
--          a) MyVar.Freq   ==> return frequency (with its unit)
--          b) MyVar.Period ==> return period    (with its unit)
--
type FreqPeriodProtected is protected body
	variable f : frequency := 0 Hz; -- Frequency (in Hertz )
	variable t : time      := 0 ps; -- Period    (in second)

	function Freq2Period(freq : frequency) return time is
	begin
		return (1.0 / ((freq / 1 Hz) * 1.0)) * 1 sec;
	end function Freq2Period;

	function Period2Freq(period : time) return frequency is
	begin
		return (1 sec / period) * 1 Hz;
	end function Period2Freq;

	procedure SetFreq(freq : frequency) is
	begin
		t := Freq2Period(freq);
		f := freq;
	end procedure SetFreq;

	procedure SetPeriod(period : time) is
	begin
		t := period;
		f := Period2Freq(period);
	end procedure SetPeriod;

	impure function Freq   return frequency is begin return 1 Hz; end function Freq  ;
	impure function Period return time      is begin return t   ; end function Period;

end protected body FreqPeriodProtected;

function "/" (l : int ; r : frequency) return time is
begin
	return 1 sec * l / ( r / 1 Hz);
end function "/";

--**************************************************************************************************************************************************************
-- Protected type for shared variables
--**************************************************************************************************************************************************************
type BooleanProtected is protected body
	variable v : boolean := false;
	procedure set      (n : boolean     ) is begin v :=     n; end procedure set      ;
	impure function value return boolean  is begin return v  ; end function  value;
end protected body BooleanProtected;

type IntegerProtected is protected body
	variable v : int := 0;

	procedure increment(n : integer := 1) is begin v := v + n; end procedure increment;
	procedure decrement(n : integer := 1) is begin v := v - n; end procedure decrement;
	procedure set      (n : integer     ) is begin v :=     n; end procedure set      ;
	impure function value return integer  is begin return v  ; end function  value;
end protected body IntegerProtected;

type SeverityLevelProtected is protected body
	variable v : severity_level := note;
	procedure set      (n : severity_level     ) is begin v :=     n; end procedure set      ;
	impure function value return severity_level  is begin return v  ; end function  value;
end protected body SeverityLevelProtected;

--**************************************************************************************************************************************************************
-- From "pkg_rs232.vhd"
--**************************************************************************************************************************************************************
procedure RS232_SendChar( signal clk        : in  sl      -- System clock
	                    ;        bps        :     natural -- Bit Per Second
	                    ;        data       :     slv8    -- Data to send
	                    ;        has_parity :     boolean -- Generate parity ?
	                    ; signal tx         : out sl      -- Tx line
	                    ) is
	variable clk_t0     : time := 0 ps;
	variable clk_t1     : time := 0 ps;
	variable clk_period : real        ; -- Clock period (in second, without time unit)
	variable divider    : natural     ;
begin
	-- Wait for two consecutives rising edge of clk
	wait until clk='1' and clk'last_value='0'; clk_t0 := now;
	wait until clk='1' and clk'last_value='0'; clk_t1 := now;

	-- Convert Delta(t) to real
	clk_period := Time_Real(clk_t1 - clk_t0);

	-- Determine how many clock cycles are required for 1 bit
	divider := natural(1.0 / real(bps) / clk_period);

	if has_parity then printf("[PKG_RS232] : RS232_SendChar 0x%X with parity"   ,data);
	else               printf("[PKG_RS232] : RS232_SendChar 0x%X without parity",data); end if;

	tx <=     '0'; WaitClk(clk,divider); -- Start bit
	tx <= data(0); WaitClk(clk,divider); -- Send 1st data bit
	tx <= data(1); WaitClk(clk,divider); -- Send 2nd data bit
	tx <= data(2); WaitClk(clk,divider); -- Send 3rd data bit
	tx <= data(3); WaitClk(clk,divider); -- Send 4th data bit
	tx <= data(4); WaitClk(clk,divider); -- Send 5th data bit
	tx <= data(5); WaitClk(clk,divider); -- Send 6th data bit
	tx <= data(6); WaitClk(clk,divider); -- Send 7th data bit
	tx <= data(7); WaitClk(clk,divider); -- Send 8th data bit

	if has_parity then
		tx <= xor1(data); WaitClk(clk,divider); -- Send parity bit
	end if;

	tx <=     'H'; WaitClk(clk,divider); -- Stop bit
end procedure RS232_SendChar;

end package body pkg_simu;
-- synthesis translate_on
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
--##############################################################################################################################################################
